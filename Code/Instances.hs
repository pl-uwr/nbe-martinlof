{-# OPTIONS -XStandaloneDeriving #-}

module Instances where

import Extracted

deriving instance Show Tm
deriving instance Show Sb

deriving instance Eq Tm
deriving instance Eq Sb

deriving instance Show D
deriving instance Show DNe
deriving instance Show DNf
deriving instance Show DEnv

deriving instance Eq D
deriving instance Eq DNe
deriving instance Eq DNf
deriving instance Eq DEnv

