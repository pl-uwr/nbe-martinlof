(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Well-founded induction principle for measurements
 *)

Require Import Wf_nat.
Require Import Nbe.Utils.TreeShape.Def.

Create HintDb measure_db.

Theorem TreeShape_wfnatind:
  forall (mu : TreeShape -> nat) (P : TreeShape -> Prop),
  (forall n sh, mu sh = n -> P sh) -> forall sh, P sh.
Proof.
eauto.
Qed.

Theorem TreeShape_wfnatind_node2:
  forall (mu : TreeShape -> nat) (P : TreeShape -> TreeShape -> Prop),
  (forall n sh1 sh2, mu (Node2 sh1 sh2) = n -> P sh1 sh2) -> forall sh1 sh2, P sh1 sh2.
Proof.
eauto.
Qed.

Ltac wfind :=
match goal with
| |- forall (n:nat), ?GOAL =>
  intro n;  apply lt_wf_ind with (P := fun n => GOAL);
  clear n;
  let WFIH := fresh "WFIH" in
  intros n WFIH; intros
end.

Ltac treeshape_wfind m :=
match goal with
| |- forall (ts : TreeShape), ?GOAL =>

  intro ts; apply TreeShape_wfnatind with
    (mu := m) (P := fun ts =>  GOAL);
  clear ts; wfind
end.

Ltac treeshape_wfind_node2 m :=
match goal with
| |- forall (sh1 sh2 : TreeShape), ?GOAL =>

  intros sh1 sh2; apply TreeShape_wfnatind_node2 with
    (mu := m) (P := fun sh1 sh2 =>  GOAL);
  clear sh2; clear sh1; wfind
end.

