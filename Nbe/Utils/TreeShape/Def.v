(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Annotation used in augmented type system (Nbe.Syntax.SystemN). 
 *)

Inductive TreeShape : Set :=
  | Leaf:
    TreeShape

  | Node1:
    TreeShape ->
    TreeShape

  | Node2:
    TreeShape ->
    TreeShape ->
    TreeShape

  | Node3:
    TreeShape ->
    TreeShape ->
    TreeShape ->
    TreeShape

  | Node4:
    TreeShape ->
    TreeShape ->
    TreeShape ->
    TreeShape ->
    TreeShape
.

