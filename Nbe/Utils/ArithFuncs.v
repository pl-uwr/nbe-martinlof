
(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Extensions of max and plus functions to many arguments.
 *)


Require Import Arith.
Require Import Arith.Max.
Require Import Arith.Lt.
Require Import Arith.Le.
Require Import Arith.Mult.
Require Import NPeano.

Import Nat.

(******************************************************************************
 * Definitions
 *)

Definition max2 a b := max a b.
Definition max3 a b c := max a (max b c).
Definition max4 a b c d := max a (max b (max c d)).

Definition smax2 a b := S (max2 a b).
Definition smax3 a b c := S (max3 a b c).
Definition smax4 a b c d := S (max4 a b c d).

Definition splus2 a b := S (a + b).
Definition splus3 a b c := S (a + b + c).
Definition splus4 a b c d := S (a + b + c + d).


(******************************************************************************
 * Facts.
 *)

Create HintDb arith2_db.

(******************************************************************************
 * SMAX2
 *)

Fact smax2_lt_1: forall b1 b2,
  b1 < smax2 b1 b2
.
Proof.
intros.
unfold smax2.
unfold lt.
eapply le_n_S.
eapply le_max_l.
Qed.
Hint Resolve smax2_lt_1 : arith2_db.

Fact smax2_lt_2: forall b1 b2,
  b2 < smax2 b1 b2
.
Proof.
intros.
unfold smax2.
unfold lt.
eapply le_n_S.
eapply le_max_r.
Qed.
Hint Resolve smax2_lt_2 : arith2_db.

(* Coq 8.3 *)
Lemma mult_lt_compat_l
     : forall n m p : nat, n < m -> 0 < p -> p * n < p * m.
Proof.
intros.
setoid_rewrite mult_comm.
eapply mult_lt_compat_r; eauto.
Qed.
Hint Resolve mult_lt_compat_l : arith.
 
Lemma max_le_1: forall a a' b,
  a < a' -> max a b <= max a' b.
Proof.
intros.
destruct le_or_lt with (n := a') (m := b).
rewrite max_r.
rewrite max_r.
eauto.
eauto.
eapply lt_le_weak.
eapply lt_le_trans.
eauto.
eauto.
setoid_rewrite max_l at 2.
eapply max_lub.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve max_le_1 : arith2_db.

Lemma max_lt_1: forall a a' b,
  a <= a' -> max a b <= max a' b.
Proof.
intros.
destruct le_or_lt with (n := a') (m := b).
rewrite max_r.
rewrite max_r.
eauto.
eauto.
eapply le_trans.
eauto.
eauto.
setoid_rewrite max_l at 2.
eapply max_lub.
eauto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve max_lt_1 : arith2_db.

Lemma smax2_lt_le_1: forall a a' b,
  a < a' -> smax2 a b <= smax2 a' b.
Proof.
intros.
unfold smax2.
eapply le_n_S.
eapply max_le_1.
eauto.
Qed.
Hint Resolve smax2_lt_le_1 : arith2_db.

Lemma smax2_le_le_1: forall a a' b,
  a <= a' -> smax2 a b <= smax2 a' b.
Proof.
intros.
unfold smax2.
eapply le_n_S.
eapply max_lt_1.
eauto.
Qed.
Hint Resolve smax2_le_le_1 : arith2_db.

Lemma smax2_comm: forall a b,
  smax2 a b = smax2 b a.
Proof.
intros.
unfold smax2.
unfold max2.
rewrite max_comm.
auto.
Qed.
Hint Resolve smax2_comm : arith2_db.

Lemma smax2_lt_le_2: forall a b b',
  b < b' -> smax2 a b <= smax2 a b'.
Proof.
intros.
setoid_rewrite smax2_comm.
eauto with arith2_db.
Qed.
Hint Resolve smax2_lt_le_2: arith2_db.

Lemma smax2_le_le_2: forall a b b',
  b <= b' -> smax2 a b <= smax2 a b'.
Proof.
intros.
setoid_rewrite smax2_comm.
eauto with arith2_db.
Qed.
Hint Resolve smax2_le_le_2 : arith2_db.

Lemma smax2_r_smax2_1: forall a b1 b2,
  smax2 a b1 <= smax2 a (smax2 b1 b2).
Proof.
intros.
destruct le_or_lt with (n := b1) (m:= b2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_r_smax2_1 : arith2_db.

Lemma smax2_r_smax2_2: forall a b1 b2,
  smax2 a b2 <= smax2 a (smax2 b1 b2).
Proof.
intros.
destruct le_or_lt with (n := b1) (m:= b2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_r_smax2_2 : arith2_db.

Lemma smax2_l_smax2_1: forall a1 a2 b,
  smax2 a1 b <= smax2 (smax2 a1 a2) b.
Proof.
intros.
destruct le_or_lt with (n := a1) (m:= a2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_l_smax2_1 : arith2_db.

Lemma smax2_l_smax2_2: forall a1 a2 b,
  smax2 a2 b <= smax2 (smax2 a1 a2) b.
Proof.
intros.
destruct le_or_lt with (n := a1) (m:= a2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_l_smax2_1 : arith2_db.

Lemma smax2_smax2_l1_r1: forall a1 a2 b1 b2,
  smax2 a1 b1 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
eapply le_n_S.
unfold smax2.
unfold max2.
rewrite <- succ_max_distr.
fold (smax2 a1 b1).
fold (smax2 a1 b1).
eapply le_n_S.
destruct le_or_lt with (n := a1) (m := a2); 
destruct le_or_lt with (n := b1) (m := b2).

setoid_rewrite max_r at 3; auto.
setoid_rewrite max_r at 3; auto.
eapply max_le_compat; eauto.

setoid_rewrite max_r at 3; auto.
setoid_rewrite max_l at 3; auto.
eapply max_le_compat; eauto.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_r at 3; auto.
eapply max_le_compat; eauto.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_l at 3; auto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve smax2_smax2_l1_r1 : arith2_db.

Lemma smax2_smax2_l1_l2: forall a1 a2 b1 b2,
  smax2 a1 a2 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
eapply le_n_S.
unfold smax2.
unfold max2.
rewrite <- succ_max_distr.
eapply le_n_S.
destruct le_or_lt with (n := a1) (m := a2); 
destruct le_or_lt with (n := b1) (m := b2).


setoid_rewrite max_r at 3; auto.
setoid_rewrite max_r at 3; auto.
setoid_rewrite max_r at 1; auto.
eapply le_max_l.

setoid_rewrite max_r at 3; auto.
setoid_rewrite max_l at 3; auto.
setoid_rewrite max_r at 1; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_r at 3; auto.
setoid_rewrite max_l at 1; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_l at 3; auto.
setoid_rewrite max_l at 1; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve smax2_smax2_l1_l2 : arith2_db.

Lemma smax2_smax2_l1_l1: forall a1 a2 b1 b2,
  smax2 a1 a1 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
eapply le_n_S.
unfold smax2.
unfold max2.
rewrite <- succ_max_distr.
eapply le_n_S.
rewrite max_id.

destruct le_or_lt with (n := a1) (m := a2); 
destruct le_or_lt with (n := b1) (m := b2).


setoid_rewrite max_r at 2; auto.
setoid_rewrite max_r at 2; auto.
eapply le_trans.
eauto.
eapply le_max_l.

setoid_rewrite max_r at 2; auto.
setoid_rewrite max_l at 2; auto.
eapply le_trans.
eauto.
eapply le_max_l.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 2; auto.
setoid_rewrite max_r at 2; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 2; auto.
setoid_rewrite max_l at 2; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve smax2_smax2_l1_l1 : arith2_db.



Lemma smax2_smax2_1_2: forall a1 a2 b1 b2,
  smax2 a1 b2 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
setoid_rewrite smax2_comm at 4.
eauto with arith2_db.
Qed.
Hint Resolve smax2_smax2_1_2: arith2_db.

(******************************************************************************
 * SMAX3
 *)

Lemma smax3_comm_1: forall a b c,
  smax3 a b c = smax3 c b a
.
Proof.
intros.
unfold smax3.
unfold max3.
eapply eq_S.
rewrite max_assoc.
setoid_rewrite max_comm at 3.
setoid_rewrite max_comm at 2.
auto.
Qed.
Hint Resolve smax3_comm_1: arith2_db.

Lemma smax3_comm_2: forall a b c,
  smax3 a b c = smax3 c a b
.
Proof.
intros.
unfold smax3.
unfold max3.
eapply eq_S.
rewrite max_assoc.
rewrite max_comm.
auto.
Qed.
Hint Resolve smax3_comm_2: arith2_db.

Lemma smax3_lt1: forall a b c,
  a < smax3 a b c.
Proof.
intros.
unfold smax3.
unfold max3.
unfold lt.
eapply le_n_S.
eapply le_max_l.
Qed.
Hint Resolve smax3_lt1 : arith2_db.

Lemma smax3_lt2: forall a b c,
  b < smax3 a b c.
Proof.
intros.
rewrite smax3_comm_2.
rewrite smax3_comm_1.
auto with arith2_db.
Qed.
Hint Resolve smax3_lt2 : arith2_db.

Lemma smax3_lt3: forall a b c,
  c < smax3 a b c.
Proof.
intros.
rewrite smax3_comm_2.
auto with arith2_db.
Qed.
Hint Resolve smax3_lt3 : arith2_db.

(******************************************************************************
 * SMAX3-SMAX2 / SMAX2-SMAX3
 *)

Lemma smax3_smax2_lt1: forall a b c d,
  a < smax2 (smax3 a b c) d.
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax3_smax2_lt1 : arith2_db.

Lemma smax3_smax2_lt2: forall a b c d,
  b < smax2 (smax3 a b c) d.
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax3_smax2_lt2 : arith2_db.

Lemma smax3_smax2_lt3: forall a b c d,
  c < smax2 (smax3 a b c) d.
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax3_smax2_lt3 : arith2_db.

Lemma smax3_smax2_lt4: forall a b c d,
  a < smax2 d (smax3 a b c).
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax3_smax2_lt4 : arith2_db.

Lemma smax3_smax2_lt5: forall a b c d,
  b < smax2 d (smax3 a b c).
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax3_smax2_lt5 : arith2_db.

Lemma smax3_smax2_lt6: forall a b c d,
  c < smax2 d (smax3 a b c).
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax3_smax2_lt6 : arith2_db.

Lemma smax2_smax3_lt1: forall a b c d,
  a < smax3 (smax2 a b) c d.
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax2_smax3_lt1 : arith2_db.

Lemma smax2_smax3_lt2: forall a b c d,
  b < smax3 (smax2 a b) c d.
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax2_smax3_lt2 : arith2_db.

Lemma smax2_smax3_lt3: forall a b c d,
  a < smax3 c (smax2 a b) d.
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax2_smax3_lt3 : arith2_db.

Lemma smax2_smax3_lt4: forall a b c d,
  b < smax3 c (smax2 a b) d.
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax2_smax3_lt4 : arith2_db.

Lemma smax2_smax3_lt5: forall a b c d,
  a < smax3 c d (smax2 a b).
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax2_smax3_lt5 : arith2_db.

Lemma smax2_smax3_lt6: forall a b c d,
  b < smax3 c d (smax2 a b).
Proof.
eauto with arith arith2_db.
Qed.
Hint Resolve smax2_smax3_lt6 : arith2_db.

(******************************************************************************
 * SPLUS2
 *)

Lemma splus2_comm: forall a b,
  splus2 a b = splus2 b a.
Proof.
intros.
unfold splus2.
eauto with arith.
Qed.
Hint Resolve splus2_comm : arith2_db.

Lemma splus2_lt1: forall a b,
  a < splus2 a b.
Proof.
intros.
unfold splus2.
eauto with arith.
Qed.
Hint Resolve splus2_lt1 : arith2_db.

Lemma splus2_lt2: forall a b,
  b < splus2 a b.
Proof.
intros.
rewrite splus2_comm.
eapply splus2_lt1.
Qed.
Hint Resolve splus2_lt2 : arith2_db.

Lemma splus2_splus2_lt1: forall a b c,
  a < splus2 (splus2 a b) c
.
Proof.
intros.
unfold splus2.
simpl.
eauto with arith.
Qed.
Hint Resolve splus2_splus2_lt1 : arith2_db.

Lemma splus2_splus2_lt2: forall a b c,
  b < splus2 (splus2 a b) c
.
Proof.
intros.
setoid_rewrite splus2_comm at 2.
eauto with arith2_db.
Qed.
Hint Resolve splus2_splus2_lt2 : arith2_db.

Lemma splus3_comm_1: forall a b c,
  splus3 a b c = splus3 c b a.
Proof.
intros.
unfold splus3.
eapply eq_S.
setoid_rewrite <- plus_comm at 2.
setoid_rewrite plus_comm at 1.
setoid_rewrite plus_assoc at 1.
eauto with arith.
Qed.
Hint Resolve splus3_comm_1 : arith2_db.


Lemma splus3_comm_2: forall a b c,
  splus3 a b c = splus3 c a b.
Proof.
intros.
unfold splus3.
eapply eq_S.
setoid_rewrite plus_comm at 1.
setoid_rewrite plus_assoc at 1.
eauto with arith.
Qed.
Hint Resolve splus3_comm_2 : arith2_db.
