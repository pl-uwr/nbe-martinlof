(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * This module contains definition of auxiliary measure
 *)

Require Import Arith.
Require Import Arith.Max.
Require Import Arith.Lt.
Require Import Arith.Le.
Require Import Arith.Mult.
Require Import NPeano.
Import Nat.

(******************************************************************************
 * Definitions
 *)

Definition max2 a b := max a b.
Definition max3 a b c := max a (max b c).

Definition smax2 a b := S (max2 a b).
Definition smax3 a b c := S (max3 a b c).

(******************************************************************************
 * Facts.
 *)

Create HintDb arithfuncs_db.
Local Create HintDb arith2_db.

(******************************************************************************
 * Mu Measure
 *)
Definition mu a b := S a * S b * smax2 a b * smax2 a b.

(******************************************************************************
 * 
 *)

Fact smax2_lt_1: forall b1 b2,
  b1 < smax2 b1 b2
.
Proof.
intros.
unfold smax2.
unfold lt.
eapply le_n_S.
eapply le_max_l.
Qed.
Hint Resolve smax2_lt_1 : arith2_db.

Fact smax2_lt_2: forall b1 b2,
  b2 < smax2 b1 b2
.
Proof.
intros.
unfold smax2.
unfold lt.
eapply le_n_S.
eapply le_max_r.
Qed.
Hint Resolve smax2_lt_2 : arith2_db.

(* Coq 8.3 *)
Lemma mult_lt_compat_l
     : forall n m p : nat, n < m -> 0 < p -> p * n < p * m.
Proof.
intros.
setoid_rewrite mult_comm.
eapply mult_lt_compat_r; eauto.
Qed.
Hint Resolve mult_lt_compat_l : arith.
 
Lemma mult4_lt1: forall a a' b c d ,
  a < a' ->
  S a * S b * S c * S d < S a' * S b * S c * S d
.
Proof.
intros.
eapply mult_lt_compat_r.
eapply mult_lt_compat_r.
eapply mult_lt_compat_r.
eauto with arith.
eauto with arith.
eauto with arith.
eauto with arith.
Qed.
Hint Resolve mult4_lt1 : artih2_db.

Lemma mult4_le1: forall a a' b c d ,
  a <= a' ->
  S a * S b * S c * S d <= S a' * S b * S c * S d
.
Proof.
intros.
eapply mult_le_compat_r.
eapply mult_le_compat_r.
eapply mult_le_compat_r.
eapply le_n_S.
eauto with arith.
Qed.
Hint Resolve mult4_le1 : artih2_db.

Lemma mult4_lt2: forall a b b' c d ,
  b < b' ->
  S a * S b * S c * S d < S a * S b' * S c * S d
.
Proof.
intros.
eapply mult_lt_compat_r.
eapply mult_lt_compat_r.
eapply mult_lt_compat_l.
eauto with arith.
eauto with arith.
eauto with arith.
eauto with arith.
Qed.
Hint Resolve mult4_lt2 : artih2_db.

Lemma mult4_le2: forall a b b' c d ,
  b <= b' ->
  S a * S b * S c * S d <= S a * S b' * S c * S d
.
Proof.
intros.
eapply mult_le_compat_r.
eapply mult_le_compat_r.
eapply mult_le_compat_l.
eapply le_n_S.
eauto with arith.
Qed.
Hint Resolve mult4_le2 : artih2_db.

Lemma mult4_lt3: forall a b c c' d ,
  c < c' ->
  S a * S b * S c * S d < S a * S b * S c' * S d
.
Proof.
intros.
eapply mult_lt_compat_r.
eapply mult_lt_compat_l.
auto with arith.
simpl.
eauto with arith.
eauto with arith.
Qed.
Hint Resolve mult4_lt3 : artih2_db.

Lemma mult4_le3: forall a b c c' d ,
  c <= c' ->
  S a * S b * S c * S d <= S a * S b * S c' * S d
.
Proof.
intros.
eapply mult_le_compat_r.
eapply mult_le_compat_l.
eapply le_n_S.
eauto with arith.
Qed.
Hint Resolve mult4_le3 : artih2_db.

Lemma mult4_lt4: forall a b c d d',
  d < d' ->
  S a * S b * S c * S d < S a * S b * S c * S d'
.
Proof.
intros.
eapply mult_lt_compat_l.
eauto with arith.
simpl.
eauto with arith.
Qed.
Hint Resolve mult4_lt4 : artih2_db.

Lemma mult4_le4: forall a b c d d',
  d <= d' ->
  S a * S b * S c * S d <= S a * S b * S c * S d'
.
Proof.
intros.
eapply mult_le_compat_l.
eapply le_n_S.
eauto with arith.
Qed.
Hint Resolve mult4_le4 : artih2_db.

Lemma mult4_lt34: forall a b c c',
  c < c' ->
  S a * S b * S c * S c < S a * S b * S c' * S c'
.
Proof.
intros.
eapply lt_trans.
eapply mult4_lt3.
eauto.
eapply mult4_lt4.
eauto.
Qed.
Local Hint Resolve mult4_lt34 : artih2_db.

Lemma mult4_le34: forall a b c c',
  c <= c' ->
  S a * S b * S c * S c <= S a * S b * S c' * S c'
.
Proof.
intros.
eapply le_trans.
eapply mult4_le3.
eauto.
eapply mult4_le4.
eauto.
Qed.
Local Hint Resolve mult4_le34 : artih2_db.


Lemma max_le_1: forall a a' b,
  a < a' -> max a b <= max a' b.
Proof.
intros.
destruct le_or_lt with (n := a') (m := b).
rewrite max_r.
rewrite max_r.
eauto.
eauto.
eapply lt_le_weak.
eapply lt_le_trans.
eauto.
eauto.
setoid_rewrite max_l at 2.
eapply max_lub.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Local Hint Resolve max_le_1 : arith2_db.

Lemma max_lt_1: forall a a' b,
  a <= a' -> max a b <= max a' b.
Proof.
intros.
destruct le_or_lt with (n := a') (m := b).
rewrite max_r.
rewrite max_r.
eauto.
eauto.
eapply le_trans.
eauto.
eauto.
setoid_rewrite max_l at 2.
eapply max_lub.
eauto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve max_lt_1 : arith2_db.

Lemma smax2_lt_le_1: forall a a' b,
  a < a' -> smax2 a b <= smax2 a' b.
Proof.
intros.
unfold smax2.
eapply le_n_S.
eapply max_le_1.
eauto.
Qed.
Hint Resolve smax2_lt_le_1 : arith2_db.

Lemma smax2_le_le_1: forall a a' b,
  a <= a' -> smax2 a b <= smax2 a' b.
Proof.
intros.
unfold smax2.
eapply le_n_S.
eapply max_lt_1.
eauto.
Qed.
Hint Resolve smax2_le_le_1 : arith2_db.

Lemma smax2_comm: forall a b,
  smax2 a b = smax2 b a.
Proof.
intros.
unfold smax2.
unfold max2.
rewrite max_comm.
auto.
Qed.

Lemma smax2_lt_le_2: forall a b b',
  b < b' -> smax2 a b <= smax2 a b'.
Proof.
intros.
setoid_rewrite smax2_comm.
eauto with arith2_db.
Qed.
Hint Resolve smax2_lt_le_2: arith2_db.

Lemma smax2_le_le_2: forall a b b',
  b <= b' -> smax2 a b <= smax2 a b'.
Proof.
intros.
setoid_rewrite smax2_comm.
eauto with arith2_db.
Qed.
Hint Resolve smax2_le_le_2 : arith2_db.

Lemma smax2_r_smax2_1: forall a b1 b2,
  smax2 a b1 <= smax2 a (smax2 b1 b2).
Proof.
intros.
destruct le_or_lt with (n := b1) (m:= b2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_r_smax2_1 : arith2_db.

Lemma smax2_r_smax2_2: forall a b1 b2,
  smax2 a b2 <= smax2 a (smax2 b1 b2).
Proof.
intros.
destruct le_or_lt with (n := b1) (m:= b2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_r_smax2_2 : arith2_db.

Lemma smax2_l_smax2_1: forall a1 a2 b,
  smax2 a1 b <= smax2 (smax2 a1 a2) b.
Proof.
intros.
destruct le_or_lt with (n := a1) (m:= a2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_l_smax2_1 : arith2_db.

Lemma smax2_l_smax2_2: forall a1 a2 b,
  smax2 a2 b <= smax2 (smax2 a1 a2) b.
Proof.
intros.
destruct le_or_lt with (n := a1) (m:= a2).
eauto with arith2_db.
eauto with arith2_db.
Qed.
Hint Resolve smax2_l_smax2_1 : arith2_db.

Lemma smax2_smax2_l1_r1: forall a1 a2 b1 b2,
  smax2 a1 b1 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
eapply le_n_S.
unfold smax2.
unfold max2.
rewrite <- succ_max_distr.
fold (smax2 a1 b1).
fold (smax2 a1 b1).
eapply le_n_S.
destruct le_or_lt with (n := a1) (m := a2); 
destruct le_or_lt with (n := b1) (m := b2).

setoid_rewrite max_r at 3; auto.
setoid_rewrite max_r at 3; auto.
eapply max_le_compat; eauto.

setoid_rewrite max_r at 3; auto.
setoid_rewrite max_l at 3; auto.
eapply max_le_compat; eauto.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_r at 3; auto.
eapply max_le_compat; eauto.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_l at 3; auto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve smax2_smax2_l1_r1 : arith2_db.

Lemma smax2_smax2_l1_l2: forall a1 a2 b1 b2,
  smax2 a1 a2 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
eapply le_n_S.
unfold smax2.
unfold max2.
rewrite <- succ_max_distr.
eapply le_n_S.
destruct le_or_lt with (n := a1) (m := a2); 
destruct le_or_lt with (n := b1) (m := b2).


setoid_rewrite max_r at 3; auto.
setoid_rewrite max_r at 3; auto.
setoid_rewrite max_r at 1; auto.
eapply le_max_l.

setoid_rewrite max_r at 3; auto.
setoid_rewrite max_l at 3; auto.
setoid_rewrite max_r at 1; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_r at 3; auto.
setoid_rewrite max_l at 1; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 3; auto.
setoid_rewrite max_l at 3; auto.
setoid_rewrite max_l at 1; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve smax2_smax2_l1_l2 : arith2_db.

Lemma smax2_smax2_l1_l1: forall a1 a2 b1 b2,
  smax2 a1 a1 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
eapply le_n_S.
unfold smax2.
unfold max2.
rewrite <- succ_max_distr.
eapply le_n_S.
rewrite max_id.

destruct le_or_lt with (n := a1) (m := a2); 
destruct le_or_lt with (n := b1) (m := b2).


setoid_rewrite max_r at 2; auto.
setoid_rewrite max_r at 2; auto.
eapply le_trans.
eauto.
eapply le_max_l.

setoid_rewrite max_r at 2; auto.
setoid_rewrite max_l at 2; auto.
eapply le_trans.
eauto.
eapply le_max_l.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 2; auto.
setoid_rewrite max_r at 2; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.

setoid_rewrite max_l at 2; auto.
setoid_rewrite max_l at 2; auto.
eapply le_max_l.
eapply lt_le_weak.
eauto.
eapply lt_le_weak.
eauto.
Qed.
Hint Resolve smax2_smax2_l1_l1 : arith2_db.


(*
Lemma smax2_smax2_1_2: forall a1 a2 b1 b2,
  smax2 a1 b2 < smax2 (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
setoid_rewrite smax2_comm at 4.
eauto with arith2_db.
Qed.
Hint Resolve smax2_smax2_1_2: arith2_db.
*)

(******************************************************************************
 * 
 *)

Ltac mu_measure_solver :=
  solve [ eauto with mu_measure_db ]
.

(******************************************************************************
 * 
 *)


Fact mu_comm: forall a b, mu a b = mu b a.
Proof.
intros.
unfold mu.
unfold smax2.
unfold max2.
setoid_rewrite max_comm at 1.
setoid_rewrite max_comm at 2.
setoid_rewrite <- mult_assoc at 1.
setoid_rewrite <- mult_assoc at 2.
setoid_rewrite mult_comm at 2.
auto.
Qed.
Hint Resolve mu_comm : mu_measure_db.

Fact mu_lt_l: forall a a' b, a < a' ->  mu a b < mu a' b.
Proof.
intros.
unfold mu.
unfold smax2.
unfold max2.

eapply lt_le_trans.

eapply mult4_lt1.
eauto.

eapply mult4_le34.
eapply max_le_1.
eauto.
Qed.
Hint Resolve mu_lt_l : mu_measure_db.

Fact mu_lt_r: forall a b b', b < b' -> mu a b < mu a b'.
Proof.
intros.
setoid_rewrite mu_comm.
apply mu_lt_l.
auto.
Qed.
Hint Resolve mu_lt_r : mu_measure_db.

Fact mu_lt_smax_1: forall a b1 b2, mu a b1 < mu a (smax2 b1 b2).
Proof.
intros.
unfold mu.
eapply lt_le_trans.
eapply mult4_lt2 with (b' := (smax2 b1 b2)).
eauto with arith2_db.

eapply mult4_le34.
eapply le_S_n.
eauto with arith2_db.
Qed.
Hint Resolve mu_lt_smax_1 : mu_measure_db.

Fact mu_lt_smax_2: forall a b1 b2, mu a b2 < mu a (smax2 b1 b2).
Proof.
intros.
rewrite smax2_comm.
mu_measure_solver.
Qed.
Hint Resolve mu_lt_smax_2: mu_measure_db.

Fact mu_lt_smax_smax_l1_r1: forall a1 a2 b1 b2, mu a1 b1 < mu (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
unfold mu.
eapply lt_le_trans.
eapply mult4_lt1 with (a' := (smax2 a1 a2)).
eauto with arith2_db.
eapply le_trans.
eapply mult4_le2 with (b' := (smax2 b1 b2)).
eapply lt_le_weak.
eauto with arith2_db.
eapply mult4_le34.
eapply lt_le_weak.
eapply lt_S_n.
eauto with arith2_db.
Qed.
Hint Resolve mu_lt_smax_smax_l1_r1 : mu_measure_db.

Fact mu_lt_smax_smax_l1_r2: forall a1 a2 b1 b2, mu a1 b2 < mu (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
setoid_rewrite smax2_comm at 2.
mu_measure_solver.
Qed.
Hint Resolve mu_lt_smax_smax_l1_r2: mu_measure_db.

Fact mu_lt_smax_smax_l2_r1: forall a1 a2 b1 b2, mu a2 b1 < mu (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
setoid_rewrite smax2_comm at 1.
mu_measure_solver.
Qed.
Hint Resolve mu_lt_smax_smax_l2_r1: mu_measure_db.

Fact mu_lt_smax_smax_l2_r2: forall a1 a2 b1 b2, mu a2 b2 < mu (smax2 a1 a2) (smax2 b1 b2).
Proof.
intros.
setoid_rewrite smax2_comm at 2.
setoid_rewrite smax2_comm at 1.
mu_measure_solver.
Qed.
Hint Resolve mu_lt_smax_smax_l2_r2: mu_measure_db.

