(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Auxiliary tactics
 *)

Require Import Program.

Ltac auto_inversion_on Hs :=
  let A := fresh "A" in
  let B := fresh "B" in
  let C := fresh "C" in
  let D := fresh "D" in
  let E := fresh "E" in
  let F := fresh "F" in
  let G := fresh "G" in
  let H := fresh "F" in
  let I := fresh "G" in
  let J := fresh "F" in
  let K := fresh "G" in
  let IH := fresh "IH" in
  case Hs; try solve
    [
      intros IH; discriminate IH
    | intros A IH; discriminate IH
    | intros A B IH; discriminate IH
    | intros A B C IH; discriminate IH
    | intros A B C D IH; discriminate IH
    | intros A B C D E IH; discriminate IH
    | intros A B C D E F IH; discriminate IH
    | intros A B C D E F G IH; discriminate IH
    | intros A B C D E F G H IH; discriminate IH
    | intros A B C D E F G H I IH; discriminate IH
    | intros A B C D E F G H I J IH; discriminate IH
    | intros A B C D E F G H I J K IH; discriminate IH
    ]
.

Ltac auto_inversion :=
  match goal with
    | [ H:_ |- _ ] =>  let T := auto_inversion_on H in progress T
  end
.


Ltac clear_inversion H :=
  inversion H; subst; clear H; clear_dups.
