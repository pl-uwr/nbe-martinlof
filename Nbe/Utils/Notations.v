(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Auxiliary notations
 *)

Notation "f $ x" := (f x) (at level 100, only parsing, right associativity).

Reserved Notation "Gamma |-- "
 (no associativity, at level 75).

Reserved Notation "Gamma |-- e : t"
 (no associativity, at level 75, e at next level).

Reserved Notation "Gamma |-- e"
  (no associativity, at level 75, e at next level).

Reserved Notation "Gamma |-- e1 === e2"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |-- e1 === e2 : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |-- e1 |> e2"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |-- e1 |> e2 : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "|-- { e1 } === { e2 }"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |-- [ e ] : t"
 (no associativity, at level 75, e at next level).

Reserved Notation "Gamma |-- [ e1 ] === [ e2 ] : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |-- [ e1 ] |> [ e2 ] : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma << n >>  |-- "
 (no associativity, at level 75).

Reserved Notation "Gamma << n >> |-- e : t"
 (no associativity, at level 75, e at next level).

Reserved Notation "Gamma << n >> |-- e"
  (no associativity, at level 75, e at next level).

Reserved Notation "Gamma << n >> |-- e1 === e2"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma << n >> |-- e1 === e2 : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma << n >> |-- [ e ] : t"
 (no associativity, at level 75, e at next level).

Reserved Notation "Gamma << n >> |-- [ e1 ] === [ e2 ] : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "<< n >> |-- { e1 } === { e2 }"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |= "
 (no associativity, at level 75).

Reserved Notation "Gamma |= e : t"
 (no associativity, at level 75, e at next level).

Reserved Notation "Gamma |= e"
  (no associativity, at level 75, e at next level).

Reserved Notation "Gamma |= e1 === e2"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |=  e1 === e2 : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |= [ e ] : t"
 (no associativity, at level 75, e at next level).

Reserved Notation "Gamma |= [ e1 ] === [ e2 ] : t"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "|= { e1 } === { e2 }"
 (no associativity, at level 75, e1, e2 at next level).

Reserved Notation "Gamma |--v i : t "
 (no associativity, at level 75, i,t at next level).

Reserved Notation "d === d' #in# R" (at level 75, no associativity).

Reserved Notation "R1 =~= R2" (at level 75, no associativity).

Reserved Notation "G ,, t" 
 (left associativity, at level 73, t at next level).

Reserved Notation "[ d ] === [ d' ] #in# Gamma"  
  (at level 75, no associativity,
    format "[ d ] === [ d' ]  '#in#'  Gamma " ).

