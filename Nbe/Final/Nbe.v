(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.CompletenessOfNbe.
Require Import Nbe.SoundnessOfNbe.


(******************************************************************************
 *)
Theorem NBE_Completeness: forall Gamma t0 t1 T,
  Gamma |-- t0 === t1 : T ->
  exists v, Nbe T Gamma t0 v /\ Nbe T Gamma t1 v.
Proof.
intros.
apply NBE_CompletenessSem.
apply Valid_for_WtTmEq.
assumption.
Qed.

(******************************************************************************
 *)
Theorem NBE_Soundness: forall Gamma t T,
  Gamma |-- t : T ->
  exists n, Nbe T Gamma t n /\ Gamma |-- t === n : T
.
Proof.
apply Nbe_Soundness'.
Qed.


