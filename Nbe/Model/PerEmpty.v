(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Definition of PER relation for Empty type
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNe.

(******************************************************************************
 * PER for Empty
 *
 * It contains only neutral-like values
 *)

Inductive PerEmpty : relation D :=
| PerEmpty_ne: forall e e',
  e === e' #in# PerNe ->
  Dup Dempty e === Dup Dempty e' #in# PerEmpty
.
Hint Constructors PerEmpty.

(******************************************************************************
 *)
Instance PerEmpty_is_PER: PER (InRel PerEmpty).
constructor 1; red; intros; auto.
clear_inversion H.
constructor 1.
intuition.

clear_inversion H.
clear_inversion H0.
constructor 1.
apply PER_Transitive with e'; eauto.
Qed.
Hint Resolve PerEmpty_is_PER.
