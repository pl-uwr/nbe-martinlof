(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * PER relation for natural numbers
 **)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.
Require Import Nbe.Model.PerNe.

(******************************************************************************
 * PER for Natural numbers
 *
 * Inductive definition of natural numbers and neutral-like values
 *)

Inductive PerNat : relation D :=
| PerNat_0: 
  D0 === D0 #in# PerNat

| PerNat_S: forall d0 d1,
  d0 === d1 #in# PerNat ->
  DS d0 === DS d1 #in# PerNat

| PerNat_ne: forall e e',
  e === e' #in# PerNe ->
  Dup Dnat e === Dup Dnat e' #in# PerNat
.
Hint Constructors PerNat.

(******************************************************************************
 *)
Instance PerNat_is_PER : PER (InRel PerNat).
Proof.
constructor 1; red; intros.
{
induction H; eauto.
symmetry in H. eauto.
}

{
revert z H0.
induction H; intros; auto.
+{
clear_inversion H0.
eauto.
}
+{
clear_inversion H0.
constructor 3. 
transitivity e'; eauto.
}
}
Qed.
Hint Resolve PerNat_is_PER.

