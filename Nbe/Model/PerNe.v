(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * PER relation for neutral-like values
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

(******************************************************************************
 * PER for Neutral elements
 *
 * Two values are in relation if they have the same normal form 
 *)
Inductive PerNe : relation DNe :=
| PerNe_intro: forall e e',
  (forall m, exists n, RbNe m e n /\ RbNe m e' n) -> e === e' #in# PerNe
.
Hint Constructors PerNe.

(******************************************************************************
 *)
Instance PerNe_is_PER : PER (InRel PerNe).
Proof.
constructor 1.
red; intros; auto.
destruct H.
apply PerNe_intro.
firstorder.

red; intros x y z Hxy Hyz.
inversion Hxy; simpl.
inversion Hyz; simpl.
subst.
constructor 1; intros.
specialize H with m.
specialize H2 with m.
program_simpl.
replace H with H2 in *.
firstorder.
symmetry; eapply RbNe_deter; eauto.
Qed.
Hint Resolve PerNe_is_PER.

