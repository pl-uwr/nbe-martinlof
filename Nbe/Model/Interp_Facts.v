(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Basic facts about interpretation of types (InterpType) 
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNf.
Require Import Nbe.Model.PerNe.
Require Import Nbe.Model.PerNat.
Require Import Nbe.Model.PerUnit.
Require Import Nbe.Model.PerEmpty.
Require Import Nbe.Model.Interp.
Require Import Nbe.Model.PerUniv.
Require Import Nbe.Model.PerNeUniv.
Require Import Nbe.Model.InterpUniv.
Require Import Nbe.Model.InterpUniv_Facts.

(******************************************************************************
 * InterpType gives PER relations
 *)
Lemma InterpType_pers: forall d R, 
  InterpType d R -> PER R
.
Proof.
intros.
induction H; eauto.
Qed.
Hint Resolve InterpType_pers.

(******************************************************************************
 * Embeds universe of small types
 *)
Lemma InterpUniv_implies_InterpType: forall d R,
   InterpUniv d R ->
   InterpType d R
.
Proof.
intros.
induction H; eauto.
Qed.
Hint Resolve InterpUniv_implies_InterpType.


(******************************************************************************
 * Respects abstract classes of PerUniv
 *)
Lemma InterpType_conservative_extends_InterpUniv: forall d R1 R2,
   d === d #in# PerUniv ->
   InterpUniv d R1 -> InterpType d R2 -> InterpUniv d R2
.
Proof.
intros d R1 R2 HPerUniv.
intros.
generalize dependent  R2.
induction H; intros R2 HR2; clear_inversion HR2; eauto.
(*Fun*)
{
clear_inversion HPerUniv.
assert (PA =~= PA0) as HX1 by eauto.

eapply InterpUniv_fun; eauto.
intros.

clear_inversion H0; clear_inversion po_ro.
edestruct ro_resp_ex with (a := a) as [PB']. eapply HX1; eauto.

eapply H2 with (PB := PB'); eauto.
eapply HX1; eauto.
}

Qed.
Hint Resolve InterpType_conservative_extends_InterpUniv.

(******************************************************************************
 * Can be truncated to small universe of types
 *)
Lemma InterpUniv_from_InterpType: forall d P,
  d === d #in# PerUniv ->
  InterpType d P ->
  InterpUniv d P
.
Proof.
intros.

assert (exists R, InterpUniv d R) as HX1 by eauto.
destruct HX1 as [R].
assert (InterpType d R) as HX2 by eauto.
eapply InterpType_conservative_extends_InterpUniv.
eauto.
eauto.
eauto.
Qed.

(******************************************************************************
 * PerType also embeds universe of small types
 *)
Lemma PerUniv_implies_PerType: forall d1 d2,
   d1 === d2 #in# PerUniv ->
   d1 === d2 #in# PerType
.
Proof.
intros.
induction H; eauto.

(* Prod *)
+{
eapply PerType_fun; eauto; intros.
}
Qed.
Hint Resolve PerUniv_implies_PerType.

(******************************************************************************
 *)
Lemma InterpType_RelProd_extdeter: forall DA DF PA PF PA0 PF0,
 InterpType DA PA ->
 ProperPerProd PA PF ->
 (forall (a : D) (PB : relation D) (DB : D),
       a === a #in# PA -> App DF a DB -> PF a PB -> InterpType DB PB) ->
 (forall (a : D) (PB : relation D) (DB : D),
       a === a #in# PA ->
       App DF a DB ->
       PF a PB -> forall R2 : relation D, InterpType DB R2 -> PB =~= R2) ->
  (forall a : D, a === a #in# PA -> exists DB : D, App DF a DB) ->
  (forall R2 : relation D, InterpType DA R2 -> PA =~= R2) ->
  InterpType DA PA0 ->
  ProperPerProd PA0 PF0 ->
  (forall (a : D) (PB : relation D) (DB : D),
       a === a #in# PA0 -> App DF a DB -> PF0 a PB -> InterpType DB PB) ->
  (forall a : D, a === a #in# PA0 -> exists DB : D, App DF a DB) ->
 RelProd PA PF =~= RelProd PA0 PF0
.
Proof.
intros.
assert (PA =~= PA0) as HX1 by eauto.

red; split; intro.
-{
constructor; intros.

assert (a0 === a1 #in# PA) as HX2.
eapply HX1; eauto.

clear_inversion H9.
rename x into f.
rename y into f'.
specialize (H11 _ _ HX2).
decompose record H11.
rename x into y0.
rename x0 into y1.
rename x1 into Y.

destruct H6.
destruct po_ro.

destruct ro_resp_ex with (a := a0) as [G].
eauto.


exists y0; exists y1; exists G.
repeat split; eauto.
+{

destruct H8 with (a := a0) as [DB0]; eauto.
destruct H8 with (a := a1) as [DB1]; eauto.
eapply H2 with (a:= a0) (PB := Y) (R2 := G); eauto.
}

}

-{
constructor; intros.

assert (a0 === a1 #in# PA0) as HX2.
eapply HX1; eauto.

clear_inversion H9.
rename x into f.
rename y into f'.
specialize (H11 _ _ HX2).
decompose record H11.
rename x into y0.
rename x0 into y1.
rename x1 into Y.

destruct H0.
destruct po_ro.

destruct ro_resp_ex with (a := a0) as [G].
eauto.


exists y0; exists y1; exists G.
repeat split; eauto.
+{

destruct H8 with (a := a0) as [DB0]; eauto.
destruct H8 with (a := a1) as [DB1]; eauto.

assert (G =~= Y) as HX3.
eapply H2 with (a := a0); eauto.

eapply HX3; eauto.
}

}
Qed.

(******************************************************************************
 * InterpType is extensionally deterministic
 *)
Lemma InterpType_extdeter: forall d R1,
  InterpType d R1 -> forall R2, InterpType d R2 -> R1 =~= R2.
Proof.
intros d R1 HR1.
induction HR1; intros;
 try solve
   [ clear_inversion H; reflexivity
   | clear_inversion H0; reflexivity
   ]; intros; eauto.

+{ (* Prod *)
clear_inversion H3.
eapply InterpType_RelProd_extdeter; eauto.
}
Qed.
Hint Resolve InterpType_extdeter.

(******************************************************************************
 * InterpType respects PerType
 *)
Lemma InterpType_resp_PerUniv: forall DA DB P,
  InterpType DA P ->
  DA === DB #in# PerUniv ->
  InterpType DB P
.
Proof.
intros.
apply InterpUniv_implies_InterpType; eauto.
assert (exists PP, InterpUniv DA PP) by eauto.
destruct H1.
eauto.
Qed.
Hint Resolve InterpType_resp_PerUniv.
 
(******************************************************************************
 * PerType does not brake equivalence in PerUniv
 *)
Lemma PerType_conservative_extends_PerUniv: forall d1,
   d1 === d1 #in# PerUniv -> forall d2,
   d1 === d2 #in# PerType ->
   d1 === d2 #in# PerUniv.
Proof.
intros d1 H.
induction H; intros; try solve
  [ clear_inversion H; eauto
  | clear_inversion H0; eauto
  ].

(* Prod *)
+{
clear_inversion H5; auto.
rename DF'0 into DF''.
rename DA'0 into DA''.

assert (PA =~= PA0) as HX1 by eauto.

eapply PerUniv_fun; eauto.

{
intros.
edestruct H11 as [DB''].
eapply HX1; eauto.
eauto.
}

{
intros.

assert (P =~= PA) as HX2 by eauto.
assert (PER P) as HX3 by eauto.

rename DB0 into DB'.
rename DB1 into DB''.
destruct H1 with (a := a0) as [DB].
{
eapply HX2; eauto.
eapply Per_domL; eauto.
}

assert (DB === DB' #in# PerUniv) as HX4.
{
eapply H3; eauto.
eapply HX2.
eapply Per_domL; eauto.
}

eapply H4; eauto.

eapply HX2.
eapply Per_domL; eauto.
}
}
Qed.
 
(******************************************************************************
 * InterpType respects PerType
 *)
Lemma InterpType_resp_PerType: forall DA DB P,
  InterpType DA P ->
  DA === DB #in# PerType ->
  InterpType DB P
.
Proof.
intros d0 d1 y Hy.
revert d1.
induction Hy; intros; try solve
   [ clear_inversion H; eauto
   | clear_inversion H0; eauto
   | clear_inversion H; eauto; try clear_inversion H; eauto ].

(*Prod*)
+{
clear_inversion H3.

eapply InterpType_fun; eauto.
(**)
-{
intros a PR y1 Ha; intros.
cut (PA0 =~= PA).
intro HPA.
destruct H8 with a as [y0 Hy0]; auto.
apply HPA; auto.
apply H1 with a y0; auto.
apply H11 with a a PA; eauto.
(* apply HPA; auto. *)
apply InterpType_extdeter with DA; auto.
}
(**)
-{
intros.
apply H9.
cut (PA0 =~= PA).
intro HPA; apply HPA; auto.
apply InterpType_extdeter with DA; auto.
}
}
Qed.
Hint Resolve InterpType_resp_PerType.

(******************************************************************************
 * Inversion of PerType
 *)
Lemma Inversion_of_PerType_fun: forall DA DF D',
 Dfun DA DF === D' #in# PerType ->
 exists DA' DF' PA,
   D' = Dfun DA' DF' /\
   InterpType DA PA /\
   DA === DA' #in# PerType /\
   (forall a : D, a === a #in# PA -> exists DB : D, App DF a DB) /\
   (forall a : D, a === a #in# PA -> exists DB : D, App DF' a DB) /\
   (forall (a0 a1 DB0 DB1 : D) (P : relation D),
        InterpType DA P ->
        a0 === a1 #in# P ->
        App DF a0 DB0 -> App DF' a1 DB1 -> DB0 === DB1 #in# PerType)
.
Proof.
intros.
clear_inversion H.
(* Types *)
+{

exists DA'; exists DF'; exists PA.
repeat split; intros; eauto.
}
Qed.

Ltac inversion_of_pertype_fun H :=
match type of H with
| Dfun ?DA ?DF === ?D' #in# PerType =>
  let DA' := fresh "DA'" in
  let DF' := fresh "DF'" in 
  let PA := fresh "PA" in
  let H1 := fresh "H" in
  let H2 := fresh "H" in
  let H3 := fresh "H" in
  let H4 := fresh "H" in
  let H5 := fresh "H" in
  let H6 := fresh "H" in
  destruct (Inversion_of_PerType_fun H) as [DA' [DF' [PA [H1 [H2 [H3 [H4 [H5 H6] ] ] ] ] ] ] ];
  try (subst D')
end.


Ltac inversion_of_pertype H :=
match type of H with
| Dfun ?DA ?DF === ?D' #in# PerType =>
  inversion_of_pertype_fun H
| ?D === ?D' #in# PerType =>
  inversion H; try (subst D); try (subst D')
end
.

Ltac clear_inversion_of_pertype H :=
  inversion_of_pertype H; clear H.


(******************************************************************************
 * Inversion of PerType
 *)
Lemma Inversion_of_InterpType_fun: forall DA DF PT,
  InterpType (Dfun DA DF) PT ->
  exists PA PF, 
  PT = RelProd PA PF /\
  InterpType DA PA /\
  ProperPerProd PA PF /\
  (forall (a : D) (PB : relation D) (DB : D),
     a === a #in# PA ->  App DF a DB -> PF a PB -> InterpType DB PB) /\
  (forall a : D, a === a #in# PA -> exists DB : D, App DF a DB)
.
Proof.
intros.
clear_inversion H.
+{
exists PA; exists PF.
repeat (split; auto); eauto.
}
Qed.

Ltac inversion_of_interptype_fun H :=
match type of H with
| InterpType (Dfun ?DA ?DF) ?PT =>
  let PA := fresh "PA" in
  let PF := fresh "PF" in 
  let H1 := fresh "H" in
  let H2 := fresh "H" in
  let H3 := fresh "H" in
  let H4 := fresh "H" in
  let H5 := fresh "H" in
  destruct (Inversion_of_InterpType_fun H) as [PA [PF [H1 [H2 [H3 [H4 H5 ] ] ] ] ] ];
  try (subst PT)
end.

Ltac inversion_of_interptype H :=
match type of H with
| InterpType (Dfun ?DA ?DF) ?PT =>
  inversion_of_interptype_fun H
| InterpType ?D ?PT =>
  inversion H; try (subst PT); try (subst D)
end.

Ltac clear_inversion_of_interptype H :=
  inversion_of_interptype H; clear H.

(******************************************************************************
 *)
Instance PerType_is_PER : PER (InRel PerType).
constructor 1; red. 

+{ (* Symmetry *)
intros.
induction H; auto.
-{
eapply PerType_ne.
symmetry; eauto.
}

-{
eapply PerType_fun; eauto.
intros.

assert (PA =~= P) as HX1 by eauto.

eapply H4; eauto.
assert (PER PA) by eauto.
symmetry; eauto.
eapply HX1; eauto.
}

}

+{
intros x y z.
intro Hxy.
revert z.
induction Hxy; intros z Hyz; clear_inversion Hyz; eauto.
-{
eapply PerType_ne.
eapply PER_Transitive; eauto.
}

-{
assert (PA =~= PA0) as HX1 by eauto.
eapply PerType_fun; eauto.
{
intros.
edestruct H10.
eapply HX1; eauto.
eauto.
}

{
intros.

assert (P =~= PA0) as HX2 by eauto.
assert (PER P) as HX3 by eauto.

rename DF'0 into DF''.
rename DB1 into DB2.
edestruct H2 with (a := a1) as [DB1].
{
eapply HX1; eapply HX2; eauto.
eapply Per_domR; eauto.
}

assert (DB1 === DB2 #in# PerType)  as HX4 by eauto.
eauto.
}
}
}
Qed.
Hint Resolve PerType_is_PER.

(******************************************************************************
 * PerType is domain for Interp
 *)

(* Auxiliary definition: used as parameter for InterpType_fun constructor.
 * It is a place where we rely on impredicativity
 *)
Definition InterpType_RelProd_F dF da P := exists DB, App dF da DB /\ InterpType DB P.

Lemma InterpType_dom_PerType: forall d, 
  d === d #in# PerType -> exists y, InterpType d y.
Proof.
intros.
induction H; eauto.

+{
exists (RelProd PA (InterpType_RelProd_F DF)).
eapply InterpType_fun; eauto.
{
constructor.
{
constructor.
{
intros.
red in H6; red.
decompose record H6.
rename x into DB0.
edestruct H1 with (a := a1) as [DB1]; eauto.
edestruct H2 with (a := a1) as [DB1']; eauto.

eexists; split; eauto.
eapply InterpType_resp_PerType; eauto.
eapply PER_Transitive with (DB1').
eapply H3; eauto.
symmetry.
eapply H3 with (a0 := a1); eauto.
}

{ 
intros.
destruct H2 with (a := a) as [DB']; auto.
destruct H1 with (a := a) as [DB]; auto.
edestruct H4 with (a0 := a) (a1 := a) as [Y]; eauto.
exists Y.
red. exists DB; split; eauto.
eapply InterpType_resp_PerType; eauto.
symmetry; eauto.
}

{
intros.
red in H6; red in H7.
decompose record H6.
decompose record H7.
rename x into DB0. 
rename x0 into DB1.
eapply InterpType_extdeter; eauto.
eapply InterpType_resp_PerType; eauto.
symmetry.
destruct H2 with (a := a1) as [DB1']; eauto.
eapply PER_Transitive with (DB1').
eapply H3; eauto.
symmetry; eapply H3 with (a0 := a1); eauto.
}
}

{
intros.
red in H6.  decompose record H6.
eauto.
}

{
eauto.
}
}

{
intros.
red in H7.
decompose record H7.
rename DB into DB'.
rename x into DB.
eapply InterpType_resp_PerType.
eauto.
eapply H3; eauto.
}
}
Qed.
Hint Resolve InterpType_dom_PerType.

(******************************************************************************
 * Interp is proper functor over PerType
 *)
Definition InterpType_ProperRelOper: ProperRelOper PerType InterpType.
constructor 1; eauto.
Defined.

