(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Relation for universe of small types
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.
Require Import Nbe.Model.PerNe.
Require Import Nbe.Model.InterpUniv.

(******************************************************************************
 * PER Relation for universe
 *)

Inductive PerUniv : relation D :=
| PerUniv_ne: forall e e',
  e === e' #in# PerNe ->
  Dup Duniv e === Dup Duniv e' #in# PerUniv

| PerUniv_nat: 
  Dnat === Dnat #in# PerUniv

| PerUniv_unit:
  Dunit === Dunit #in# PerUniv

| PerUniv_empty:
  Dempty === Dempty #in# PerUniv

| PerUniv_fun: forall DA DF DA' DF' PA,
  DA === DA' #in# PerUniv ->
  InterpUniv DA PA -> 
  (forall a, a === a #in# PA -> exists DB, App DF a DB) ->
  (forall a, a === a #in# PA -> exists DB', App DF' a DB') ->
  (forall a0 a1 DB0 DB1 P , InterpUniv DA P -> a0 === a1 #in# P -> App DF a0 DB0 -> App DF' a1 DB1 ->
    DB0 === DB1 #in# PerUniv
  ) ->
  Dfun DA DF === Dfun DA' DF' #in# PerUniv
.

(******************************************************************************
 *)
Hint Constructors PerUniv.
Scheme PerUniv_dind := Induction for PerUniv Sort Prop.
