(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Relation for environments.
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.
Require Import Nbe.Model.Interp.


(******************************************************************************
 * Relation for environments
 *)

Inductive ValEnv : Cxt -> DEnv -> DEnv -> Prop :=
 | ValEnv_id: 
   [ Did ] === [ Did ] #in# nil

| ValEnv_ext: forall env0 env1 Gamma A d0 d1,
  [env0] === [env1] #in# Gamma -> forall PA,
  InterpTypePer A env0 PA ->
  d0 === d1 #in# PA ->
  [ Dext env0 d0 ] === [ Dext env1 d1 ] #in# Gamma,,A
  
where "[ d ] === [ d' ] #in# Gamma" := (ValEnv Gamma d d')
.
Hint Constructors ValEnv.

(******************************************************************************
 *)
Lemma Context_Extension: forall Gamma A env0 env1 d0 d1,
  (
    [ Dext env0 d0 ] === [Dext env1 d1 ] #in# Gamma,,A <->
    exists PA, InterpTypePer A env0 PA /\ d0 === d1 #in# PA /\ [env0] === [env1] #in# Gamma
  )
.
Proof.
intros.
split; intros.
inversion H; subst.
exists PA; split; auto.

destruct H as [PA].
constructor 2 with PA; intuition.
Qed.

(******************************************************************************
 *)

Definition Context_Extension_l Gamma A env0 env1 d0 d1 :=
  proj1 (Context_Extension Gamma A env0 env1 d0 d1)
.
Hint Resolve Context_Extension_l.

Definition Context_Extension_r Gamma A env0 env1 d0 d1 :=
  proj2 (Context_Extension Gamma A env0 env1 d0 d1)
.
Hint Resolve Context_Extension_r.

