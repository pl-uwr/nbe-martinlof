(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * PER relation for neutral types (type expressions where computations
 * stuck on a free variable).
 *)

Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.PER.

Require Import Nbe.Model.PerNe.

(******************************************************************************
 * PER for neutral types
 *)

Inductive PerNeUniv : relation D :=
| PerNeUniv_ne: forall e e',
  e === e' #in# PerNe ->
  Dup Duniv e === Dup Duniv e' #in# PerNeUniv
.
Hint Constructors PerNeUniv.

(******************************************************************************
 *)
Instance PerNeUniv_is_PER: PER (InRel PerNeUniv).
constructor 1; red; intros; auto.
clear_inversion H.
constructor 1.
intuition.

clear_inversion H.
clear_inversion H0.
constructor 1.
apply PER_Transitive with e'; eauto.
Qed.
Hint Resolve PerNeUniv_is_PER.
