(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)
 
Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import List.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.System.
Require Import Nbe.Syntax.SystemN.


(******************************************************************************
 *)

Theorem SystemN_to_System:
  (forall n Gamma, Gamma << n >> |--                        -> Gamma |-- )  /\
  (forall n Gamma T, Gamma << n >> |-- T                    ->  Gamma |-- T) /\
  (forall n Gamma t T, Gamma << n >> |-- t : T              -> Gamma |-- t : T) /\
  (forall n Gamma s Delta, Gamma << n >> |-- [s] : Delta           ->  Gamma |-- [s] : Delta) /\
  (forall n Gamma Delta, << n >> |-- {Gamma} === {Delta}             ->  |-- {Gamma} === {Delta}) /\
  (forall n Gamma T T', Gamma << n >> |-- T === T'            -> Gamma |-- T === T') /\
  (forall n Gamma t t' T, Gamma << n >> |-- t === t' : T      ->  Gamma |-- t === t' : T )  /\
  (forall n Gamma s s' Delta, Gamma << n >> |-- [s] === [s'] : Delta -> Gamma |-- [s] === [s'] : Delta )
.
Proof.
apply SystemN_ind; intros; eauto.
Qed.

(******************************************************************************
 *)
Theorem System_to_SystemN:
  (forall Gamma, Gamma |--                        -> exists n, Gamma << n >> |-- )  /\
  (forall Gamma T, Gamma |-- T                    -> exists n, Gamma << n >> |-- T) /\
  (forall Gamma t T, Gamma |-- t : T              -> exists n, Gamma << n >> |-- t : T) /\
  (forall Gamma s Delta, Gamma |-- [s] : Delta           -> exists n, Gamma << n >> |-- [s] : Delta) /\
  (forall Gamma Delta,  |-- {Gamma} === {Delta}             -> exists n, << n >>  |-- {Gamma} === {Delta}) /\
  (forall Gamma T T', Gamma |-- T === T'            -> exists n, Gamma << n >> |-- T === T') /\
  (forall Gamma t t' T, Gamma |-- t === t' : T      -> exists n, Gamma << n >> |-- t === t' : T )  /\
  (forall Gamma s s' Delta, Gamma |-- [s] === [s'] : Delta -> exists n, Gamma << n >> |-- [s] === [s'] : Delta )
.
Proof.
apply System_ind; intros; program_simpl; eexists; eauto.
Qed.
