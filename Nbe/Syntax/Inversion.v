(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Inversion lemmas
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import List.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.System.
Require Import Nbe.Utils.

(******************************************************************************
 * Context Well-formedness
 *)

Section T1.

Lemma ContextWellFormedness:
  (forall Gamma, Gamma |--                        -> True ) /\
  (forall Gamma T, Gamma |-- T                    -> Gamma |-- ) /\
  (forall Gamma t T, Gamma |-- t : T              -> Gamma |-- ) /\
  (forall Gamma s Delta, Gamma |-- [s] : Delta           -> Gamma |-- ) /\
  (forall Gamma Delta, |-- {Gamma} === {Delta}             -> Gamma |-- ) /\
  (forall Gamma T T', Gamma |-- T === T'            -> Gamma |-- ) /\
  (forall Gamma t t' T, Gamma |-- t === t' : T      -> Gamma |-- )  /\
  (forall Gamma s s' Delta, Gamma |-- [s] === [s'] : Delta -> Gamma |-- )
.
Proof.
apply System_ind; intros; eauto.
(*clear_inversion H; eauto.*)
(* clear_inversion H; eauto. *)
Qed.

Definition ContextWellFormedness_WfTp
  := proj1 (proj2 ContextWellFormedness).
Hint Resolve ContextWellFormedness_WfTp.

Definition ContextWellFormedness_WtTm
  := proj1 (proj2 (proj2 ContextWellFormedness)).
Hint Resolve ContextWellFormedness_WtTm.

End T1.

(******************************************************************************
 *)

