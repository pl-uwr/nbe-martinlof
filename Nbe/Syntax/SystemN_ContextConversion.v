(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Context conversion for augmented system
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.SystemN.
Require Import Nbe.Syntax.SystemN_wfind.
Require Import Nbe.Syntax.SystemN_PreliminaryContextConversion.
Require Import Nbe.Syntax.SystemN_SimplePreliminaryContextConversion.
Require Import Nbe.Syntax.SystemN_Validity.
Require Import Nbe.Utils.TreeShape.
Require Import Utils.Tactics.
Require Import Nbe.Utils.ArithFuncs.
Require Import Arith.
Require Import Arith.Le.
Require Import Arith.Lt.
Require Import Arith.Max.

(******************************************************************************
 *)

Fact WfCxtEq_PreCxtConv: forall sh Gamma Delta,
 << sh >> |-- {Gamma} === {Delta} -> 
  exists ch,    PreCxtConv ch Gamma Delta.
Proof.
intros.
induction H.
eapply PreCxtConv_refl.
eauto.
(**)
program_simpl.


assert (exists sh, Delta << sh >> |--) as HX1.
eapply WfCxt_from_WfTp.
eauto.

assert (exists sh, Delta << sh >> |-- A === B) as HX2.
program_simpl.
edestruct PreliminaryContextConversion with (n := tree_height sh4).
eauto.
eauto.
program_simpl.
eauto.

assert (exists sh, Delta << sh >> |-- A) as HX3.
program_simpl.
eapply WfTp_from_WfTpEq1.
eauto.



program_simpl.
eexists.
econstructor 2; eauto.
Qed.


(******************************************************************************
 *)
Lemma ContextConversion_tp: forall sh1 sh2 Gamma Delta T,
  Gamma << sh1 >> |-- T ->
  << sh2 >> |-- {Gamma} === {Delta} ->
  exists ssh, Delta << ssh >> |-- T.
Proof.
intros.

assert (exists ssh, Delta << ssh >> |-- ) as HX1.
eapply WfCxt_from_WfCxtEq2.
eauto.


program_simpl.

edestruct WfCxtEq_PreCxtConv; eauto.
edestruct PreliminaryContextConversion with (n := tree_height sh1).
eauto.
eauto.

program_simpl; eauto.
Qed.

(******************************************************************************
 *)
Lemma ContextConversion_tpeq: forall sh1 sh2 Gamma Delta T1 T2,
  Gamma << sh1 >> |-- T1 === T2 ->
  << sh2 >> |-- {Gamma} === {Delta} ->
  exists ssh, Delta << ssh >> |-- T1 === T2.
Proof.
intros.

assert (exists ssh, Delta << ssh >> |-- ) as HX1.
eapply WfCxt_from_WfCxtEq2.
eauto.


program_simpl.

edestruct WfCxtEq_PreCxtConv; eauto.
edestruct PreliminaryContextConversion with (n := tree_height sh1).
eauto.
eauto.

program_simpl; eauto.
Qed.


(******************************************************************************
 *)
Lemma ContextConversion_tm: forall sh1 sh2 Gamma Delta t T,
  Gamma << sh1 >> |-- t : T ->
  << sh2 >> |-- {Gamma} === {Delta} ->
  exists ssh, Delta << ssh >> |-- t : T.
Proof.
intros.

assert (exists ssh, Delta << ssh >> |-- ) as HX1.
eapply WfCxt_from_WfCxtEq2.
eauto.


program_simpl.

edestruct WfCxtEq_PreCxtConv; eauto.
edestruct PreliminaryContextConversion with (n := tree_height sh1).
eauto.
eauto.
program_simpl; eauto.
Qed.

(******************************************************************************
 *)
Lemma ContextConversion_tmeq: forall sh1 sh2 Gamma Delta t1 t2 T,
  Gamma << sh1 >> |-- t1 === t2 : T ->
  << sh2 >> |-- {Gamma} === {Delta} ->
  exists ssh, Delta << ssh >> |-- t1 === t2 : T.
Proof.
intros.

assert (exists ssh, Delta << ssh >> |-- ) as HX1.
eapply WfCxt_from_WfCxtEq2.
eauto.


program_simpl.

edestruct WfCxtEq_PreCxtConv; eauto.
edestruct PreliminaryContextConversion with (n := tree_height sh1).
eauto.
eauto.
program_simpl; eauto.
Qed.


(******************************************************************************
 *)
Lemma ContextConversion_sb: forall sh1 sh2 Gamma Delta sb Psi,
  Gamma << sh1 >> |-- [sb] : Psi ->
  << sh2 >> |-- {Gamma} === {Delta} ->
  exists ssh, Delta << ssh >> |-- [sb] : Psi.
Proof.
intros.

assert (exists ssh, Delta << ssh >> |-- ) as HX1.
eapply WfCxt_from_WfCxtEq2.
eauto.


program_simpl.

edestruct WfCxtEq_PreCxtConv; eauto.
edestruct PreliminaryContextConversion with (n := tree_height sh1).
eauto.
eauto.
program_simpl; eauto.
Qed.

(******************************************************************************
 *)
Lemma ContextConversion_sbeq: forall sh1 sh2 Gamma Delta sb1 sb2 Psi,
  Gamma << sh1 >> |-- [sb1] === [sb2] : Psi ->
  << sh2 >> |-- {Gamma} === {Delta} ->
  exists ssh, Delta << ssh >> |-- [sb1] === [sb2] : Psi.
Proof.
intros.

assert (exists ssh, Delta << ssh >> |-- ) as HX1.
eapply WfCxt_from_WfCxtEq2.
eauto.


program_simpl.

edestruct WfCxtEq_PreCxtConv; eauto.
edestruct PreliminaryContextConversion with (n := tree_height sh1).
eauto.
eauto.
program_simpl; eauto.
Qed.
