(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Syntactical validity theorem
 *)

Set Implicit Arguments.


Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import List.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Syntax.System.
Require Import Nbe.Syntax.Inversion.
Require Import Nbe.Syntax.SimplePreliminaryContextConversion.
Require Import Nbe.Utils.

(******************************************************************************
 * Theorem
 *)


Lemma Inversion_SEXT: forall Gamma Delta A M SB,
  Gamma |-- [Sext SB M] : Delta,, A ->
  Gamma |-- [SB] : Delta /\ Gamma |-- M : TmSb A SB
.
Proof.
intros.
remember (Sext SB M).
remember (Delta,,A).
generalize dependent A.
generalize dependent Delta.
induction H; intros; try solve [clear_inversion Heqs].
clear_inversion Heqs.
clear_inversion Heql.
eauto.
(**)
subst.
clear_inversion H0.
eauto.

edestruct IHWtSb.
eauto.
eauto.
split.
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Fact Inversion_UNIV_FUN_F: forall Gamma A F,
  Gamma |-- ->
  Gamma |-- TmFun A F : TmUniv -> Gamma |-- A : TmUniv /\ Gamma,,A |-- F : TmUniv
.
Proof.
cut (forall Gamma A F T, Gamma |-- ->  Gamma |-- TmFun A F : T -> Gamma |-- T === TmUniv ->  Gamma |-- A : TmUniv /\ Gamma,,A |-- F : TmUniv); intros; eauto.

remember (TmFun A F) as C.
generalize dependent A.
generalize dependent F.
induction H0; intros; try solve [clear_inversion HeqC].
clear_inversion HeqC.
eauto.
subst.
eauto.
Qed.

(******************************************************************************
 *)
Fact PRE_Inversion_FUN_F Gamma A F:
  Gamma |-- ->
  Gamma |-- TmFun A F -> Gamma |-- A /\ Gamma,,A |-- F
.
Proof.
intros.
clear_inversion H0; auto.
edestruct Inversion_UNIV_FUN_F; eauto.
Qed.


(******************************************************************************
 *)
Lemma SyntacticValidity:
  (forall Gamma, Gamma |--                        -> True) /\
  (forall Gamma T, Gamma |-- T                    -> Gamma |--) /\
  (forall Gamma t T, Gamma |-- t : T              -> Gamma |-- /\ Gamma |-- T) /\
  (forall Gamma s Delta, Gamma |-- [s] : Delta           -> Gamma |-- /\ Delta |-- ) /\
  (forall Gamma Delta,  |-- {Gamma} === {Delta}           -> Gamma |-- /\ Delta |-- ) /\
  (forall Gamma T T', Gamma |-- T === T'            -> Gamma |-- /\ Gamma |-- T /\ Gamma |-- T'  ) /\
  (forall Gamma t t' T, Gamma |-- t === t' : T      -> Gamma |-- /\ Gamma |-- T /\ Gamma |-- t : T /\ Gamma |-- t' : T)  /\
  (forall Gamma s s' Delta, Gamma |-- [s] === [s'] : Delta -> Gamma |-- /\ Delta |-- /\  Gamma |-- [s] : Delta /\ Gamma |-- [s'] : Delta)
.
Proof.

apply System_ind; unfold TmArr; intros; program_simpl; repeat split; 
eauto; try solve [ eapply FUN_F; eauto ]; try solve [ eapply CONV; eauto ].
(**)
+{
clear_inversion w; eauto.
}
(**)
+{
apply SB_F with (Gamma,, A).
apply SEXT; eauto. 
auto.
}
(**)
+{
apply FUN_F.
auto.

eapply SimplePreliminaryContextConversion_tp with (A := A); eauto.
}
(**)
+{
apply FUN_F; eauto.
eapply SB_F; eauto.
apply SEXT; eauto.
eapply CONV; eauto.
}
(**)
+{
eapply UNIV_FUN_F.
eauto.
eapply CONV.
eapply SB.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply UNIV_E.
eauto.
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.
eapply EQ_TP_SB_UNIV.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply UNIV_E.
eauto.
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
}
(**)
+{
eapply UNIV_FUN_F.
eauto.
eapply SimplePreliminaryContextConversion_tm with (A := A); eauto.
}
(**)
+{
eapply CONV.
eapply FUN_I.
eauto.

eapply SimplePreliminaryContextConversion_tm with (A := A1); eauto.
eauto.
}
(**)
+{
edestruct PRE_Inversion_FUN_F; eauto.
unfold Ssing.
eapply SB_F; eauto.
eapply SEXT; eauto.
}
(**)
+{
unfold Ssing.
eapply FUN_E.
eauto.
eauto.

edestruct PRE_Inversion_FUN_F; eauto.
}
(**)
+{
eapply CONV; eauto.
eapply FUN_E.
eauto.
auto.
edestruct PRE_Inversion_FUN_F; eauto.
unfold Ssing.
apply EQ_TP_CONG_SB with (Gamma,,A); eauto.
eapply EQ_SB_CONG_SEXT; eauto.
apply EQ_TP_REFL.
edestruct PRE_Inversion_FUN_F; eauto.
}
(**)
+{
eapply EMPTY_E.
eauto.
}
(**)
+{
eapply CONV.
eapply EMPTY_E.
eauto.
eapply EQ_TP_CONG_FUN.
eauto.
eapply EQ_TP_CONG_SB.
eauto.
eauto.
}
(**)
+{
unfold Ssing.
eapply SB_F; eauto.
eapply SEXT; eauto.
}
(**)
+{
unfold Ssing.
eapply SB; eauto.
eapply SEXT; eauto.
}
(**)
+{
eapply CONV with (TmSb (TmSb A Sup) (Sext S M)).
eapply SB; eauto.
eapply SEXT; eauto.
apply HYP; eauto.
eapply EQ_TP_TRANS with (TmSb A (Sseq Sup (Sext S M))); eauto.
eapply EQ_TP_SBSEQ; eauto.
eapply SEXT; eauto.
eauto.
}
(**)
+{
edestruct PRE_Inversion_FUN_F; eauto.
eapply SB_F; eauto.
eapply SEXT; eauto.
}
(**) 
+{
edestruct PRE_Inversion_FUN_F; eauto.
eapply CONV; eauto.
unfold Ssing.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eapply SEXT; eauto.
eapply EQ_TP_CONG_SB; eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT. eauto.
eapply SEXT; eauto.
eapply EQ_SB_CONG_SEXT.
eauto.
eauto.
eauto.
eapply EQ_CONV; eauto.
}
(**)
+{
assert (Gamma |-- TmSb A S) by eauto.

edestruct PRE_Inversion_FUN_F; eauto.
eapply CONV; eauto.
eapply FUN_E; eauto.
(*new*)
eapply SB_F.
eapply SEXT.
eauto.
eauto.

eapply CONV.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.
(*end new*)

unfold Ssing.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SEXT; eauto. 
eapply SEXT; eauto. 
eapply CONV; eauto. eauto.
eapply EQ_TP_CONG_SB; eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT; eauto.
eapply SEXT; eauto. 
eapply SEXT; eauto.
eapply CONV; eauto.
eapply EQ_SB_CONG_SEXT; eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM; eauto.
eapply SEXT; eauto. eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ. eauto.
eapply EQ_SB_SUP; eauto.
eapply EQ_SB_REFL.
eauto.
eapply EQ_SB_SIDR; eauto.

eapply EQ_CONV.
eapply EQ_SBVAR; eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eapply EQ_TP_CONG_SB; eauto.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM; eauto.
eapply SEXT; eauto. eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ; eauto.
eapply EQ_SB_SUP; eauto.
eauto.
}
(**)
+{
clear_inversion H0.
apply FUN_F; eauto.
eapply SB_F; eauto.
eapply SEXT; eauto.
eapply CONV; eauto.
}
(**)
+{
clear_inversion H0.
eapply CONV; eauto.
}
(**)
+{
clear_inversion H0.
eapply FUN_I.
eauto.
eapply SB; eauto.
eapply SEXT; eauto.
eapply CONV; eauto.
}
(**)

(*
+{
eapply CONV.
eapply SB.
eauto.
eauto.

unfold TmArr.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.

assert (
  forall Gamma S1 S2,
    forall Delta__0, Gamma |-- [S2] : Delta__0 ->
    forall Delta__1, Delta__0 |-- [S1] : Delta__1 ->
    Gamma |-- TmSb (TmSb TmNat S1) S2 === TmNat 
  ) as HELPER.

intros.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL; eauto.
eapply EQ_TP_SB_NAT.
eauto.
eapply EQ_TP_SB_NAT.
eauto.


assert (
  Gamma,, TmSb TmNat S |--  [Sext (Sseq S Sup) TmVar] : Delta,, TmNat 
) as HELPER2.
intros.
eapply SEXT.
eauto.
eauto.
eapply CONV; eauto.
eapply EQ_TP_TRANS. 
eapply HELPER; eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_NAT; eauto.

assert (
  Gamma,, TmNat |--  [Sext (Sseq S Sup) TmVar] : Delta,, TmNat 
) as HELPER3.
intros.
eapply SEXT.
eauto.
eauto.
eapply CONV; eauto.
eapply EQ_TP_TRANS. 
eapply EQ_TP_SB_NAT; eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_NAT; eauto.


assert ( forall A,
  forall Delta__0 S0 , Gamma,,A |-- [S0] : Delta__0 ->
 Gamma,, A |-- TmNat === TmSb TmNat S0
) as HELPER4.
intros.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_NAT.
eauto.


eapply EQ_TP_CONG_FUN.
eauto.

eapply EQ_TP_TRANS. 
eapply HELPER; eauto.

eapply EQ_TP_SYM.
eauto.
}
*)

(* EQ_SBABSURD *)
+{
eapply CONV.
eauto.

(**)


assert (
  forall Gamma S1 S2,
    forall Delta__0, Gamma |-- [S2] : Delta__0 ->
    forall Delta__1, Delta__0 |-- [S1] : Delta__1 ->
    Gamma |-- TmSb (TmSb TmEmpty S1) S2 === TmEmpty
  ) as HELPER.
{
intros.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
}

assert (
  Gamma,, TmEmpty  |--  [Sext (Sseq S Sup) TmVar] : Delta,, TmEmpty
) as HELPER3.
{
intros.
eapply SEXT.
eauto.
eauto.
eapply CONV; eauto.
eapply EQ_TP_TRANS. 
eapply EQ_TP_SB_EMPTY; eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY; eauto.
}

assert (
  Gamma,, TmSb TmEmpty S |--  [Sext (Sseq S Sup) TmVar] : Delta,, TmEmpty
) as HELPER2.
{
intros.
eapply SEXT.
eauto.
eauto.
eapply CONV; eauto.
eapply EQ_TP_TRANS. 
eapply HELPER; eauto. 
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY; eauto.
}

assert (
 Gamma,, TmEmpty |-- [Sseq Sup (Sext (Sseq S Sup) TmVar)] === [Sseq S Sup] : Delta
) as HELPER4.
{
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F; eauto.
eapply CONV.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eauto.
}

assert (
 Gamma,, TmSb TmEmpty S |-- [Sseq Sup (Sext (Sseq S Sup) TmVar)] === [Sseq S Sup] : Delta
) as HELPER5.
{
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F; eauto.
eapply CONV.
eauto.
eapply EQ_TP_SBSEQ; eauto.
}

unfold TmArr.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_FUN; eauto.

eapply EQ_TP_CONG_FUN.
eauto.

eapply EQ_TP_TRANS. 
eapply EQ_TP_SBSEQ; eauto.
eapply EQ_TP_TRANS.

eapply EQ_TP_CONG_SB. eauto. eapply EQ_TP_REFL; eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBSEQ; eauto.
}
(**)
+{
apply EMPTY_E.
eauto.
 }
(* ETA? *)
+{
edestruct PRE_Inversion_FUN_F; eauto.
assert (Gamma,,  A |-- TmVar : TmSb (TmSb A Sup) Sid) by (eapply CONV; eauto).
assert (Gamma ,, A |-- TmSb A Sup) by eauto.

eapply FUN_I; eauto.
eapply CONV. 
eapply FUN_E.
eapply CONV; eauto.
eauto.

(*NEW*)
eapply SB_F.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply H1.
eapply CONV.
eapply HYP.
eauto.
eauto.
eauto.
(**)
unfold Ssing.
eapply EQ_TP_TRANS with (TmSb B Sid); eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SEXT; eauto. 
eapply SEXT; eauto. 
eapply CONV; eauto.
eauto.
eapply EQ_TP_CONG_SB; eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT; eauto.
eapply SEXT; eauto. 
eapply SEXT; eauto. 
eapply CONV; eauto.
eapply EQ_SB_TRANS with (Sext Sup TmVar); eauto.
eapply EQ_SB_CONG_SEXT; eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM; eauto.
eapply SEXT; eauto. 
eauto. eauto.
eapply EQ_SB_TRANS with (Sseq Sup Sid); eauto.
eapply EQ_SB_CONG_SSEQ; eauto.
eapply EQ_CONV; eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eapply EQ_TP_CONG_SB; eauto.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM; eauto.
eapply SEXT; eauto. eauto. eauto.
eapply EQ_SB_CONG_SSEQ; eauto.
}
(**)
+{
clear_inversion H1.

edestruct Inversion_SEXT.
eauto.
(*clear_inversion w0.*)
eapply SEXT; eauto.
}
(**)
+{
eapply SSEQ; eauto.
eapply SEXT; eauto.
eauto.
}
Qed.


(******************************************************************************
 *)

Lemma WfCxt_from_WfTp Gamma T (H : Gamma |-- T) : Gamma |--.
Proof.
destruct SyntacticValidity; intuition.
eauto.
Qed.
Hint Resolve WfCxt_from_WfTp.

(******************************************************************************
 *)
Lemma WfTp_from_WtTm Gamma t T (H : Gamma |-- t : T) : Gamma |-- T.
Proof.
destruct SyntacticValidity; intuition.
edestruct H1; eauto; intuition.
Qed.
Hint Resolve WfTp_from_WtTm.

(******************************************************************************
 *)
Lemma WfTp_from_WfTpEq1 Gamma T T' (H : Gamma |-- T === T') : Gamma |-- T.
Proof.
destruct SyntacticValidity; intuition.
edestruct H5; eauto; intuition.
Qed.
Hint Resolve WfTp_from_WfTpEq1.

(******************************************************************************
 *)
Lemma WfTp_from_WfTpEq2 Gamma T T' (H : Gamma |-- T === T') : Gamma |-- T'.
Proof.
destruct SyntacticValidity; intuition.
edestruct H5; eauto; intuition.
Qed.
Hint Resolve WfTp_from_WfTpEq2.

(******************************************************************************
 *)
Lemma WtTm_from_WtTmEq1 Gamma t t' T (H : Gamma |-- t === t' : T) : Gamma |-- t : T.
Proof.
destruct SyntacticValidity; intuition.
edestruct H6; eauto; intuition.
Qed.
Hint Resolve WtTm_from_WtTmEq1.

(******************************************************************************
 *)
Lemma WtTm_from_WtTmEq2 Gamma t t' T (H : Gamma |-- t === t' : T) : Gamma |-- t' : T.
Proof.
destruct SyntacticValidity; intuition.
edestruct H6; eauto; intuition.
Qed.
Hint Resolve WtTm_from_WtTmEq2.

(******************************************************************************
 *)

Lemma WtSb_from_WtSbEq1 Gamma s s' Delta (H : Gamma |-- [s] === [s'] : Delta) : Gamma |-- [s] : Delta.
Proof.
destruct SyntacticValidity; intuition.
edestruct H8; eauto; intuition.
Qed.
Hint Resolve WtSb_from_WtSbEq1.

(******************************************************************************
 *)
Lemma WtSb_from_WtSbEq2 Gamma s s' Delta (H : Gamma |-- [s] === [s'] : Delta) : Gamma |-- [s'] : Delta.
Proof.
destruct SyntacticValidity; intuition.
edestruct H8; eauto; intuition.
Qed.
Hint Resolve WtSb_from_WtSbEq2.

(******************************************************************************
 *)
Lemma WfSbCxt_from_WtSb Gamma s Delta (H : Gamma |-- [s] : Delta) : Delta |--.
Proof.
destruct SyntacticValidity; intuition.
edestruct H8; eauto; intuition.
Qed.
Hint Resolve WfSbCxt_from_WtSb.

(******************************************************************************
 *)
Lemma WfCxt_from_WtSb Gamma s Delta (H : Gamma |-- [s] : Delta) : Gamma |--.
Proof.
destruct SyntacticValidity; intuition.
edestruct H8; eauto; intuition.
Qed.
Hint Resolve WfSbCxt_from_WtSb.


(******************************************************************************
 *)
Lemma WfCxt_from_WfCxtEq1  Gamma Delta (H : |-- {Gamma} === {Delta}) : Gamma |--.
Proof.
destruct SyntacticValidity; intuition.
eapply (H4 Gamma Delta).
eauto.
Qed.
Hint Resolve WfCxt_from_WfCxtEq1.

(******************************************************************************
 *)
Lemma WfCxt_from_WfCxtEq2  Gamma Delta (H : |-- {Gamma} === {Delta}) : Delta |--.
Proof.
destruct SyntacticValidity; intuition.
eapply (H4 Gamma Delta).
eauto.
Qed.
Hint Resolve WfCxt_from_WfCxtEq2.

(******************************************************************************
 *)

Fact Inversion_FUN_F Gamma A F:
  Gamma |-- TmFun A F -> Gamma |-- A /\ Gamma,,A |-- F
.
Proof.
intros.
assert (Gamma |--). eauto.
eapply PRE_Inversion_FUN_F; eauto.
Qed.

(******************************************************************************
 * Each substitution formed in non-empty context is equal to sobe substition
 * of the form (sb, t)
 *)
 
Lemma Sb_sext: forall Gamma Delta A sb,
  Gamma |-- [sb] : Delta,,A ->
  exists sb' t, Gamma |-- [sb] === [Sext sb' t] : Delta,,A
.
Proof.
intros.
remember (Delta,,A).
generalize dependent Delta.
generalize dependent A.
induction H; intros; subst.
(**)
exists Sup. exists TmVar.
apply EQ_SB_SIDSUP.
clear_inversion H.
eauto.
(**)
assert (Delta,, A0 |--).
eauto.
clear_inversion H0.

exists (Sseq Sup Sup); exists (TmSb TmVar Sup).

eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SYM.
eapply EQ_SB_SEXT.
eauto.
eapply SEXT.
eapply SUP.
eauto.
eauto.
eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL; eauto.
apply EQ_SB_SYM.
apply EQ_SB_SIDSUP. auto.
eauto.
(**)
edestruct IHWtSb2 as [sb1].
eauto.
destruct H1 as [t1].
exists (Sseq sb1 S__2).
exists (TmSb t1 S__2).
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL.
eauto.
apply H1.
eapply EQ_SB_SEXT.
eauto.
eauto.
(**)
symmetry in Heql.
clear_inversion Heql.
exists S. exists M.
eapply EQ_SB_REFL.
eauto.

(***)
clear_inversion H0.
eauto.
(**)

specialize (IHWtSb A0 Gamma0 eq_refl).
program_simpl.
eauto.
Qed.

