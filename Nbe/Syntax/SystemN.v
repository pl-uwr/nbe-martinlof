(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Duplicate of the system augmented with type-shape annotation.
 *
 * It allows to formulate measure on derivations and derive well-founded
 * induction principle.
 *)

Set Implicit Arguments.

Require Import Nbe.Syntax.Def.
Require Import Nbe.Utils.
Require Import Nbe.Utils.TreeShape.

(******************************************************************************
 * Well-formed context
 * Gamma |--
 *)

Inductive WfCxtN : TreeShape -> Cxt -> Prop :=

| EMPTY: 
  nil << Leaf >> |--

| EXT : forall sh1 sh2 Gamma A,
  Gamma  << sh1 >>   |-- ->
  Gamma << sh2 >> |-- A -> 
  Gamma,,A << Node2 sh1 sh2 >>  |--

where "Gamma << n >>  |--" := (WfCxtN n Gamma)

(******************************************************************************
 *)
with WfCxtEqN : TreeShape -> Cxt -> Cxt -> Prop :=

| EQ_CXT_REFL: forall sh Gamma,
  Gamma << sh >> |-- ->
  << Node1 sh >> |-- {Gamma} === {Gamma}

| EQ_CXT_EXT: forall sh1 sh2 sh3 sh4 Gamma Delta A B,
  << sh1 >> |-- {Gamma} === {Delta} ->
  Gamma << sh2 >> |-- A ->
  Delta << sh3 >> |-- B -> 
  Gamma << sh4 >> |-- A === B ->
  << Node4 sh1 sh2 sh3 sh4 >>  |-- {Gamma,,A} === {Delta,,B}
where "<< n >> |-- { Gamma } === { Delta } " := (WfCxtEqN n Gamma Delta)


(******************************************************************************
 * Well-formed types
 * Gamma |-- A
 *)
with WfTpN : TreeShape -> Cxt -> Tm -> Prop :=

| UNIV_E : forall sh T Gamma,
  Gamma << sh >> |-- T : TmUniv ->
  Gamma << Node1 sh >> |-- T

| FUN_F: forall sh1 sh2 Gamma A B,
  Gamma  << sh1 >>  |-- A ->
  Gamma,,A << sh2 >> |-- B ->
  Gamma << Node2 sh1 sh2 >>   |-- TmFun A B

| SB_F: forall sh1 sh2 Gamma Delta A SB,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- A ->
  Gamma << Node2 sh1 sh2 >> |-- TmSb A SB


| UNIV_F: forall sh Gamma,
  Gamma << sh >> |-- ->
  Gamma << Node1 sh >> |-- TmUniv

where "Gamma << n >> |-- A" := (WfTpN n Gamma A)

(******************************************************************************
 * Well-typed term
 * Gamma |-- t : A
 *)
with WtTmN : TreeShape -> Cxt -> Tm -> Tm -> Prop :=

| UNIV_NAT_F: forall sh Gamma,
  Gamma << sh >> |-- ->
  Gamma << Node1 sh >> |-- TmNat : TmUniv

| UNIV_EMPTY_F: forall sh  Gamma,
  Gamma << sh >> |-- ->
  Gamma << Node1 sh >> |-- TmEmpty : TmUniv

| UNIV_UNIT_F: forall sh Gamma,
  Gamma << sh >>  |-- ->
  Gamma << Node1 sh >> |-- TmUnit : TmUniv

| UNIV_FUN_F: forall sh1 sh2 Gamma A B,
  Gamma  << sh1 >>  |-- A : TmUniv ->
  Gamma,,A << sh2 >> |-- B : TmUniv ->
  Gamma << Node2 sh1 sh2 >>   |-- TmFun A B : TmUniv

| HYP: forall sh Gamma T,
  Gamma ,, T << sh >> |-- ->
  Gamma ,, T << Node1 sh >> |-- TmVar : TmSb T Sup

| FUN_E: forall sh1 sh2 sh3 Gamma M N A B,
  Gamma << sh1 >> |-- M : TmFun A B ->
  Gamma  << sh2 >> |-- N : A ->
  Gamma,, A << sh3 >>  |-- B ->
  Gamma << Node3 sh1 sh2 sh3 >>  |-- TmApp M N : TmSb B (Ssing N)

| FUN_I: forall sh1 sh2 Gamma M A B,
  Gamma    << sh1 >> |-- A ->
  Gamma,,A << sh2 >> |-- M : B ->
  Gamma << Node2 sh1 sh2 >>  |-- TmAbs A M : TmFun A B

| N0: forall sh Gamma,
  Gamma << sh >> |-- ->
  Gamma << Node1 sh >> |-- Tm0 : TmNat

| NS: forall sh Gamma n,
  Gamma << sh >> |-- n : TmNat ->
  Gamma << Node1 sh >> |-- TmS n : TmNat

| SB: forall sh1 sh2 Gamma M A SB Delta,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- M : A ->
  Gamma << Node2 sh1 sh2 >> |-- (TmSb M SB) : (TmSb A SB)

| UNIT: forall sh Gamma,
  Gamma << sh >>  |-- ->
  Gamma << Node1 sh >> |-- Tm1 : TmUnit

| CONV: forall sh1 sh2 Gamma t A B,
  Gamma << sh1 >> |-- t : A ->
  Gamma << sh2 >> |-- A === B ->
  Gamma << Node2 sh1 sh2 >> |-- t : B

| EMPTY_E: forall sh Gamma A,
  Gamma << sh >> |-- A ->
  Gamma << Node1 sh >>  |-- TmAbsurd A : TmArr TmEmpty A

where "Gamma << n >> |-- e : T" := (WtTmN n Gamma e T) 

(******************************************************************************
 * Well-typed substitution
 * Gamma |-- [s] : Delta
 *)
with WtSbN : TreeShape -> Cxt -> Sb -> Cxt -> Prop :=

| SID: forall sh Gamma,
  Gamma << sh >> |-- ->
  Gamma << Node1 sh >> |-- [Sid] : Gamma

| SUP: forall sh Gamma A,
  Gamma << sh >> |-- A ->
  Gamma,,A << Node1 sh >> |-- [Sup] : Gamma

| SSEQ: forall sh1 sh2 Gamma__0 Gamma__1 Gamma__2 S__1 S__2,
  Gamma__0 << sh1 >>  |-- [S__2] : Gamma__1 ->
  Gamma__1 << sh2 >> |-- [S__1] : Gamma__2 ->
  Gamma__0 << Node2 sh1 sh2 >> |-- [Sseq S__1 S__2] : Gamma__2

| SEXT: forall sh1 sh2 sh3 Gamma SB Delta M A,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- A ->
  Gamma << sh3 >> |-- M : TmSb A SB ->
  Gamma << Node3 sh1 sh2 sh3 >> |-- [Sext SB M] : (Delta ,, A)

| SB_CONV: forall sh1 sh2 Gamma S Delta Psi,
  Gamma << sh1 >> |-- [S] : Psi ->
  << sh2 >> |-- {Psi} === {Delta} ->
  Gamma << Node2 sh1 sh2 >> |-- [S] : Delta

where "Gamma << n >> |-- [ e ] : T" := (WtSbN n  Gamma e T)
(******************************************************************************
 * Type equality (Equivalence)
 * Gamma |-- A === B
 *)
with WtTpEqN : TreeShape -> Cxt -> Tm -> Tm -> Prop :=

| EQ_TP_REFL: forall sh Gamma A,
  Gamma << sh >> |-- A ->
  Gamma << Node1 sh >> |-- A === A

| EQ_TP_SYM:  forall sh Gamma A B,
  Gamma << sh >> |-- A === B ->
  Gamma << Node1 sh >> |-- B === A

| EQ_TP_TRANS: forall sh1 sh2 Gamma A B C,
  Gamma << sh1 >> |-- A === B ->
  Gamma << sh2 >> |-- B === C ->
  Gamma << Node2 sh1 sh2 >> |-- A === C

 (******************************************************************************
 * Type equality (universe)
 * Gamma |-- A === B
 *)

| EQ_TP_UNIV_ELEM: forall sh Gamma A B,
  Gamma << sh >> |-- A === B : TmUniv ->
  Gamma << Node1 sh >> |-- A === B


(******************************************************************************
 * Type equality (Congruence)
 * Gamma |-- A === B
 *)

| EQ_TP_CONG_FUN: forall sh1 sh2 Gamma A A' B B',
  Gamma  << sh1 >>   |-- A === A' ->
  Gamma,,A << sh2 >>  |-- B === B' ->
  Gamma << Node2 sh1 sh2 >> |-- TmFun A B === TmFun A' B'

| EQ_TP_CONG_SB: forall sh1 sh2 Gamma Delta A1 A2 S1 S2,
  Gamma << sh1 >> |-- [S1] === [S2] : Delta ->
  Delta << sh2 >> |-- A1 === A2 ->
  Gamma << Node2 sh1 sh2 >> |-- TmSb A1 S1 === TmSb A2 S2

(******************************************************************************
 * Type equality (Computation)
 * Gamma |-- A === B
 *)

| EQ_TP_SBSEQ: forall sh1 sh2 sh3 Gamma S1 S2 GammaS1 GammaS2 A,
  Gamma << sh1 >> |-- [S1] : GammaS1 ->
  GammaS1 << sh2 >> |-- [S2] : GammaS2 ->
  GammaS2 << sh3 >> |-- A ->
  Gamma << Node3 sh1 sh2 sh3 >> |-- TmSb (TmSb A S2) S1 === TmSb A (Sseq S2 S1) 

| EQ_TP_SB_UNIV: forall sh Gamma Delta S,
  Gamma << sh >> |-- [S] : Delta ->
  Gamma << Node1 sh >> |-- TmSb TmUniv S === TmUniv

| EQ_TP_SB_FUN: forall sh1 sh2 sh3 Gamma Delta S A B,
  Gamma << sh1 >> |-- [S] : Delta ->
  Delta << sh2 >> |-- A ->
  Delta,,A << sh3 >> |-- B ->
  Gamma << Node3 sh1 sh2 sh3 >>  |-- TmSb (TmFun A B) S === TmFun (TmSb A S) (TmSb B (Sext (Sseq S Sup) TmVar))

| EQ_TP_SBID: forall sh Gamma A,
  Gamma << sh >> |-- A ->
  Gamma << Node1 sh >>  |-- (TmSb A Sid) === A

where "Gamma << n >> |-- A === B" := (WtTpEqN n Gamma A B)

(******************************************************************************
 * Term equalities (Equivalence)
 * Gamma |-- t == t' : A
 *)
with WtTmEqN : TreeShape -> Cxt -> Tm -> Tm -> Tm -> Prop :=

| EQ_REFL: forall sh Gamma M A,
  Gamma << sh >> |-- M : A ->
  Gamma << Node1 sh >> |-- M === M : A

| EQ_SYM:  forall sh Gamma M1 M2 A,
  Gamma << sh >> |-- M1 === M2 : A ->
  Gamma << Node1 sh >> |-- M2 === M1 : A

| EQ_TRANS: forall sh1 sh2 Gamma M1 M2 M3 A,
  Gamma << sh1 >> |-- M1 === M2 : A ->
  Gamma << sh2 >> |-- M2 === M3 : A ->
  Gamma << Node2 sh1 sh2 >> |-- M1 === M3 : A

 (******************************************************************************
 * Term equalities (Universe)
 * Gamma |-- t == t' : A
 *)

| EQ_UNIV_SB_NAT: forall sh Gamma Delta S,
  Gamma << sh >> |-- [S] : Delta ->
  Gamma << Node1 sh >> |-- TmSb TmNat S === TmNat : TmUniv

| EQ_UNIV_SB_EMPTY: forall sh Gamma Delta SB,
  Gamma << sh >> |-- [SB] : Delta ->
  Gamma << Node1 sh >> |-- TmSb TmEmpty SB === TmEmpty : TmUniv

| EQ_UNIV_SB_UNIT: forall sh Gamma Delta SB,
  Gamma << sh >> |-- [SB] : Delta ->
  Gamma << Node1 sh >> |-- TmSb TmUnit SB === TmUnit : TmUniv

| EQ_UNIV_SB_FUN: forall sh1 sh2 sh3 Gamma Delta S A B,
  Gamma << sh1 >> |-- [S] : Delta ->
  Delta << sh2 >> |-- A : TmUniv ->
  Delta,,A << sh3 >> |-- B : TmUniv ->
  Gamma << Node3 sh1 sh2 sh3 >>  |-- TmSb (TmFun A B) S === TmFun (TmSb A S) (TmSb B (Sext (Sseq S Sup) TmVar)) : TmUniv

| EQ_UNIV_CONG_FUN: forall sh1 sh2 Gamma A A' B B',
  Gamma  << sh1 >>   |-- A === A' : TmUniv ->
  Gamma,,A << sh2 >>  |-- B === B' : TmUniv  ->
  Gamma << Node2 sh1 sh2 >> |-- TmFun A B === TmFun A' B' : TmUniv


(******************************************************************************
 * Term equalities (Congruence)
 * Gamma |-- t == t' : A
 *)

| EQ_CONV: forall sh1 sh2 Gamma M1 M2 A B,
  Gamma << sh1 >> |-- M1 === M2 : A ->
  Gamma << sh2 >> |-- A === B ->
  Gamma << Node2 sh1 sh2 >> |-- M1 === M2 : B

| EQ_CONG_FUN_I: forall sh1 sh2  Gamma M1 M2 A1 A2 B,
  Gamma,,A1 << sh1 >> |-- M1 === M2 : B ->
  Gamma  << sh2 >>   |-- A1 === A2 ->
  Gamma  << Node2 sh1 sh2 >>  |-- TmAbs A1 M1 === TmAbs A2 M2 : TmFun A1 B

| EQ_CONG_FUN_E: forall sh1 sh2 Gamma M1 N1 M2 N2 A B,
  Gamma << sh1 >> |-- M1 === M2 : TmFun A B ->
  Gamma << sh2 >> |-- N1 === N2 : A ->
  Gamma << Node2 sh1 sh2 >> |-- TmApp M1 N1 === TmApp M2 N2 : TmSb B (Ssing N1)

| EQ_CONG_SB: forall sh1 sh2 Gamma M1 M2 A S1 S2 GammaS,
  Gamma << sh1 >> |-- [S1] === [S2] : GammaS ->
  GammaS << sh2 >> |-- M1 === M2 : A ->
  Gamma << Node2 sh1 sh2 >> |-- TmSb M1 S1 === TmSb M2 S2 : TmSb A S1

| EQ_CONG_ABSURD: forall sh1 Gamma A1 A2,
  Gamma << sh1 >> |-- A1 === A2 ->
  Gamma << Node1 sh1 >> |-- TmAbsurd A1 === TmAbsurd A2 : TmArr TmEmpty A1

| EQ_CONG_S: forall sh Gamma n1 n2,
  Gamma << sh >> |-- n1 === n2 : TmNat ->
  Gamma << Node1 sh >> |-- TmS n1 === TmS n2 : TmNat

(******************************************************************************
 * Term equalities (Computation)
 * Gamma |-- t == t' : A
 *)

| EQ_FUN_Beta: forall sh1 sh2 Gamma M N A B,
  Gamma,,A << sh1 >> |-- M : B  ->
  Gamma << sh2 >>  |-- N : A ->
  Gamma << Node2 sh1 sh2 >> |-- TmApp (TmAbs A M) N === TmSb M (Ssing N) : TmSb B (Ssing N)

| EQ_SBVAR: forall sh1 sh2 sh3 Gamma SB Delta M A,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- A ->
  Gamma << sh3 >> |-- M : TmSb A SB ->
  Gamma << Node3 sh1 sh2 sh3 >> |-- (TmSb TmVar (Sext SB M)) === M : TmSb A SB

| EQ_SBID: forall sh Gamma M A,
  Gamma << sh >> |-- M : A ->
  Gamma << Node1 sh >> |-- (TmSb M Sid) === M : A

(******************************************************************************
 * Term equalities (Pushing substitutions)
 * Gamma |-- t == t' : A
 *)

| EQ_SBAPP: forall sh1 sh2 sh3 Gamma SB Delta M N A B,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- M : TmFun A B ->
  Delta << sh3 >> |-- N : A ->
  Gamma << Node3 sh1 sh2 sh3 >> |-- TmSb (TmApp M N) SB === TmApp (TmSb M SB) (TmSb N SB) : TmSb B (Sext SB (TmSb N SB))

| EQ_SBABS: forall sh1 sh2 Gamma SB Delta M A B,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta,,A << sh2 >> |-- M : B ->
  Gamma << Node2 sh1 sh2 >> |-- TmSb (TmAbs A M) SB === TmAbs (TmSb A SB) (TmSb M (Sext (Sseq SB Sup) TmVar)) : TmFun (TmSb A SB) (TmSb B (Sext (Sseq SB Sup) TmVar))

| EQ_SBUNIT: forall sh Gamma SB Delta,
  Gamma << sh >> |-- [SB] : Delta ->
  Gamma << Node1 sh >> |-- TmSb Tm1 SB === Tm1 : TmUnit

| EQ_SBNAT0: forall sh Gamma SB Delta,
  Gamma << sh >> |-- [SB] : Delta ->
  Gamma << Node1 sh >> |-- TmSb Tm0 SB === Tm0 : TmNat

| EQ_SBNATS: forall sh1 sh2 Gamma SB n Delta,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- n : TmNat ->
  Gamma << Node2 sh1 sh2 >>  |-- TmSb (TmS n) SB === TmS (TmSb n SB) : TmNat

| EQ_SBABSURD: forall sh1 sh2 Gamma SB Delta A,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- A ->
  Gamma << Node2 sh1 sh2 >> |-- TmSb (TmAbsurd A) SB === TmAbsurd (TmSb A SB) : TmArr TmEmpty (TmSb A SB)

| EQ_SBSEQ: forall sh1 sh2 sh3 Gamma S1 S2 GammaS1 GammaS2 M A,
  Gamma << sh1 >> |-- [S1] : GammaS1 ->
  GammaS1 << sh2 >> |-- [S2] : GammaS2 ->
  GammaS2 << sh3 >> |-- M :  A ->
  Gamma << Node3 sh1 sh2 sh3 >> |-- TmSb (TmSb M S2) S1 === TmSb M (Sseq S2 S1) : TmSb (TmSb A S2) S1


(******************************************************************************
 * Term equalities (Extensionality)
 * Gamma |-- t == t' : A
 *)

| EQ_FUN_Eta: forall sh Gamma M A B,
  Gamma << sh >> |-- M : TmFun A B ->
  Gamma << Node1 sh >> |-- M === TmAbs A (TmApp (TmSb M Sup) TmVar) : TmFun A B

| EQ_Unit_Eta: forall sh Gamma M,
  Gamma << sh >> |-- M : TmUnit ->
  Gamma << Node1 sh >> |-- M === Tm1 : TmUnit

where "Gamma << n >> |-- e1 === e2 : T" := (WtTmEqN n Gamma e1 e2 T)

(******************************************************************************
 * Substitution equalities (Equivalence)
 * Gamma |-- [s] == [s'] : Delta
 *)

with WtSbEqN: TreeShape -> Cxt -> Sb -> Sb -> Cxt -> Prop :=

| EQ_SB_REFL: forall sh Gamma SB Delta,
  Gamma << sh >> |-- [SB] : Delta ->
  Gamma << Node1 sh >> |-- [SB] === [SB] : Delta

| EQ_SB_SYM:  forall sh Gamma S1 S2 Delta,
  Gamma << sh >> |-- [S1] === [S2] : Delta ->
  Gamma << Node1 sh >> |-- [S2] === [S1] : Delta

| EQ_SB_TRANS: forall sh1 sh2 Gamma S1 S2 S3 Delta,
  Gamma << sh1 >> |-- [S1] === [S2] : Delta ->
  Gamma << sh2 >> |-- [S2] === [S3] : Delta ->
  Gamma << Node2 sh1 sh2 >> |-- [S1] === [S3] : Delta

| EQ_SB_CONV: forall sh1 sh2 Gamma S1 S2 Delta Psi,
  Gamma << sh1 >> |-- [S1] === [S2] : Psi ->
  << sh2 >> |-- {Psi} === {Delta} ->
  Gamma << Node2 sh1 sh2 >> |-- [S1] === [S2] : Delta

(******************************************************************************
 * Substitution equalities (Congruence)
 * Gamma |-- [s] == [s'] : Delta
 *)

| EQ_SB_CONG_SSEQ: forall Gamma sh1 sh2 S1 S2 U1 U2 GammaS1 GammaS2,
  Gamma << sh1 >>  |-- [S2] === [U2] : GammaS1 ->
  GammaS1 << sh2 >> |-- [S1] === [U1] : GammaS2 ->
  Gamma << Node2 sh1 sh2 >>  |-- [Sseq S1 S2] === [Sseq U1 U2] : GammaS2

| EQ_SB_CONG_SEXT: forall sh1 sh2 sh3 Gamma S1 S2 Delta M1 M2 A,
  Gamma << sh1 >> |-- [S1] === [S2] : Delta ->
  Delta << sh2 >> |-- A ->
  Gamma << sh3 >> |-- M1 === M2 : TmSb A S1  ->
  Gamma << Node3 sh1 sh2 sh3 >> |-- [Sext S1 M1] === [Sext S2 M2] : Delta ,, A

(******************************************************************************
 * Substitution equalities (Computation)
 * Gamma |-- [s] == [s'] : Delta
 *)

| EQ_SB_SEXT: forall sh1 sh2 Gamma S1 S2 GammaS1 GammaS2 M A,
  Gamma << sh1 >> |-- [S1] : GammaS1 ->
  GammaS1 << sh2 >> |-- [Sext S2 M] : GammaS2,,A ->
  Gamma << Node2 sh1 sh2 >> |-- [Sseq (Sext S2 M) S1] === [Sext (Sseq S2 S1) (TmSb M S1)] : (GammaS2,,A)

| EQ_SB_SUP: forall sh1 sh2 sh3  Gamma SB M A Delta,
  Gamma << sh1 >> |-- [SB] : Delta ->
  Delta << sh2 >> |-- A ->
  Gamma << sh3 >> |-- M : TmSb A SB ->
  Gamma << Node3 sh1 sh2 sh3 >> |-- [Sseq Sup (Sext SB M)] === [SB] : Delta

(******************************************************************************
 * Substitution equalities (Rest)
 * Gamma |-- [s] == [s'] : Delta
 *)

| EQ_SB_SIDSUP: forall sh Gamma A,
  Gamma << sh >> |-- A ->
  Gamma,,A << Node1 sh >> |-- [Sid] === [Sext Sup TmVar] : Gamma,,A

| EQ_SB_SIDL: forall sh Gamma SB GammaS,
  Gamma << sh >> |-- [SB] : GammaS ->
  Gamma << Node1 sh >> |-- [Sseq Sid SB] === [SB] : GammaS

| EQ_SB_SIDR: forall sh Gamma SB GammaS,
  Gamma << sh >> |-- [SB] : GammaS ->
  Gamma << Node1 sh >> |-- [Sseq SB Sid] === [SB] : GammaS

| EQ_SB_COMM: forall sh1 sh2 sh3 Gamma S1 S2 S3 GammaS1 GammaS2 GammaS3,
  Gamma << sh1 >>  |-- [S3] : GammaS1 ->
  GammaS1 << sh2 >>  |-- [S2] : GammaS2 ->
  GammaS2 << sh3 >> |-- [S1] : GammaS3 ->
  Gamma << Node3 sh1 sh2 sh3 >>   |-- [Sseq (Sseq S1 S2) S3] === [Sseq S1 (Sseq S2 S3)] : GammaS3

where "Gamma << n >> |-- [ e1 ] === [ e2 ] : T" := (WtSbEqN n Gamma e1 e2 T)
.

Hint Constructors WfCxtN.
Hint Constructors WfTpN.
Hint Constructors WtTmN.
Hint Constructors WtSbN.
Hint Constructors WfCxtEqN.
Hint Constructors WtTpEqN.
Hint Constructors WtTmEqN.
Hint Constructors WtSbEqN.


(******************************************************************************
 * Induction
 *)

Scheme WfCxtN_dind   := Induction for WfCxtN Sort Prop
 with  WfTpN_dind    := Induction for WfTpN Sort Prop
 with  WtTmN_dind    := Induction for WtTmN Sort Prop
 with  WtSbN_dind    := Induction for WtSbN Sort Prop
 with  WfCxtEqN_dind := Induction for WfCxtEqN Sort Prop
 with  WtTpEqN_dind  := Induction for WtTpEqN Sort Prop
 with  WtTmEqN_dind  := Induction for WtTmEqN Sort Prop
 with  WtSbEqN_dind  := Induction for WtSbEqN Sort Prop
.

Combined Scheme SystemN_ind from WfCxtN_dind, WfTpN_dind, WtTmN_dind, WtSbN_dind, WfCxtEqN_dind, WtTpEqN_dind, WtTmEqN_dind, WtSbEqN_dind.

(******************************************************************************
 *)

Lemma EQ_TP_SBSEQ3: forall h1 h2 h3 h4 SB1 SB2 SB3 Delta1 Delta2 Delta3 Gamma A,
   
    Gamma << h1 >> |-- [SB1] : Delta1 ->
    Delta1 << h2 >> |-- [SB2] : Delta2 ->
    Delta2 << h3 >> |-- [SB3] : Delta3 ->   
    Delta3 << h4 >> |-- A ->
   exists h, 
    Gamma << h >> |-- TmSb (TmSb (TmSb A SB3) SB2) SB1 === TmSb A (Sseq SB3 (Sseq SB2 SB1))
.
Proof.
intros.
eexists.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.
Qed.

