(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments for natural numbers (terms)
 *)
Set Implicit Arguments.

Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

Require Import Nbe.SoundnessOfJudgements.WfTp_NatTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtTm_UnivTypeJudgements.

(******************************************************************************
 *)
Lemma Soundness_of_NAT_F_type: forall Gamma,
  Gamma |= ->
  Gamma |= TmNat 
.
Proof.
intros.
eapply Soundness_of_UNIV_ELEM; eauto.
Qed.
Hint Immediate Soundness_of_NAT_F_type.

(******************************************************************************
 *)
Lemma Soundness_of_N0: forall Gamma,
  Gamma |= ->
  Gamma |= Tm0 : TmNat
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
do 2 eexists; exists PerNat. repeat split; eauto. red; eexists; repeat split; eauto.
Qed.
Hint Resolve Soundness_of_N0.


(******************************************************************************
 *)
Lemma Soundness_of_EQ_CONG_NS: forall Gamma n1 n2,
  Gamma |= n1 === n2 : TmNat ->
  Gamma |= TmS n1 === TmS n2 : TmNat
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto.

specialize (H2 _ _ H0).
program_simpl.
clear_inversion H5.
clear_inversion H9.
clear_inversion H5.
clear_inversion H10.

renamer.
exists (DS Dn1_env0), (DS Dn2_env1), PerNat.
repeat split; eauto.
exists Dnat; split; eauto.
Qed.
Hint Resolve Soundness_of_EQ_CONG_NS.

(******************************************************************************
 *)
Lemma Soundness_of_NS: forall Gamma n,
  Gamma |= n : TmNat ->
  Gamma |= TmS n : TmNat
.
Proof.
intros.
apply Soundness_of_EQ_CONG_NS.
eauto.
Qed.
Hint Resolve Soundness_of_NS.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SBNAT0: forall Gamma S Delta,
  Gamma |=  [S]:Delta ->
  Gamma |=  TmSb Tm0 S === Tm0 : TmNat
.
Proof.

constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.

apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 2 eexists; exists PerNat; repeat split; eauto.
red; eexists ; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SBNAT0.


(******************************************************************************
 *)
Lemma Soundness_of_EQ_SBNAT: forall Gamma S Delta n,
  Gamma |=  [S]:Delta ->
  Delta |= n : TmNat ->
  Gamma |=  TmSb (TmS n) S === TmS (TmSb n S) : TmNat
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto.
specialize (H6 _  _ H1).
program_simpl.

renamer.
specialize (H3 _ _ H8).
program_simpl.
reds.
compute_sth.
specialize (H4 _ _ H8).
renamer.
exists (DS Dn_DS_env0), (DS Dn_DS_env1), PerNat.
program_simpl; repeat split; eauto.
exists Dnat; split; auto.
Qed.
Hint Immediate  Soundness_of_EQ_SBNAT.

