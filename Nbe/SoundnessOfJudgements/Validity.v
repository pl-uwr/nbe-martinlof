(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Definition of validity relation for PER model
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.

(******************************************************************************
 * Validity relation
 *)
Inductive ValCxt : Cxt -> Prop :=

| ValCxt_empty: 
  nil |=

| ValCxt_ext: forall Gamma A,
  Gamma |= ->
  Gamma |= A === A ->
  Gamma,,A |=

where "Gamma |=" := (ValCxt Gamma)


with ValCxtEq: Cxt -> Cxt -> Prop :=

| ValCxtEq_intro: forall Gamma Delta,
   Gamma |= ->
   Delta |= ->
  (forall env0 env1,
    [env0] === [env1] #in# Gamma <-> [env0] === [env1] #in# Delta) ->
    |= {Gamma} === {Delta}


where "|= { Gamma } === { Delta }" := (ValCxtEq Gamma Delta)
with ValTpEq: Cxt -> Tm -> Tm -> Prop :=

| ValTp_intro: forall Gamma A B,
  Gamma |= ->
  (forall env0 env1,
    [env0] === [env1] #in# Gamma ->
    exists DA, exists DB,
    EvalTm A env0 DA /\
    EvalTm B env1 DB /\
    DA === DB #in# PerType
  ) ->
  Gamma |= A === B

where "Gamma |= A === B" := (ValTpEq Gamma A B)
with ValTmEq : Cxt -> Tm -> Tm -> Tm -> Prop :=

| ValTm_intro: forall Gamma tm0 tm1 A,
  Gamma |= A === A ->
  (forall env0 env1,
    [env0] === [env1] #in# Gamma ->
    exists dtm0, exists dtm1, exists PA,
    InterpTypePer A   env0 PA /\
    EvalTm tm0 env0 dtm0 /\
    EvalTm tm1 env1 dtm1 /\
    dtm0 === dtm1 #in# PA) ->
  Gamma |= tm0 === tm1 : A
  
where "Gamma |= t === t' : A" := (ValTmEq Gamma t t' A)
with ValSbEq : Cxt -> Sb -> Sb -> Cxt -> Prop :=

| ValSb_intro: forall Gamma sb0 sb1 Delta,
  Gamma |= ->
  Delta |= ->
  (forall env0 env1,
    [env0] === [env1] #in# Gamma ->
    exists denv0, exists denv1, 
    EvalSb sb0 env0 denv0 /\
    EvalSb sb1 env1 denv1 /\
    [denv0] === [denv1] #in# Delta ) ->
  Gamma |= [sb0] === [sb1] : Delta

where "Gamma |= [ sb0 ] === [ sb1 ] : Delta" := (ValSbEq Gamma sb0 sb1 Delta)
.
Hint Constructors ValCxt.
Hint Constructors ValCxtEq.
Hint Constructors ValTpEq.
Hint Constructors ValTmEq.
Hint Constructors ValSbEq.

Notation "Gamma |= A" := (Gamma |= A === A).
Notation "Gamma |= t : A" := (Gamma |= t === t : A).
Notation "Gamma |= [ t ] : A" := (Gamma |= [t] === [t] : A).

(******************************************************************************
 * Induction scheme
 *)
Scheme ValCxt_mind   := Induction for ValCxt Sort Prop
  with ValCxtEq_mind := Induction for ValCxtEq Sort Prop
  with ValTpEq_mind  := Induction for ValTpEq Sort Prop
  with ValTmEq_mind  := Induction for ValTmEq Sort Prop
.

Combined Scheme Val_ind from ValCxt_mind, ValCxtEq_mind, ValTpEq_mind, ValTmEq_mind.
