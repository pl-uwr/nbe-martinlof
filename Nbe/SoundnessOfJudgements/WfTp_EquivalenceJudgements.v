(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of equivalence judgments (types)
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

(******************************************************************************
 *)
Lemma Helper_TP_SYM: forall Gamma A B,
  Gamma |= A === B ->
  Gamma |= B === A
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto.
assert (PER $ ValEnv Gamma) by eauto.
assert ([env1] === [env0] #in# Gamma) by (symmetry; auto).
apply H2 in H3.
program_simpl; renamer.
exists DB_env0.
exists DA_env1.
repeat split; eauto.
symmetry. auto.
Qed.
Hint Resolve Helper_TP_SYM.

(******************************************************************************
 *)
Lemma Helper_TP_TRANS: forall Gamma A B C,
  Gamma |= A === B ->
  Gamma |= B === C ->
  Gamma |= A === C
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto.
assert (PER $ ValEnv Gamma) by eauto.
assert ([env0] === [env0] #in# Gamma).
eapply Per_domL; eauto.
apply H4 in H0.
apply H3 in H1.
program_simpl; compute_sth; renamer.
exists DA_env0.
exists DC_env1.
repeat split; auto.
apply PER_Transitive with DB_env0; eauto.
Qed.
Hint Resolve Helper_TP_TRANS.

(******************************************************************************
 *)
Lemma Helper_EQ_TP_ValidL: forall Gamma A B,
  Gamma |= A === B ->
  Gamma |= A
.
Proof.
intros.
cut (Gamma |= B === A); intros; eauto.
Qed.
Hint Resolve Helper_EQ_TP_ValidL.

(******************************************************************************
 *)
Lemma Helper_EQ_TP_ValidR: forall Gamma A B,
  Gamma |= A === B ->
  Gamma |= B
.
Proof.
intros.
cut (Gamma |= A === B); intros; eauto.
Qed.
Hint Resolve Helper_EQ_TP_ValidR.
