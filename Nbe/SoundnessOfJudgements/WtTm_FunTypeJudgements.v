(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgments for Pi-type (terms)
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

Require Import Nbe.SoundnessOfJudgements.WfTp_FunTypeJudgements.
Require Import Nbe.SoundnessOfJudgements.WtSb_GeneralJudgements.
Require Import Nbe.SoundnessOfJudgements.WfTp_GeneralJudgements.

(******************************************************************************
 * Beta reduction
 *)


Lemma Valid_FUN_E_TP: forall Gamma A B N,
 Gamma |= TmFun A B ->
 Gamma |= N : A ->
 Gamma |= TmSb B (Ssing N)
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
clear_inversion_of_pertype H10.
program_simpl; compute_sth; reds; renamer.
find_extdeters. close_extdeters. close_doms. renamer. move_inpers.

destruct H9 with DN_env0; auto.
destruct H11 with DN_env1; auto.
clear_inversion H23; clear_inversion H24; renamer.

exists DB_env0_DN_env0; exists DB_env1_DN_env1.
unfold Ssing; repeat split; eauto.
Qed.
Hint Resolve Valid_FUN_E_TP.

Lemma Valid_FUN_E: forall Gamma A B M N,
 Gamma,,A |= M : B  ->
 Gamma    |= N : A ->
 Gamma    |= TmApp (TmAbs A M) N === TmSb M (Ssing N) : TmSb B (Ssing N)
.
Proof.
intros.

assert (Gamma |= TmFun A B) by (inversion H; inversion H0; eauto).

constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.

apply_valenvs; program_simpl; compute_sth; reds; renamer.
clear_inversion_of_pertype H12.
program_simpl; compute_sth; reds; renamer.
find_extdeters. close_extdeters. close_doms. renamer. move_inpers.

assert ([Dext env0 DN_env0] === [Dext env1 DN_env1] #in# Gamma,,A).
apply ValEnv_ext with PDA_env0; auto.
red; eexists; repeat split; eauto.

apply_valenvs; program_simpl; compute_sth; reds; renamer.

move_inpers.
exists DM_env0_DN_env0; exists DM_env1_DN_env1; exists PDB_env0_DN_env0.
unfold Ssing; repeat split; eauto.
red; eexists; repeat split; eauto.
Qed.
Hint Resolve Valid_FUN_E.

(******************************************************************************
 * Extensionality for abstraction
 *)

Lemma Valid_FUN_Eta: forall Gamma A B M,
  Gamma |= M : TmFun A B ->
  Gamma |=  M === TmAbs A (TmApp (TmSb M Sup) TmVar) : TmFun A B
.
Proof.
intros.

constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
clear_inversion H1.
renamer; move_inpers.

exists DM_env0.
exists (Dclo (TmApp (TmSb M Sup) TmVar) env1).
exists PH13.
repeat split; eauto.
red; eexists; repeat split; eauto.
(**)
invert_interp.
constructor 1; intros.
clear_inversion_of_pertype H14.
program_simpl; compute_sth; reds; renamer.
clear_inversion H11; clear_inversion po_ro.
clear_inversion H7; clear_inversion po_ro.
inversion_clear H18; inversion_clear H10.
find_extdeters. close_extdeters. close_doms. move_inpers.

destruct H5 with a0 a1; eauto.
destruct H7 with a0 a1; eauto.
program_simpl; compute_sth; reds; renamer.
rename H41 into DB0.
rename x into res0.
rename H34 into res1.
exists res0; exists res1; exists DB0.
repeat split; eauto.
Qed.
Hint Resolve Valid_FUN_Eta.

(******************************************************************************
 *)
Lemma Soundness_of_FUN_E: forall Gamma M N A B,
  Gamma |= M : TmFun A B ->
  Gamma |= N : A ->
  Gamma,, A |= B ->
  Gamma |= TmApp M N : TmSb B (Ssing N)
.
Proof.

constructor 1; auto; intros; extract_vals; reds; eauto.

compute_sth; apply_valenvs.
program_simpl.
reds.
compute_sth.
renamer. 
clear_inversion H25.
repeat progress (elim_extdeter; clear_dups).

assert (DN_env0 === DN_env1 #in# PDA_env0).
apply H0; eauto.

destruct H with DN_env0 DN_env1 as [y1 HH].
apply H0; eauto.
destruct HH as [y2 HH].
destruct HH as [Y HH].
program_simpl; renamer.
exists y1; exists y2; exists Y.
repeat split; eauto.
red.
intuition.

destruct H28 with DN_env0; eauto. 

exists x; repeat split.
apply evalSb with (Dext env0 DN_env0).
unfold Ssing; eauto.
clear_inversion H15; eauto.
eauto.
Qed.
Hint Resolve Soundness_of_FUN_E.

Require Import Nbe.SoundnessOfJudgements.WtTm_EquivalenceJudgements.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_CONV_ABS: forall Gamma M1 M2 A1 A2 B,  
 Gamma,, A1 |=  M1 === M2 : B ->
 Gamma |= A1 === A2 ->
 Gamma |=  TmAbs A1 M1 === TmAbs A2 M2 : TmFun A1 B
.
Proof.
intros.

assert (Gamma,,A1 |= M1 : B) by (eapply Helper_EQ_ValidL; eauto).
assert (Gamma    |= TmAbs A1 M1 : TmFun A1 B) by eauto.
assert (Gamma    |= TmAbs A1 M2 : TmFun A1 B) by eauto.

constructor 1; auto; intros; extract_vals; reds; auto.
apply_valenvs; program_simpl; compute_sth; renamer.

move H30 at bottom.
move H20 at bottom.
move H11 at bottom.
move H28 at bottom.
move H13 at bottom.
move H28 at bottom.
move H18 at bottom.

rename H29 into PF_A1.
rename H19 into PF_A2.

rename H11 into clo1.
rename H28 into clo1'.
rename H13 into clo2.
rename H18 into clo2'.

exists clo1.
exists clo2'.
exists PF_A1.
repeat (split; auto).

(**)
clear_inversion H22.
eauto.
(**) 

destruct H30 as [DX [HDX1 HDX2]].
clear_inversion HDX1.
clear_inversion_of_interptype HDX2.
compute_sth; renamer.

destruct H20 as [DX [HDX1 HDX2]].
clear_inversion HDX1.
clear_inversion_of_interptype HDX2.
compute_sth; renamer.

clear_inversion H33.
clear_inversion H23.

renamer.
clear_inversion H31.
clear_inversion H21.
clear_inversion H22.
clear_inversion H32.

assert (DA1_env0 === DA1_env0 #in# PerType) by (
  eapply PER_Transitive; eauto; symmetry; eauto).


apply RelProd_intro.
intros.

assert (PDA1_env0 =~= PDA1_env1) as EX.
eapply InterpType_extdeter.
eauto.
eapply InterpType_resp_PerType.
eauto.
symmetry.
eauto.

destruct H with a0 a1 as [y0 [y1 [Y [HY [Hy1 [Hy2 Hyy] ] ] ] ] ].
auto.

destruct H19 with a0 a1 as [y0' [y1' [Y' [HY' [Hy1' [Hy2' Hyy'] ] ] ] ] ].
apply EX; auto.

exists y0; exists y1'; exists Y.
repeat (split; auto).

destruct H14 with a0 as [DB HDB].
eauto.

assert (InterpType DB Y).
apply H12 with a0; eauto.

assert (PER PDA1_env0).
eauto.

assert (PER PDA1_env1).
eauto.

assert (InterpType DB Y').
apply H18 with a0; auto.
apply EX.
apply PER_Transitive with a1.
apply H22.
symmetry.
apply H22.

assert (Y =~= Y') as EXY.
eapply InterpType_extdeter; eauto.

assert (PER Y).
eauto.

assert (PER Y').
eauto.

move H10 at bottom.

assert ( [Dext env0 a0] === [Dext env1 a1] #in# Gamma,, A1 ) as Henv.
econstructor 2.
eauto.
eexists. split. eauto. eapply H0.
eauto.


destruct (H10 _ _ Henv) as [dtm0 [dtm1 [PA [HB1 [HB2 [HB3 HB4]]]]]].
renamer.

clear_inversion Hy1.
clear_inversion Hy2'.
compute_sth.

destruct HB1 as [X [DX1 DX2]].
clear_inversion HDB.
compute_sth.

assert (PDB0 =~= PA) as EXF.
eapply InterpType_extdeter; eauto.

apply EXF.
apply HB4.
Qed.
Hint Resolve Soundness_of_EQ_CONV_ABS.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_CONG_FUN_E: forall Gamma M1 M2 N1 N2 A B,
  Gamma |=  M1 === M2 : TmFun A B ->
  Gamma |=  N1 === N2 : A ->
  Gamma |=  TmApp M1 N1 === TmApp M2 N2 : TmSb B (Ssing N1)
.
Proof.
intros.

constructor 1; auto; intros; extract_vals; reds; auto.
apply Helper_APP with A; eauto.
(**)
apply_valenvs; program_simpl; compute_sth; renamer.
reds; program_simpl; compute_sth; renamer.
clear_inversion H24.
find_extdeters; close_extdeters.
specialize (H7 _ _ H10).
clear_inversion_of_pertype H28.
reds; program_simpl; compute_sth; renamer.
find_extdeters; close_extdeters; close_doms; move_inpers.

specialize (H24 _ H34).
reds; program_simpl; compute_sth; renamer.
clear_inversion H42.
clear_inversion H15. clear_inversion po_ro.
specialize (ro_resp_ex _ H36).
reds; program_simpl; compute_sth; renamer.

exists H7; exists H27; exists ro_resp_ex.
repeat split; eauto.
red. exists DB_env0_DN1_env0; repeat split; eauto.
eapply evalSb with (Dext env0 DN1_env0); eauto.
apply evalExt; eauto.
(**)
reds; program_simpl; compute_sth; renamer.
find_extdeters; close_extdeters; close_doms; move_inpers; move_inpers.

assert (InterpType  DB_env0_DN1_env0 H28).
eapply H19 with DN1_env0; auto. 

assert (InterpType  DB_env0_DN1_env0 ro_resp_ex).
eapply H19 with DN1_env0; auto. 
find_extdeters; close_extdeters.
auto.
Qed.
Hint Resolve Soundness_of_EQ_CONG_FUN_E.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SBAPP: forall Gamma S Delta M N A B,
  Gamma |=  [S]:Delta ->
  Delta |= M : TmFun A B ->
  Delta |= N : A ->
  Gamma |=  TmSb (TmApp M N) S === TmApp (TmSb M S) (TmSb N S) : TmSb B (Sext S (TmSb N S))
.
Proof.
intros.
assert (Gamma |= TmSb B (Sext S (TmSb N S))).
assert (Gamma |= [Sext S (TmSb N S)] : Delta,,A ).
apply Helper_SB_EXT; auto.
inversion H1; auto. 
inversion H1; auto.
eauto.
eapply Helper_SB_F; inversion H0; eauto.

constructor 1; auto; intros; extract_vals; reds; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.

clear_inversion_of_pertype H39.
program_simpl; compute_sth; reds; renamer.
clear_inversion H35.
clear_inversion H11; clear_inversion H12.
program_simpl; compute_sth; reds; renamer.
find_extdeters. close_extdeters. close_doms.
program_simpl; compute_sth; reds; renamer.  move_inpers.

destruct H0 with DN_DS_env0 DN_DS_env1; eauto.
program_simpl; compute_sth; reds; renamer; move_inpers.
clear_inversion H41; clear_inversion H42.
program_simpl; compute_sth; reds; renamer; move_inpers.

do 3 eexists; repeat split; eauto.
red; eexists; repeat split; eauto.

Qed.
Hint Immediate Soundness_of_EQ_SBAPP.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SBABS: forall Gamma S Delta M A B,
  Gamma |=  [S]:Delta ->
  Delta,, A |= M : B ->
  Gamma |=  TmSb (TmAbs A M) S === TmAbs (TmSb A S) (TmSb M (Sext (Sseq S Sup) TmVar)) : TmFun (TmSb A S) (TmSb B (Sext (Sseq S Sup) TmVar))
.
Proof.
intros.

assert (Gamma |= TmFun (TmSb A S) (TmSb B (Sext (Sseq S Sup) TmVar))).
eapply Helper_FUN_SB. eauto.
inversion H0; auto.
clear_inversion H0.
clear_inversion H1.
clear_inversion H0.
auto.

constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
assert (exists PF, InterpType (Dfun DA0 (Dclo (TmSb B (Sext (Sseq S Sup) TmVar)) env0)) PF) by eauto.
clear_inversion H21.
clear_inversion H19.
clear_inversion_of_pertype H12.
destruct H.
exists (Dclo M DS_env0).
exists (Dclo (TmSb M (Sext (Sseq S Sup) TmVar)) env1).
exists x.
repeat split; eauto.
red. eexists; repeat split; eauto.

program_simpl; compute_sth; reds; renamer.
constructor 1.

intros.
find_extdeters. close_extdeters. close_doms.
program_simpl; compute_sth; reds; renamer. move_inpers.

build_valenv Delta A DS_env0 DS_env1 a0 a1.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
destruct H5 with (Dext DS_env0 a0) (Dext DS_env1 a1); eauto.
program_simpl; compute_sth; reds; renamer.

destruct H25.
destruct po_ro.
destruct ro_resp_ex with a0; auto.
do 2 eexists; exists x.
repeat split; eauto.
assert (InterpType DB_DS_env0_a0 x).

apply H26 with a0; eauto.
find_extdeters.
close_extdeters.
auto.
Qed.
Hint Immediate Soundness_of_EQ_SBABS.
