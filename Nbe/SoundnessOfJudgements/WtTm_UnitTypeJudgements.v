(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Soundness of judgements for unit type (terms)
 *)

Set Implicit Arguments.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.SoundnessOfJudgements.ValEnv_Facts.

Require Import Nbe.SoundnessOfJudgements.WfTp_UnitTypeJudgements.

(******************************************************************************
 *)
Lemma Valid_UNIT_Eta: forall Gamma M,
  Gamma |= M : TmUnit ->
  Gamma |= M === Tm1 : TmUnit
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
eexists DM_env0.
eexists D1.
clear_inversion H1.
clear_inversion H11.

exists PerUnit.
repeat split; eauto.
red; exists Dunit; repeat split; eauto.
Qed.


Hint Resolve Valid_UNIT_Eta.

(******************************************************************************
 *)
Lemma Soundness_of_UNIT: forall Gamma,
  Gamma |= ->
  Gamma |= Tm1 : TmUnit
.
Proof.
intros.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.
repeat eexists; eauto.
Qed.
Hint Resolve Soundness_of_UNIT.

(******************************************************************************
 *)
Lemma Soundness_of_EQ_SB1: forall Gamma S Delta,
  Gamma |=  [S]:Delta ->
  Gamma |=  TmSb Tm1 S === Tm1 : TmUnit
.
Proof.
constructor 1; auto; intros; extract_vals; reds; auto;
compute_sth; eauto.

apply_valenvs; program_simpl; compute_sth; reds; renamer.
apply_valenvs; program_simpl; compute_sth; reds; renamer.
do 3 eexists; repeat split; eauto.
red; eexists ; repeat split; eauto.
Qed.
Hint Immediate Soundness_of_EQ_SB1.
