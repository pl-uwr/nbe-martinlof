(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Set Implicit Arguments.

Require Import List.
Require Import Arith.
Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Bool.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.SoundnessOfNbe.
Require Import Nbe.CompletenessOfNbe.

Require Import Nbe.CodeExtraction.Eval.
Require Import Nbe.CodeExtraction.Rb.
Require Import Nbe.CodeExtraction.Nbe.
Require Import Nbe.Domain.Nbe.

(******************************************************************************
 * Helpers
 *)

Definition Exists_apply: forall X Y:Type, forall P : X -> Y -> Prop, forall Q : X -> Prop, forall x,
  (exists y, P x y) -> (forall x y, P x y -> Q x) -> Q x
  := fun X Y P Q x H I =>
    let (y, Hy) := H in I x y Hy
.

Fact ValCxt_from_Tp: forall Gamma A, 
  Gamma |= A -> Gamma |=
.
Proof.
intros.
inversion H; assumption.
Qed.

Fact ValTp_from_Tm: forall Gamma t A, 
  Gamma |= t : A -> Gamma |= A
.
Proof.
intros.
inversion H; assumption.
Qed.

(******************************************************************************
 * Evaluators for well-formed/typed
 *)

Definition wfCxt_eval Gamma (H: Gamma |--) : { denv | EvalCxt Gamma denv } :=
  let Hexists := Terminating_on_WfCxt H in 
  let Hdom    := Exists_apply EvalCxt EvalCxt_Dom Gamma Hexists EvalCxt_Dom_corr in
  evalCxt Hdom
.

Definition wfTp_eval Gamma A (H: Gamma |-- A)
  : { DA | exists denv, EvalCxt Gamma denv /\ EvalTm A denv DA } :=
  let HValTp    := Valid_for_WfTp H in
  let HValCxt   := ValCxt_from_Tp HValTp in
  let HTermCxt  := Terminating_on_ValCxt1 HValCxt in
  let HDomCxt   := Exists_apply EvalCxt EvalCxt_Dom Gamma HTermCxt EvalCxt_Dom_corr in
  let (de,Hde)  := evalCxt HDomCxt in
  let HTermTp   := Terminating_on_WfTp H Hde in
  let HDomTp    := Exists_apply (EvalTm A) (EvalTm_Dom A) de HTermTp (@EvalTm_Dom_corr A) in
  let (dt,Hdt)  := evalTm HDomTp in
  let Hprf      := conj Hde Hdt in
  exist _ dt  (ex_intro _ de Hprf)
.


Definition wtTm_eval Gamma t A (H: Gamma |-- t : A)
  : { dt | exists denv, EvalCxt Gamma denv /\ EvalTm t denv dt } :=
  let HValTm    := Valid_for_WtTm H in
  let HValTp    := ValTp_from_Tm HValTm in
  let HValCxt   := ValCxt_from_Tp HValTp in
  let HTermCxt  := Terminating_on_ValCxt1 HValCxt in
  let HDomCxt   := Exists_apply EvalCxt EvalCxt_Dom Gamma HTermCxt EvalCxt_Dom_corr in
  let (de,Hde)  := evalCxt HDomCxt in
  let HTermTp   := Terminating_on_WtTm H Hde in
  let HDomTp    := Exists_apply (EvalTm t) (EvalTm_Dom t) de HTermTp (@EvalTm_Dom_corr t) in
  let (dt,Hdt)  := evalTm HDomTp in
  let Hprf      := conj Hde Hdt in
  exist _ dt  (ex_intro _ de Hprf)
.
Extraction Implicit wtTm_eval [A]
.

Extraction Language Haskell.

Lemma WtTm_InNbeDom: forall Gamma t T,
  Gamma |-- t : T ->
  Nbe_Dom T Gamma t
.
Proof.
intros.
destruct Nbe_Soundness' with Gamma t T; auto.
program_simpl.
eapply Nbe_Dom_corr with x.
assumption.
Qed.


Definition wtTm_nbe Gamma t A (H: Gamma |-- t : A)
  : { v | Nbe A Gamma t v }
 := nbe (WtTm_InNbeDom H)
.


Definition wtTm_nbetest Gamma t s A (Ht: Gamma |-- t : A) (Hs: Gamma |-- s : A)
  : { Gamma |-- t === s : A } + { ~ Gamma |-- t === s : A }
.
refine (
 let (vt, Hvt) := wtTm_nbe Ht in 
 let (vs, Hvs) := wtTm_nbe Hs in 
 match Tm_eqdec vt vs with
   | left Heq => left _
   | right Hneq => right _
 end
).
(* yes *)
subst; eauto.
set (Hnbe_vt := Nbe_Soundness Ht Hvt).
set (Hnbe_vs := Nbe_Soundness Hs Hvs).
eauto.
(* no *)
intro H.
contradict Hneq.
eapply NBE_Determined; eauto.
Defined.

Definition nf Gamma t A (H: Gamma |-- t : A) : { v | Nf v /\ Gamma |-- t === v : A }.
edestruct wtTm_nbe; eauto.
exists x.
split.

eapply Nbe_gives_nf; eauto.
eapply Nbe_Soundness; eauto.
Defined.


Extraction Inline wtTm_nbe.

 
