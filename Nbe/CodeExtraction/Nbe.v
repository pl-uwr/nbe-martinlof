(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Set Implicit Arguments.

Require Import List.
Require Import Arith.
Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Bool.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.CodeExtraction.Eval.
Require Import Nbe.CodeExtraction.Rb.

(******************************************************************************
 * PARTIAL FUNCTIONS FROM Nbe
 *)

Inductive Nbe_Dom (A:Tm) (Gamma:Cxt) (t:Tm) : Prop :=
  | NbeD_intro:
    EvalCxt_Dom Gamma ->
    (forall denv, EvalCxt Gamma denv -> EvalTm_Dom A denv) ->
    (forall denv, EvalCxt Gamma denv -> EvalTm_Dom t denv) ->
    (forall denv DA d, EvalCxt Gamma denv ->
      EvalTm A denv DA ->
      EvalTm t denv d ->
      RbNf_Dom (length Gamma) (Ddown DA d)) ->
    Nbe_Dom A Gamma t
.


(******************************************************************************
 * Nbe Dom Correctness
 *)

Theorem Nbe_Dom_corr: 
  forall Gamma t T v,
    Nbe T Gamma t v -> Nbe_Dom T Gamma t
.
Proof.
intros.
induction H.
constructor 1.
eapply EvalCxt_Dom_corr; eauto.
intros; assert (denv0 = denv) by (eapply EvalCxt_deter; eauto); subst.
eapply EvalTm_Dom_corr; eauto.
intros; assert (denv0 = denv) by (eapply EvalCxt_deter; eauto); subst.
eapply EvalTm_Dom_corr; eauto.

intros; assert (denv0 = denv) by (eapply EvalCxt_deter; eauto); subst.
assert (DA0 = DA) by eauto; subst.
assert (d0 = d) by eauto; subst.
eapply RbNf_Dom_corr; eauto.
Qed.

(******************************************************************************
 * Inversions for RbNe_Dom
 *)

Definition Nbe_Dom_invG:
  forall A Gamma t,
  Nbe_Dom A Gamma t ->
  EvalCxt_Dom Gamma
.
intros.
auto_inversion_on H.
intros.
assumption.
Defined.

Definition Nbe_Dom_invA:
  forall A Gamma t,
  Nbe_Dom A Gamma t ->
  (forall denv, EvalCxt Gamma denv -> EvalTm_Dom A denv)
.
intros.
auto_inversion_on H.
intros.
apply H2.
assumption.
Defined.

Definition Nbe_Dom_invT:
  forall A Gamma t,
  Nbe_Dom A Gamma t ->
  (forall denv, EvalCxt Gamma denv -> EvalTm_Dom t denv)
.
intros.
auto_inversion_on H.
intros.
apply H3.
assumption.
Defined.

Definition Nbe_Dom_invR:
  forall A Gamma t,
  Nbe_Dom A Gamma t ->
  (forall denv DA d, EvalCxt Gamma denv ->
    EvalTm A denv DA ->
    EvalTm t denv d ->
    RbNf_Dom (length Gamma) (Ddown DA d))
.
intros.
auto_inversion_on H.
intros denv0 H' H'' H'''.
apply H''' with denv; auto.
Defined.

(******************************************************************************
 * Functions
 *)

Program Definition nbe (A:Tm) (Gamma:Cxt) (t:Tm) (H:Nbe_Dom A Gamma t)
  : { v | Nbe A Gamma t v } :=
  let (denv, Hdenv) := evalCxt (Nbe_Dom_invG H) in
  let (DA  , HDA  ) := evalTm  (Nbe_Dom_invA H Hdenv) in
  let (d   , Hd   ) := evalTm  (Nbe_Dom_invT H Hdenv) in
  let (v   , Hv   ) := rbNf    (Nbe_Dom_invR H Hdenv HDA Hd) in
  v
.

(******************************************************************************
 * Extraction parameters
 *)

Extraction Inline nbe_obligation_1.

