(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Set Implicit Arguments.
Require Import List.
Require Import Arith.
Require Import Program.
Require Import Program.Tactics.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Bool.

Require Import Nbe.Utils.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.

(******************************************************************************
 * PARTIAL FUNCTIONS FROM EvalTm, EvalSb, App, EvalCxt
 *)

(******************************************************************************
 * Domains for functions
 *)

Inductive EvalTm_Dom: Tm -> DEnv -> Prop :=
| evalVarD : forall d env,
  EvalTm_Dom TmVar (Dext env d)

| evalAbsD : forall A t env,
  EvalTm_Dom (TmAbs A t) env

| evalAppD : forall tf tx env,
  EvalTm_Dom tf env ->
  EvalTm_Dom tx env ->
  (forall df dx, EvalTm tf env df -> EvalTm tx env dx -> App_Dom df dx) ->
  EvalTm_Dom (TmApp tf tx) env

| eval0D : forall env,
  EvalTm_Dom Tm0 env

| evalSD : forall tn env,
  EvalTm_Dom tn env ->
  EvalTm_Dom (TmS tn) env

| evalSbD : forall denv s t,
  EvalSb_Dom s denv ->
  (forall denvs, EvalSb s denv denvs -> EvalTm_Dom t denvs) ->
  EvalTm_Dom (TmSb t s) denv

| eval1D: forall denv,
  EvalTm_Dom (Tm1) denv

| evalUnitD: forall denv,
  EvalTm_Dom TmUnit denv

| evalNat: forall denv,
  EvalTm_Dom TmNat denv

| evalFunD: forall A B denv,
  EvalTm_Dom A denv ->
  EvalTm_Dom (TmFun A B) denv

| evalEmptyD : forall denv,
  EvalTm_Dom TmEmpty denv

| evalUnivD : forall denv,
  EvalTm_Dom TmUniv denv

| evalAbsurdD : forall A denv,
  EvalTm_Dom A denv ->
  EvalTm_Dom (TmAbsurd A) denv

with EvalSb_Dom : Sb -> DEnv -> Prop :=

| evalIdD: forall denv,
  EvalSb_Dom Sid denv

| evalUpD: forall denv d,
  EvalSb_Dom Sup (Dext denv d)

| evalSeqD: forall denv0 s1 s2,
  EvalSb_Dom s2 denv0 ->
  (forall denv1, EvalSb s2 denv0 denv1 -> EvalSb_Dom s1 denv1) ->
  EvalSb_Dom (Sseq s1 s2) denv0

| evalExtD: forall denv0 s M,
  EvalSb_Dom s denv0 ->
  EvalTm_Dom M denv0 ->
  EvalSb_Dom (Sext s M) denv0

with App_Dom: D -> D -> Prop :=

| appCloD : forall tm denv dx,
  EvalTm_Dom tm (Dext denv dx) ->
  App_Dom (Dclo tm denv) dx

| appArrD: forall DA DF e a,
  App_Dom DF a ->
  App_Dom (DupX (Dfun DA DF) e) a

| appAbsurdneD: forall DA u,
  App_Dom (Dabsurd DA) (DupN u)
.

Hint Constructors EvalTm_Dom.
Hint Constructors EvalSb_Dom.
Hint Constructors App_Dom.

Inductive EvalCxt_Dom: Cxt -> Prop :=
 | evalCxtEmptyD: 
   EvalCxt_Dom nil

 | evalCxtExtD: forall Gamma A,
   EvalCxt_Dom Gamma ->
   (forall denv, EvalCxt Gamma denv -> EvalTm_Dom A denv) ->
   EvalCxt_Dom (Gamma,,A)
.
Hint Constructors EvalCxt_Dom.

(******************************************************************************
 * 
 *)

Theorem Evals_Dom_Correctness: 
  (forall t denv d,
    EvalTm t denv d -> EvalTm_Dom t denv
  ) /\
  (forall s denv d,
    EvalSb s denv d -> EvalSb_Dom s denv
  ) /\
  (forall df dx d,
    App df dx d -> App_Dom df dx
  )
.
Proof.
apply Evals_ind; intros; eauto.
(**)
{
apply evalAppD; auto.
intros.
replace df0 with df.
replace dx0 with dx.
assumption.
eapply EvalTm_deter; eauto.
eapply EvalTm_deter; eauto.
}
(**)
{
apply evalSbD; auto.
intros.
replace denvs with envs.
assumption.
eapply EvalSb_deter; eauto.
}
(**)
{
apply evalSeqD; auto.
intros.
replace env1 with denv1 in *.
assumption.
eapply EvalSb_deter; eauto.
}
Qed.

Definition EvalTm_Dom_corr := proj1 Evals_Dom_Correctness.
Definition EvalSb_Dom_corr := proj1 (proj2 Evals_Dom_Correctness).
Definition App_Dom_corr    := proj2 (proj2 Evals_Dom_Correctness).

Hint Resolve EvalTm_Dom_corr.
Hint Resolve EvalSb_Dom_corr.
Hint Resolve App_Dom_corr.

Theorem EvalCxt_Dom_corr: forall Gamma denv,
  EvalCxt Gamma denv -> EvalCxt_Dom Gamma
.
Proof.
intros.
induction H; eauto.
apply evalCxtExtD; auto.
intros.
replace denv0 with denv; auto.
eapply EvalTm_Dom_corr; eauto.
eapply EvalCxt_deter; eauto.
Qed.


(******************************************************************************
 * Inversions for EvalTm_Dom
 *)

Definition EvalTm_Dom_S_inv: forall tm tm0 denv,
  EvalTm_Dom tm denv ->
  tm = TmS tm0 ->
  EvalTm_Dom tm0 denv
.
intros tm tm0 denv Htm.
auto_inversion.
intros tn env Hdom Hx.
inversion Hx.
rewrite <- H0.
assumption.
Defined.
Hint Resolve EvalTm_Dom_S_inv.

Definition EvalTm_Dom_app_invF: forall tm tm0 tm1 denv,
  EvalTm_Dom tm denv ->
  tm = TmApp tm0 tm1 ->
  EvalTm_Dom tm0 denv
.
intros tm tm0 tm1 denv Htm.
auto_inversion.

intros tf tx env Htf Htx.
intros HApp H; injection H.
intros Hf Hx.
rewrite <- Hx; assumption.
Defined.
Hint Resolve EvalTm_Dom_app_invF.

Definition EvalTm_Dom_app_invX: forall tm tm0 tm1 denv,
  EvalTm_Dom tm denv ->
  tm = TmApp tm0 tm1 ->
  EvalTm_Dom tm1 denv
.
intros tm tm0 tm1 denv Htm.
auto_inversion.

intros tf tx env Htf Htx.
intros HApp H; injection H.
intros Hf Hx.
rewrite <- Hf; assumption.
Defined.
Hint Resolve EvalTm_Dom_app_invX.

Definition EvalTm_Dom_app_invA: forall tm tm0 tm1 denv,
  EvalTm_Dom tm denv ->
  tm = TmApp tm0 tm1 ->
  forall df dx : D,  EvalTm tm0 denv df -> EvalTm tm1 denv dx -> App_Dom df dx
.
intros tm tm0 tm1 denv Htm.
auto_inversion.

intros tf tx env Htf Htx.
intros HApp H; injection H.
intros Hf Hx df dx.
rewrite <- Hf.
rewrite <- Hx.
auto.
Defined.
Hint Resolve EvalTm_Dom_app_invA.


Definition EvalTm_Dom_sb_inv1:
  forall denv tm t s,
  EvalTm_Dom tm denv ->
  tm = TmSb t s ->
  EvalSb_Dom s denv.
intros denv tm t s H.
auto_inversion.
intros denv0 s0 t0 H1 H2 Hi.
injection Hi.
intro Hr0; rewrite <- Hr0.
intro Hr1.
assumption.
Defined.
Hint Resolve EvalTm_Dom_sb_inv1.

Definition EvalTm_Dom_sb_inv2:
  forall denv tm t s,
  EvalTm_Dom tm denv ->
  tm = TmSb t s ->
  (forall denvs, EvalSb s denv denvs -> EvalTm_Dom t denvs)
.
intros denv tm t s H.
auto_inversion.
intros denv0 s0 t0 H1 H2 Hi.
injection Hi.
intro Hr0; rewrite <- Hr0.
intro Hr1; rewrite <- Hr1.
assumption.
Defined.
Hint Resolve EvalTm_Dom_sb_inv2.

Definition EvalTm_Dom_fun_inv1: forall tp denv A B,
  EvalTm_Dom tp denv ->
  tp = TmFun A B ->
  EvalTm_Dom A denv
.
intros tp denv A B Htp.
auto_inversion.
intros.
injection H0.
intro Hr0.
intro Hr1; rewrite <- Hr1.
assumption.
Defined.
Hint Resolve EvalTm_Dom_fun_inv1.

Definition EvalTm_Dom_absurd_inv: forall A tm denv,
  EvalTm_Dom tm denv ->
  tm = TmAbsurd A ->
  EvalTm_Dom A denv
.
Proof.
intros A tm denv H.
auto_inversion.
intros.
injection H1.
intro Hr0. rewrite <- Hr0.
assumption.
Defined.
Hint Resolve EvalTm_Dom_absurd_inv.

(******************************************************************************
 * Inversions for EvalSb_Dom
 *)

Definition EvalSb_Dom_seq_inv1:
  forall denv0 s s1 s2,
  EvalSb_Dom s denv0 ->
  s = Sseq s1 s2 ->
  EvalSb_Dom s2 denv0
.
intros denv0 s s1 s2 Hs.
auto_inversion.

intros.
injection H1.
intros Hr0; intros Hr1; rewrite <- Hr0.
assumption.
Defined.
Hint Resolve EvalSb_Dom_seq_inv1.

Definition EvalSb_Dom_seq_inv2:
  forall denv0 s s1 s2,
  EvalSb_Dom s denv0 ->
  s = Sseq s1 s2 ->
  (forall denv1, EvalSb s2 denv0 denv1 -> EvalSb_Dom s1 denv1)
.
intros denv0 s s1 s2 Hs.
auto_inversion.

intros denv1 s' s0 s3 H1 H.
injection H.
intros Hr0; rewrite <- Hr0.
intros Hr1; rewrite <- Hr1.
assumption.
Defined.
Hint Resolve EvalSb_Dom_seq_inv2.

Definition EvalSb_Dom_ext_inv1:
  forall s denv0 s1 M,
  EvalSb_Dom s denv0 ->
  s = (Sext s1 M) ->
  EvalSb_Dom s1 denv0
.
intros s denv0 s1 M Hs.
auto_inversion.

intros denv1 s0 M0.
intros Hs0 HM0 H.
injection H.
intro Hr0.
intro Hr1; rewrite <- Hr1.
assumption.
Defined.
Hint Resolve EvalSb_Dom_ext_inv1.

Definition EvalSb_Dom_ext_inv2:
  forall s denv0 s1 M,
  EvalSb_Dom s denv0 ->
  s = (Sext s1 M) -> 
  EvalTm_Dom M denv0
.
intros s denv0 s1 M Hs.
auto_inversion.
intros denv1 s0 M0.
intros Hs0 HM0 H.
injection H.
intro Hr0; rewrite <- Hr0.
intro Hr1. 
assumption.
Defined.
Hint Resolve EvalSb_Dom_ext_inv2.

(******************************************************************************
 * Inversions for App_Dom
 *)

Definition App_Dom_clo_inv: forall df tm denv dx,
  App_Dom df dx ->
  df = Dclo tm denv ->
  EvalTm_Dom tm (Dext denv dx)
.
intros df tm denv dx Hdf.
auto_inversion.

intros tm0 denv0 dx0 Htm0.
intros H; injection H.
intros.
rewrite <- H0. rewrite <- H1.
assumption.
Defined.
Hint Resolve App_Dom_clo_inv.

Definition App_Dom_fun_inv: forall df dx DA DF e,
  App_Dom df dx ->
  df = (DupX (Dfun DA DF) e) ->
  App_Dom DF dx
.
intros df dx DA DF e Hdf.
auto_inversion.
intros.
injection H0.
intro Hr0.
intro Hr1; rewrite <- Hr1.
intro Hr2.
assumption.
Defined.
Hint Resolve App_Dom_fun_inv.
  

(******************************************************************************
 * Inversions for EvalCxt_Dom
 *)

Definition EvalCxt_Dom_ext_inv1: forall Gamma Delta A,
  EvalCxt_Dom Gamma ->
  Gamma = (Delta,,A) ->
  EvalCxt_Dom Delta
.
intros Gamma Delta A HG.
auto_inversion.
intros Gamma0 A0 HGamma0 H Heq.
injection Heq.
intro Hr0; rewrite <- Hr0.
intro Hr1.
assumption.
Defined.
Hint Resolve EvalCxt_Dom_ext_inv1.

Definition EvalCxt_Dom_ext_inv2: forall Gamma Delta A,
  EvalCxt_Dom Gamma ->
  Gamma = (Delta,,A) ->
  (forall denv, EvalCxt Delta denv -> EvalTm_Dom A denv)
.
intros Gamma Delta A HG.
auto_inversion.
intros Gamma0 A0 HGamma0 H Heq.
injection Heq.
intro Hr0; rewrite <- Hr0.
intro Hr1; rewrite <- Hr1.
assumption.
Defined.
Hint Resolve EvalCxt_Dom_ext_inv2.

(******************************************************************************
 * Evaluator code
 *)

Obligation Tactic := try solve [
  try solve [ program_simpl ];
  intros; subst;
  program_simpl; eauto;
  try match goal with
    | [ H:_ |- _ ] =>
      solve [ contradict H; intro H; inversion H ]
  end ]
.

Program Fixpoint evalTm (tm:Tm) (denv:DEnv) (H:EvalTm_Dom tm denv) {struct H}:
  { d:D | EvalTm tm denv d } :=
  match tm as T
    return (tm = T -> { d:D | EvalTm T denv d })
    with

    | TmVar => fun _  =>
      match denv with
        | Did => _
        | Dext denv' d' => d'
      end

    | Tm0 => fun H' => D0

    | TmS n => fun H' =>
       let (dx, Hdx) := evalTm (EvalTm_Dom_S_inv H H') in
       DS dx
(*
    | DS => fun H' =>
      match dx as DX return dx = DX -> { d:D | App df DX d } with
        | Dnv n => fun H'' =>
          (Dnv (S n))

        | DupN u => fun H'' =>
          DupN (DneS u)

        | _ => _
      end (eq_refl dx)
*)
    | TmAbs A tm0 => fun  _ =>
      Dclo tm0 denv 

    | TmApp tm0 tm1 => fun H'  =>
      let (df,Hdf) := evalTm (EvalTm_Dom_app_invF H H') in
      let (dx,Hdx) := evalTm (EvalTm_Dom_app_invX H H') in
      let (dy,Hdy) := app  (EvalTm_Dom_app_invA H H' Hdf Hdx) in
      dy


    | TmSb tm0 sb0 => fun H' =>
      let (denvs, Hdenvs) := evalSb (EvalTm_Dom_sb_inv1 H H') in
      let (d,     Hd)     := evalTm (EvalTm_Dom_sb_inv2 H H' Hdenvs) in
      d


    | Tm1 => fun H' =>
      D1

    | TmNat => fun _ =>
      Dnat

    | TmUnit => fun _ =>
      Dunit

    | TmEmpty => fun _ =>
      Dempty

    | TmUniv => fun _ =>
      Duniv

    | TmAbsurd A => fun H' =>
      let (DA, HDA) := evalTm (EvalTm_Dom_absurd_inv H H') in
      Dabsurd DA

    | TmFun tp0 tp1 => fun H' =>
      let (DA, HDA) := evalTm (EvalTm_Dom_fun_inv1 H H') in
      Dfun DA (Dclo tp1 denv)

  end (eq_refl tm) 

with evalSb (sb:Sb) (denv:DEnv) (H:EvalSb_Dom sb denv) {struct H}:
  { rdenv:DEnv | EvalSb sb denv rdenv } :=
  match sb as SB
    return (sb = SB -> { rdenv:DEnv | EvalSb SB denv rdenv })
    with

    | Sid => fun _ =>
        denv

    | Sup => fun _ =>
        match denv with
          | Did =>
            _

          | Dext denv' d' => 
            denv'
        end

    | Sseq s1 s2 => fun H' =>
      let (denv1, Hdenv1) := evalSb (EvalSb_Dom_seq_inv1 H H') in
      let (denv2, Hdenv2) := evalSb (EvalSb_Dom_seq_inv2 H H' Hdenv1) in
      denv2

    | Sext s1 M1 => fun H' =>
      let (denv1, Hdenv1) := evalSb (EvalSb_Dom_ext_inv1 H H') in
      let (d    , Hd    ) := evalTm (EvalSb_Dom_ext_inv2 H H') in
      Dext denv1 d

  end (eq_refl sb)

with app (df dx:D) (H:App_Dom df dx) {struct H} :
  { d:D | App df dx d } :=
  match df as DF
    return df = DF -> { d:D | App DF dx d }
    with

    | Dclo tm denv => fun H' =>
      let (dy, Hdy) := evalTm (App_Dom_clo_inv H H') in
      let H         := appClo Hdy in
        exist (fun d => App (Dclo tm denv) dx d)
        dy H

    | DupX (Dfun DA DF) e => fun H' =>
      let (DB, HDB) := app (App_Dom_fun_inv H H') in
      Dup DB (Dapp e (Ddown DA dx))

    | Dabsurd DA => fun H' =>
      match dx with
        | DupN u => (Dup DA $ DneAbsurd (DdownN DA) u)
        | _ => _
      end


    | _ => _

  end (eq_refl df) 
.

(*
Obligations of evalTm.

Obligations of evalSb.

Obligations of app.
*)

Obligation 22 of app.
intros.
program_simpl.
contradict H.  intro H. inversion H.
subst.
apply (H0 u). reflexivity.
Qed.


(******************************************************************************
 * evalCxt
 *)
Program Fixpoint evalCxt (Gamma:Cxt) (H:EvalCxt_Dom Gamma) :
  { denv:DEnv | EvalCxt Gamma denv } :=
  match Gamma as G
    return (Gamma = G -> { denv:DEnv | EvalCxt G denv })
    with
    | nil    => fun H' => 
      Did

    | Gamma',,A' => fun H' =>
      let (denv, Hdenv) := evalCxt (EvalCxt_Dom_ext_inv1 H H') in
      let (d   , Hd)    := evalTm (EvalCxt_Dom_ext_inv2 H H' Hdenv) in
      Dext denv (Dup d $ Dvar $ length Gamma')
  end (eq_refl Gamma)
.

(******************************************************************************
 * Extraction parameters
 *)

Extraction Inline evalTm_obligation_1.
Extraction Inline evalTm_obligation_2.
Extraction Inline evalTm_obligation_3.
Extraction Inline evalTm_obligation_4.
Extraction Inline evalTm_obligation_5.
Extraction Inline evalTm_obligation_6.
Extraction Inline evalTm_obligation_7.
Extraction Inline evalTm_obligation_8.
Extraction Inline evalTm_obligation_9.
Extraction Inline evalTm_obligation_10.
Extraction Inline evalTm_obligation_11.
Extraction Inline evalTm_obligation_12.
Extraction Inline evalTm_obligation_13.
Extraction Inline evalTm_obligation_14.

Extraction Inline evalSb_obligation_1.
Extraction Inline evalSb_obligation_2.
Extraction Inline evalSb_obligation_3.
Extraction Inline evalSb_obligation_4.
Extraction Inline evalSb_obligation_5.

Extraction Inline app_obligation_1.
Extraction Inline app_obligation_2.
Extraction Inline app_obligation_3.
Extraction Inline app_obligation_4.
Extraction Inline app_obligation_5.
Extraction Inline app_obligation_6.
Extraction Inline app_obligation_7.
Extraction Inline app_obligation_8.
Extraction Inline app_obligation_9.
Extraction Inline app_obligation_10.
Extraction Inline app_obligation_11.
Extraction Inline app_obligation_12.
Extraction Inline app_obligation_13.
Extraction Inline app_obligation_14.
Extraction Inline app_obligation_15.
Extraction Inline app_obligation_16.
Extraction Inline app_obligation_17.
Extraction Inline app_obligation_18.
Extraction Inline app_obligation_19.
Extraction Inline app_obligation_20.
Extraction Inline app_obligation_21.
Extraction Inline app_obligation_22.
Extraction Inline app_obligation_23.
Extraction Inline app_obligation_24.
Extraction Inline app_obligation_25.
Extraction Inline app_obligation_26.
Extraction Inline app_obligation_27.
Extraction Inline app_obligation_28.
Extraction Inline app_obligation_29.
Extraction Inline app_obligation_30.
Extraction Inline app_obligation_31.
Extraction Inline app_obligation_32.
Extraction Inline app_obligation_33.
Extraction Inline app_obligation_34.

Extraction Inline evalCxt_obligation_1.
Extraction Inline evalCxt_obligation_2.

