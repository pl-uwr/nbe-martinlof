(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.Lift.

(******************************************************************************
 *)

Theorem RelReify1_Duniv_b: forall Gamma T t dt
  (i : forall d1 d2 : D, d1 === d2 #in# PerUniv -> d1 === d2 #in# PerType),
  (forall (d1 d2 : D) (i0 : d1 === d2 #in# PerUniv) (Gamma : Cxt) (T : Tm),
      Gamma ||- T #in# i d1 d2 i0 ->
      (exists A : Tm, RbNf (length Gamma) (DdownN d1) A /\ Gamma |-- T === A) /\
      (forall (t : Tm) (dt : D),
       Gamma ||- t : T #aeq# dt #in# i d1 d2 i0 ->
       exists v : Tm,
         RbNf (length Gamma) (Ddown d1 dt) v /\ Gamma |-- t === v : T) /\
      (forall (t : Tm) (k : DNe),
       k === k #in# PerNe ->
       (forall (Delta : Cxt) (i : nat),
        CxtShift Delta i Gamma ->
        exists tv : Tm,
          RbNe (length Delta) k tv /\
          Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
       Gamma ||- t : T #aeq# Dup d1 k #in# i d1 d2 i0)) ->
  Gamma ||- T #in# PerType_univ i ->
  Gamma ||- t : T #aeq# dt #in# PerType_univ i ->
   exists v : Tm,
     RbNf (length Gamma) (Ddown Duniv dt) v /\ Gamma |-- t === v : T
.
Proof.
intros.
simpl in *.
decompose record H1.


assert (CxtShift Gamma 0 Gamma) as HX1 by eauto.
specialize (H3 _ _ HX1).
destruct H3 as [tv [HTV1 HTV2]].
exists tv; split; auto.
eapply EQ_CONV; [ | eapply EQ_TP_SYM; eapply H4 ].
eapply EQ_TRANS; [ | eassumption ].
simpl.
eauto.
Qed.
Hint Immediate RelReify1_Duniv_b.

(******************************************************************************
 *)
Theorem RelReify1_Duniv_c: forall Gamma T t k
  (i : forall d1 d2 : D, d1 === d2 #in# PerUniv -> d1 === d2 #in# PerType),
  (forall (d1 d2 : D) (i0 : d1 === d2 #in# PerUniv) (Gamma : Cxt) (T : Tm),
      Gamma ||- T #in# i d1 d2 i0 ->
      (exists A : Tm, RbNf (length Gamma) (DdownN d1) A /\ Gamma |-- T === A) /\
      (forall (t : Tm) (dt : D),
       Gamma ||- t : T #aeq# dt #in# i d1 d2 i0 ->
       exists v : Tm,
         RbNf (length Gamma) (Ddown d1 dt) v /\ Gamma |-- t === v : T) /\
      (forall (t : Tm) (k : DNe),
       k === k #in# PerNe ->
       (forall (Delta : Cxt) (i : nat),
        CxtShift Delta i Gamma ->
        exists tv : Tm,
          RbNe (length Delta) k tv /\
          Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
       Gamma ||- t : T #aeq# Dup d1 k #in# i d1 d2 i0)) ->
  Gamma ||- T #in# PerType_univ i ->
  k === k #in# PerNe ->
  (forall (Delta : Cxt) (i : nat),
       CxtShift Delta i Gamma ->
       exists tv : Tm,
         RbNe (length Delta) k tv /\
         Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
   Gamma ||- t : T #aeq# Dup Duniv k #in# PerType_univ i
.
Proof.
intros.

simpl in *.

assert (CxtShift Gamma 0 Gamma) as HX1 by eauto.

destruct (H2 _ _ HX1) as [tv [H3 H4]].

assert (Dup Duniv k === Dup Duniv k #in# PerUniv) as HX2.
{
eauto.
}

assert (Gamma |-- TmSb t (Sups 0) : TmSb T (Sups 0)) as HX3 by eauto.
assert (Gamma |-- t : T) as HX4 by eauto.
assert (Gamma |-- t : TmUniv) as HX5 by eauto.


split; [ | split ]; auto.
split.
+{
intros.

destruct (H2 _ _ H5).
destruct H6.
exists x.
split; auto.
eapply EQ_CONV; auto.
eassumption.
eauto.
}


+{ 
exists HX2.
generalize dependent (i (Dup Duniv k) (Dup Duniv k) HX2).
cut (Dup Duniv k = Dup Duniv k); [ | auto ].
cut (Dup Duniv k = Dup Duniv k); [ | auto ].

generalize (Dup Duniv k) at 1 5 7 as d1.
generalize (Dup Duniv k) at 2 4 5 as d2.
intros.

induction i0 using PerType_dind; try solve [clear_inversion H5].
simpl.
split; auto.
intros.

specialize (H2 _ _ H7).
destruct H2 as [tv' [HY1 HY2]].
exists tv'.
split.
{
rewrite Dup_Duniv.
eapply reifyNf_ne.
clear_inversion H5.
auto.
}

{
eapply EQ_TP_UNIV_ELEM.
eapply EQ_CONV.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL; eauto.
eapply H0.
eauto.
}
}
Qed.
Hint Immediate RelReify1_Duniv_c.

