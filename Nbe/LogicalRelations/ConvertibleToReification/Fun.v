(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.Lift.
Require Import Nbe.Syntax.LogRelTmSyntaxStuff.

(******************************************************************************
 *)

Theorem RelReify1_Dfun_a:
  forall DA DF DA' DF' PA
    (i : DA === DA' #in# PerType)    
    (IHHDT : forall (Gamma : Cxt) (T : Tm),
          Gamma ||- T #in# i ->
          (exists A : Tm,
             RbNf (length Gamma) (DdownN DA) A /\ Gamma |-- T === A) /\
          (forall (t : Tm) (dt : D),
           Gamma ||- t : T #aeq# dt #in# i ->
           exists v : Tm,
             RbNf (length Gamma) (Ddown DA dt) v /\ Gamma |-- t === v : T) /\
          (forall (t : Tm) (k : DNe),
           k === k #in# PerNe ->
           (forall (Delta : Cxt) (i : nat),
            CxtShift Delta i Gamma ->
            exists tv : Tm,
              RbNe (length Delta) k tv /\
              Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
           Gamma ||- t : T #aeq# Dup DA k #in# i))
    (i0 : InterpType DA PA)
    (e : forall a : D, a === a #in# PA -> exists DB : D, App DF a DB)
    (e0 : forall a : D, a === a #in# PA -> exists DB' : D, App DF' a DB')
    (i1 : forall (a0 a1 DB0 DB1 : D) (P : relation D),
       InterpType DA P ->
       a0 === a1 #in# P ->
       App DF a0 DB0 -> App DF' a1 DB1 -> DB0 === DB1 #in# PerType)
    (H : forall (a0 a1 DB0 DB1 : D) (P : relation D) (i : InterpType DA P)
        (i0 : a0 === a1 #in# P) (a : App DF a0 DB0) 
        (a2 : App DF' a1 DB1) (Gamma : Cxt) (T : Tm),
      Gamma ||- T #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
      (exists A : Tm, RbNf (length Gamma) (DdownN DB0) A /\ Gamma |-- T === A) /\
      (forall (t : Tm) (dt : D),
       Gamma ||- t : T #aeq# dt #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
       exists v : Tm,
         RbNf (length Gamma) (Ddown DB0 dt) v /\ Gamma |-- t === v : T) /\
      (forall (t : Tm) (k : DNe),
       k === k #in# PerNe ->
       (forall (Delta : Cxt) (i1 : nat),
        CxtShift Delta i1 Gamma ->
        exists tv : Tm,
          RbNe (length Delta) k tv /\
          Delta |-- TmSb t (Sups i1) === tv : TmSb T (Sups i1)) ->
       Gamma ||- t : T #aeq# Dup DB0 k #in# i1 a0 a1 DB0 DB1 P i i0 a a2))
   (Gamma : Cxt)
   (T : Tm)
   (H0 : Gamma ||- T #in# PerType_fun i i0 e e0 i1),


   exists A : Tm,
     RbNf (length Gamma) (DdownN (Dfun DA DF)) A /\ Gamma |-- T === A
.
Proof.
intros.
renamer; intros.
rename i into HDA.
rename i1 into HREC.
rename i0 into HPDA.
rename e0 into e'.

simpl in H0.
destruct H0 as [A [F [HS1 [HS2 HS3]]]].
 
move IHHDT at bottom.
set (IHHDT2 := IHHDT).
specialize (IHHDT2).
specialize (IHHDT Gamma A HS2).
destruct IHHDT as [HI1 [HI2 HI3]].
destruct HI1 as [NFA HNFA].
destruct HNFA as [HNFA1 HNFA2].

 
move H at bottom.
 

set (var := Dvar (length Gamma)).
set (dv  := Dup DA var).

assert (var === var #in# PerNe) as HvarNe.
unfold var.
apply PerNe_intro.
intros.
exists (tmVar (m - S (length Gamma))); split; auto.

assert (dv === dv #in# PDA) as HdvPDA.
apply Reflect_Characterization; eauto.


assert (CxtShift Gamma 0 Gamma) as HCxtShift0 by eauto.
assert (CxtShift (Gamma,,A) 1 Gamma) as HCxtShift1 by (eapply SSEQ; eauto).

specialize (HI3 TmVar var HvarNe).

edestruct e as [DB Happ]; [ eauto | ].
edestruct e' as [DB' Happ']; [ eauto | ].


assert (Gamma,,A ||- TmSb A (Sups 1) #in# HDA) as HDAliftA by (eapply RelTypeLifting; eauto).

assert (Gamma,,A ||- TmVar : TmSb A (Sups 1) #aeq# Dup DA var #in# HDA) as Ha.
set (IHHDT3 := IHHDT2).
specialize IHHDT3.
specialize (IHHDT2 (Gamma,,A) (TmSb A (Sups 1)) HDAliftA). 
destruct IHHDT2 as [HI1' [HI2' HI3']].
specialize (HI3' TmVar var HvarNe).
apply HI3'.
intros.
exists (tmVar (length Delta - (S (length Gamma)))).
unfold var.
split; auto.
rewrite CxtShift_length with (i := i) (Gamma := Gamma,,A).
simpl.
rewrite minus_plus.
eapply TmVar_Sups_tmVar'; eauto.
eauto.

move HS3 at bottom.

assert (CxtShift (Gamma,,NFA) 1 Gamma) as HCxtShift1'.
red.
simpl.
eapply SSEQ.
eauto.
eauto.

specialize (HS3 (Gamma,,A) 1 TmVar dv DB DB' PDA HPDA). 
specialize (HS3 HdvPDA Happ Happ' HCxtShift1 Ha).

move H at bottom.
specialize (H dv dv DB DB' PDA HPDA HdvPDA Happ Happ' _ _ HS3).
 
destruct H as [HF1 [HF2 HF3]].
destruct HF1 as [NFF [HF1' HF1'']].
set (NT := TmFun NFA NFF). 
exists NT.
split.
apply reifyNf_fun with DB.
auto.
auto.
auto.

assert (Gamma |-- TmFun A F) as HZZ1.
eauto.

assert (Gamma |-- A /\ Gamma,,A |-- F) as HZZ1'.
eapply Inversion_FUN_F; eauto.

clear_inversion HZZ1'.
auto.
 

assert (Gamma,,A |-- TmSb F (Sext (Sups 1) TmVar) === F) as HZZ2.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
simpl.
eapply EQ_SB_TRANS.
eapply EQ_SB_SIDSUP.
eauto.
eapply EQ_SB_CONG_SEXT.
eauto.
eauto.
eauto.
eapply EQ_TP_REFL.
auto.
apply EQ_TP_SBID.
auto.

(**)
 
move NFA at bottom.
move NFF at bottom.
  
eapply EQ_TP_TRANS.
eauto.
eapply EQ_TP_CONG_FUN.
auto.

(**)

eapply EQ_TP_TRANS.
eapply EQ_TP_SYM.
eauto.
auto.

(**)

Qed.
Hint Immediate RelReify1_Dfun_a.

(******************************************************************************
 *)

Theorem RelReify1_Dfun_b: forall
  (DA : D)
  (DF : D)
  (DA' : D)
  (DF' : D)
  (PA : relation D)
  (i : DA === DA' #in# PerType)
  (IHHDT : forall (Gamma : Cxt) (T : Tm),
          Gamma ||- T #in# i ->
          (exists A : Tm,
             RbNf (length Gamma) (DdownN DA) A /\ Gamma |-- T === A) /\
          (forall (t : Tm) (dt : D),
           Gamma ||- t : T #aeq# dt #in# i ->
           exists v : Tm,
             RbNf (length Gamma) (Ddown DA dt) v /\ Gamma |-- t === v : T) /\
          (forall (t : Tm) (k : DNe),
           k === k #in# PerNe ->
           (forall (Delta : Cxt) (i : nat),
            CxtShift Delta i Gamma ->
            exists tv : Tm,
              RbNe (length Delta) k tv /\
              Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
           Gamma ||- t : T #aeq# Dup DA k #in# i))
  (i0 : InterpType DA PA)
  (e : forall a : D, a === a #in# PA -> exists DB : D, App DF a DB)
  (e0 : forall a : D, a === a #in# PA -> exists DB' : D, App DF' a DB')
  (i1 : forall (a0 a1 DB0 DB1 : D) (P : relation D),
       InterpType DA P ->
       a0 === a1 #in# P ->
       App DF a0 DB0 -> App DF' a1 DB1 -> DB0 === DB1 #in# PerType)
  (H : forall (a0 a1 DB0 DB1 : D) (P : relation D) (i : InterpType DA P)
        (i0 : a0 === a1 #in# P) (a : App DF a0 DB0) 
        (a2 : App DF' a1 DB1) (Gamma : Cxt) (T : Tm),
      Gamma ||- T #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
      (exists A : Tm, RbNf (length Gamma) (DdownN DB0) A /\ Gamma |-- T === A) /\
      (forall (t : Tm) (dt : D),
       Gamma ||- t : T #aeq# dt #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
       exists v : Tm,
         RbNf (length Gamma) (Ddown DB0 dt) v /\ Gamma |-- t === v : T) /\
      (forall (t : Tm) (k : DNe),
       k === k #in# PerNe ->
       (forall (Delta : Cxt) (i1 : nat),
        CxtShift Delta i1 Gamma ->
        exists tv : Tm,
          RbNe (length Delta) k tv /\
          Delta |-- TmSb t (Sups i1) === tv : TmSb T (Sups i1)) ->
       Gamma ||- t : T #aeq# Dup DB0 k #in# i1 a0 a1 DB0 DB1 P i i0 a a2))
  (Gamma : Cxt)
  (T : Tm)
  (H0 : Gamma ||- T #in# PerType_fun i i0 e e0 i1)
  (t : Tm)
  (dt : D)
  (H1 : Gamma ||- t : T #aeq# dt #in# PerType_fun i i0 e e0 i1),

   exists v : Tm,
     RbNf (length Gamma) (Ddown (Dfun DA DF) dt) v /\ Gamma |-- t === v : T
.
Proof.
intros.
 
renamer; intros.
rename i into HDA.
rename i1 into HREC.
rename i0 into HPDA.
rename e0 into e'.

simpl in H1.

destruct H1 as [HS1 [A [F [PT [HS2 [HS3 [HS4 [HS5 HS6]]]]]]]].

move IHHDT at bottom.
set (IHHDT2 := IHHDT).
move H at bottom.
set (var := Dvar (length Gamma)).
set (dv  := Dup DA var).

assert (var === var #in# PerNe) as HvarNe.
unfold var.
apply PerNe_intro.
intros.
exists (tmVar (m - S (length Gamma))); split; auto.

assert (dv === dv #in# PDA) as HdvPDA.
apply Reflect_Characterization; eauto.

assert (Gamma |-- A /\ Gamma ,,A |-- F) as HfunAF.
eapply Inversion_FUN_F; eauto.
clear_inversion HfunAF.



assert (CxtShift Gamma 0 Gamma) as HCxtShift0 by eauto.
assert (CxtShift (Gamma,,A) 1 Gamma) as HCxtShift1 by (eapply SSEQ; eauto).

edestruct e as [DB Happ]; [ eauto | ].
edestruct e' as [DB' Happ']; [ eauto | ].

assert (Gamma,,A ||- TmSb A (Sups 1) #in# HDA) as HDAliftA by (eapply RelTypeLifting; eauto).

assert (Gamma,,A ||- TmVar : TmSb A (Sups 1) #aeq# Dup DA var #in# HDA) as Ha.
specialize (IHHDT (Gamma,,A) (TmSb A (Sups 1)) HDAliftA).
destruct IHHDT as [HI1' [HI2' HI3']].
specialize (HI3' TmVar var HvarNe).
apply HI3'.
intros.
exists (tmVar (length Delta - (S (length Gamma)))).
unfold var.
split; auto.
rewrite CxtShift_length with (i := i) (Gamma := Gamma,,A).
simpl.
rewrite minus_plus.
eapply TmVar_Sups_tmVar'; eauto.
eauto.

move HS6 at bottom.
specialize (HS6 (Gamma,,A) 1 TmVar dv DB DB' PDA HPDA). 
specialize (HS6 HdvPDA Happ Happ' HCxtShift1 Ha).
destruct HS6 as [dy [HS6' HS6'']].

assert ( Gamma,, A ||-  TmSb F (Sext (Sups 1) TmVar)  #in# HREC dv dv DB DB' PDA HPDA HdvPDA Happ Happ'  )  as Htype .
eapply RelTerm_implies_RelType.
eauto.

move H at bottom.
specialize (H dv dv DB DB' PDA HPDA HdvPDA Happ Happ' _ _ Htype).
destruct H as [HF1 [HF2 HF3]].
edestruct HF2 as [nft Hnft].
eauto.
destruct Hnft as [Hnft1 Hft2].

destruct HF1 as [HNF [HNF1 HNF2]].

specialize (IHHDT Gamma A).
destruct IHHDT as [HI1 [HI2 HI3]].
auto.
destruct HI1 as [NFA HNFA].
destruct HNFA as [HNFA1 HNFA2].

set (NT := TmAbs NFA nft).
 
assert (Gamma |-- t : TmFun A F) as HtAF.
eapply CONV.
eauto.
auto.
 
assert (Gamma,, A |-- TmSb (TmFun A F) Sup === TmFun (TmSb A Sup) (TmSb F (Sext (Sseq Sup Sup) TmVar))) as HX1.
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.

assert (  Gamma,, A |-- TmSb A Sup ) as HX2.
eauto.

assert (  Gamma,, A |-- TmVar : TmSb A Sup ) as HX3.
eauto.

assert (Gamma,, A |-- [Ssing TmVar] : Gamma,, A,,TmSb A Sup) as HX4.
eapply SEXT.
eauto.
eauto.
eapply CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
eauto.

assert (Gamma,, A,, TmSb A Sup |--  [Sseq Sup Sup]: Gamma)  as HX5 by eauto.

assert ( Gamma,, A,, TmSb A Sup |--  [Sext (Sseq Sup Sup) TmVar] : Gamma ,, A ) as HX6.
eapply SEXT.
eauto.
eauto.
eapply CONV.
eapply HYP; eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.


assert ( Gamma,, A |-- TmVar : TmSb A (Sseq Sup Sid)) as HX7.
eapply CONV.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
eapply EQ_SB_SIDR.
eauto.
eauto.

assert ( Gamma,, A |-- TmVar : TmSb A (Sseq Sid Sup)) as HX7'.
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
eapply EQ_SB_SIDL.
eauto.
eauto.

assert( Gamma,, A |--  [Sext (Sseq Sup Sid) TmVar]=== [Sid]:Gamma,, A) as HX8.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SEXT.
eapply EQ_SB_SIDR.
eauto.
eauto.
eapply EQ_REFL.
eauto.
eapply EQ_SB_SYM.
eapply EQ_SB_SIDSUP.
eauto.

assert( Gamma,, A |--  [Sext (Sseq Sid Sup) TmVar]=== [Sid]:Gamma,, A) as HX8'.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SEXT.
eapply EQ_SB_SIDL.
eauto.
eauto.
eapply EQ_REFL.
eauto.
eapply EQ_SB_SYM.
eapply EQ_SB_SIDSUP.
eauto.

assert( Gamma,, A |--  [Sseq Sup Sid]=== [Sseq (Sseq Sup Sup) (Ssing TmVar)] : Gamma ) as HX9.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.
eauto.
eauto.
 
assert (Gamma,, A |-- TmSb (TmSb F (Sext (Sseq Sup Sup) TmVar)) (Ssing TmVar) === F) as HX10.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
auto.
eapply EQ_TP_SYM.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SEXT.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.
eapply EQ_SB_REFL; eauto.
eauto.
eauto.
eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
eauto.
eauto.
eauto.
eauto.


assert (Gamma,,A |-- TmSb F (Sext (Sups 1) TmVar) === F ) as HX11.
simpl.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eauto.
eauto.
eauto.

(**!*)

exists NT.
split; auto.
unfold Ddown; simpl.
eapply reifyNf_abs with (db := dy) (DB := DB); auto.
 
eapply EQ_CONV with (A := TmFun A F); [ | eauto ].
eapply EQ_TRANS.
eapply EQ_FUN_Eta.
auto.
eapply EQ_CONG_FUN_I; auto.
eapply EQ_TRANS.
eapply EQ_CONV.
eapply EQ_CONG_FUN_E.
eapply EQ_REFL.
eapply CONV.
eapply SB.
auto.
apply HtAF.
eauto.
eapply EQ_REFL;eauto.
eauto.

(**)

assert ( Gamma,, A |-- TmApp (TmSb t (Sups 1)) TmVar === nft : F) as HX12.
eapply EQ_CONV.
eauto.
eauto.
 
eapply EQ_TRANS; [ | apply HX12 ].
eapply EQ_CONV.
eapply EQ_CONG_FUN_E.
eapply EQ_CONV.
eapply EQ_CONG_SB.
simpl.
eauto.
eauto.
eauto.
eapply EQ_REFL.
eauto.
eauto.

(**)

Qed.
Hint Immediate RelReify1_Dfun_b.

(******************************************************************************
 *)

Theorem RelReify1_Dfun_c: forall
  (DA : D)
  (DF : D)
  (DA' : D)
  (DF' : D)
  (PA : relation D)
  (i : DA === DA' #in# PerType)
  (IHHDT : forall (Gamma : Cxt) (T : Tm),
          Gamma ||- T #in# i ->
          (exists A : Tm,
             RbNf (length Gamma) (DdownN DA) A /\ Gamma |-- T === A) /\
          (forall (t : Tm) (dt : D),
           Gamma ||- t : T #aeq# dt #in# i ->
           exists v : Tm,
             RbNf (length Gamma) (Ddown DA dt) v /\ Gamma |-- t === v : T) /\
          (forall (t : Tm) (k : DNe),
           k === k #in# PerNe ->
           (forall (Delta : Cxt) (i : nat),
            CxtShift Delta i Gamma ->
            exists tv : Tm,
              RbNe (length Delta) k tv /\
              Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)) ->
           Gamma ||- t : T #aeq# Dup DA k #in# i))
  (i0 : InterpType DA PA)
  (e : forall a : D, a === a #in# PA -> exists DB : D, App DF a DB)
  (e0 : forall a : D, a === a #in# PA -> exists DB' : D, App DF' a DB')
  (i1 : forall (a0 a1 DB0 DB1 : D) (P : relation D),
       InterpType DA P ->
       a0 === a1 #in# P ->
       App DF a0 DB0 -> App DF' a1 DB1 -> DB0 === DB1 #in# PerType)
  (H : forall (a0 a1 DB0 DB1 : D) (P : relation D) (i : InterpType DA P)
        (i0 : a0 === a1 #in# P) (a : App DF a0 DB0) 
        (a2 : App DF' a1 DB1) (Gamma : Cxt) (T : Tm),
      Gamma ||- T #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
      (exists A : Tm, RbNf (length Gamma) (DdownN DB0) A /\ Gamma |-- T === A) /\
      (forall (t : Tm) (dt : D),
       Gamma ||- t : T #aeq# dt #in# i1 a0 a1 DB0 DB1 P i i0 a a2 ->
       exists v : Tm,
         RbNf (length Gamma) (Ddown DB0 dt) v /\ Gamma |-- t === v : T) /\
      (forall (t : Tm) (k : DNe),
       k === k #in# PerNe ->
       (forall (Delta : Cxt) (i1 : nat),
        CxtShift Delta i1 Gamma ->
        exists tv : Tm,
          RbNe (length Delta) k tv /\
          Delta |-- TmSb t (Sups i1) === tv : TmSb T (Sups i1)) ->
       Gamma ||- t : T #aeq# Dup DB0 k #in# i1 a0 a1 DB0 DB1 P i i0 a a2))
  (Gamma : Cxt)
  (T : Tm)
  (H0 : Gamma ||- T #in# PerType_fun i i0 e e0 i1)
  (t : Tm)
  (k : DNe)
  (H1 : k === k #in# PerNe)
  (H2 : forall (Delta : Cxt) (i : nat),
       CxtShift Delta i Gamma ->
       exists tv : Tm,
         RbNe (length Delta) k tv /\
         Delta |-- TmSb t (Sups i) === tv : TmSb T (Sups i)),

   Gamma ||- t : T #aeq# Dup (Dfun DA DF) k #in# PerType_fun i i0 e e0 i1
.
Proof.
intros. 
renamer; intros.
rename i into HDA.
rename i1 into HREC.
rename i0 into HPDA.
rename e0 into e'.
 
simpl in H0.
destruct H0 as [A [F [HS1 [HS2 HS3]]]].

assert (
 exists PT : relation D,
     InterpType (Dfun DA DF) PT
) as HPT.

assert (Dfun DA DF === Dfun DA' DF' #in# PerType).
apply PerType_fun with PDA; auto.
eauto.

destruct HPT as [PT HPT].

assert (Gamma |-- t : T) as HtT.
destruct (H2 Gamma 0) as [tt [Htt1 Htt2]].
red; eauto.
assert (Gamma |-- TmSb t (Sups 0) : TmSb T (Sups 0)) by eauto.
apply SBID_inv.
apply CONV with (A := TmSb T Sid); eauto.

split; auto.
(**)
exists A, F, PT; repeat (split; auto).
(**)
clear_inversion HPT.
apply RelProd_intro; intros.

edestruct e with (a := a0) as [DB0 HDB0] .
find_extdeters.
close_doms.
apply H3; eauto.

edestruct e with (a := a1) as [DB1 HDB1] .
find_extdeters.
close_doms.
apply H3; eauto.
 

clear_inversion H5.
clear_inversion po_ro.
destruct ro_resp_ex with (a := a0) as [Y HY].
eauto.

set (y0 := (Dup DB0 (Dapp k (Ddown DA a0)))).
set (y1 := (Dup DB1 (Dapp k (Ddown DA a1)))).
 
exists y0, y1, Y.
unfold Dup; simpl.
repeat (split; auto).
apply appFun; auto.
apply appFun; auto.
 
assert (DB0 === DB1 #in# PerType) as HDBPerType.
assert (Dfun DA DF === Dfun DA' DF' #in# PerType).
apply PerType_fun with PDA; auto.
find_extdeters.
close_doms.

edestruct e' with (a := a0) as [DB0' HDB0'] .
apply H5; eauto.

eapply PER_Transitive with DB0'.

eapply HREC with (a0 := a0) (a1 := a0) (P := PDA); auto.
apply H5; eauto.

symmetry.
eapply HREC with (a0 := a1) (a1 := a0) (P := PDA); auto.
apply H5.
symmetry.
apply H0.


apply Reflect_Characterization.
auto.
eauto.
apply PerNe_intro; intros.

assert (Ddown DA a0 === Ddown DA a1 #in# PerNf).
eapply Reify_Characterization.
eauto.
eauto.
eauto.
clear_inversion H3.
destruct H5 with m as [nd [Hnd1 Hnd2]].

clear_inversion H1.
destruct H3 with m as [nk [Hnk1 Hnk2]].
 
set (n := TmApp nk nd).
exists n.
repeat (split; auto).
apply reifyNe_app; auto.
apply reifyNe_app; auto.

(*....*)

intros.
simpl in H3.
simpl.

clear_inversion HPT.
set (dy := Dup DB (Dapp k (Ddown DA da))).
exists dy.
split; auto.
unfold Dup; simpl.
apply appFun.
auto.
move HS3 at bottom.
specialize (HS3 Delta i a da DB DB' PA HPA Hda Happ Happ' H0 H3).

move H at bottom.
specialize (H da da DB DB').
specialize (H PA HPA Hda Happ Happ' Delta _ HS3).
destruct H as [HF1 [HF2 HF3]].

(* try HF3 *)

assert (Ddown DA da === Ddown DA da #in# PerNf).
eapply Reify_Characterization.
eauto.
eauto.
eauto.

apply HF3.

 
apply PerNe_intro.
intros.


clear_inversion H.
destruct H4 with m as [nd [Hnd1 Hnd2]].

clear_inversion H1.
destruct H with m as [ne [Hne1 Hne2]].

set (n := TmApp ne nd).
exists n.
repeat (split; auto).
apply reifyNe_app; auto.
apply reifyNe_app; auto. 
(**)
 
intros.
move H2 at bottom.
rename i1 into j. 
destruct H2 with (Delta := Delta0) (i := j + i) as [ne [Hne1 Hne2]].
eapply CxtShift_comp; eauto.
 
clear_inversion H.
destruct H5 with (m := length Delta0) as [nd [Hnd1 Hnd2]].
set (tv := TmApp ne nd).

move a at bottom.

exists tv.
repeat (split; auto).
apply reifyNe_app; auto.

rename Delta0 into Psi.

assert (Psi ||- TmSb a (Sups j) : TmSb (TmSb A (Sups i)) (Sups j) #aeq# da #in# HDA) as HA1.
eapply RelTermLifting.
eauto.
eauto.

assert (Delta ||- TmSb A (Sups i) #in# HDA) as HA2.
eapply RelTypeLifting; eauto.

assert (Psi ||- TmSb (TmSb A (Sups i)) (Sups j) #in# HDA) as HA3.
eapply RelTypeLifting; eauto.


assert (Psi |-- TmSb a (Sups j) === nd : TmSb (TmSb A (Sups i)) (Sups j)) as HA4.
edestruct (IHHDT _ _ HA3) as [IHHDT1 [IHHDT2 IHHDT3]].
edestruct (IHHDT2 _ _ HA1) as [ntv [Htv1 Htv2]].
assert (nd = ntv).
eapply RbNf_deter; eauto.
subst.
eauto.

assert (Gamma |-- A) as HA5.
{
eapply RelType_implies_WfTp.
eauto.
}

assert (Gamma |-- TmFun A F) as HA6.
{
eauto.
}

assert (Gamma,,A |-- F) as HA7.
{
edestruct Inversion_FUN_F; eauto.
}

assert (Delta |-- a : TmSb A (Sups i)) as HA8.
{
eapply RelTerm_implies_WtTm.
eauto.
}

assert (Delta |-- TmSb A (Sups i)) as HA9.
{
eauto.
}

assert (Delta |--  [Sext (Sups i) a]: Gamma,,A) as HA10.
{
eauto.
}

assert ( Psi |--  [Sext (Sups j) (TmSb a (Sups j))]: Delta,, TmSb A (Sups i)) as HA11.
{
eauto.
}

assert (Delta,, TmSb A (Sups i) |-- TmVar : TmSb A (Sseq (Sups i) Sup)) as HA12.
{
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
}

assert (Delta,, TmSb A (Sups i) |--  [Sext (Sseq (Sups i) Sup) TmVar]:Gamma,, A) as HA13.
{
eapply SEXT; eauto.
}

assert (Psi |--  [Sseq (Sups i) (Sups j)] === [Sseq (Sseq (Sups i) Sup) (Sext (Sups j) (TmSb a (Sups j)))]:Gamma) as HA14.
{
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eapply EQ_SB_CONG_SSEQ.
eauto.
eauto.
}


eapply EQ_TRANS.
eapply EQ_CONV.
eapply EQ_SBAPP.
eauto.
eapply CONV.
eapply SB.
eauto.
eapply CONV.
eauto.
eauto.
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.
eauto.


eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.


eapply EQ_TP_CONG_SB; [ | eauto ].

eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eauto.

eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eauto.


eapply EQ_SB_CONG_SEXT.
eauto.
eauto.
eapply EQ_SYM.
eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eauto.
eauto.

(**)


assert (Psi |-- TmSb t (Sups (j + i)) : TmSb T (Sups (j + i))) as HA15.
eauto.

assert (Psi |--  [Sups (j + i)]:Gamma) as HA16.
eapply CxtShift_comp; eauto.

assert (Psi |-- TmSb T (Sups (j + i)) === TmFun (TmSb A (Sups (j + i))) (TmSb F (Sext (Sseq (Sups (j + i)) Sup) TmVar)) ) as HA17.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eauto.
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.

assert (Psi |-- TmSb A (Sups (j + i))) as HA18.
eapply SB_F.
eauto.
eauto.


assert (Psi |-- TmSb a (Sups j) === nd : TmSb A (Sups (j + i))) as HA19.
eapply EQ_CONV.
eauto.
eapply EQ_TP_SYM.
rewrite plus_comm.
eapply nc_Sups_tp_plus_eq.
rewrite plus_comm.
eauto.
eauto.
rewrite plus_comm.
eauto.

assert ( Psi,, TmSb A (Sups (j + i)) |-- TmVar : TmSb  A (Sseq (Sups (j + i)) Sup)) as HA20.
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

assert (Psi,, TmSb A (Sups (j + i)) |--  [Sext (Sseq (Sups (j + i)) Sup) TmVar] : Gamma,, A  ) as HA21.
eapply SEXT.
eauto.
eauto.
eauto.

assert (Psi |-- TmSb (TmSb A (Sups (j + i))) Sid === TmSb A (Sseq (Sups i) (Sups j))) as HA22.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBID.
eauto.
eapply EQ_TP_TRANS.
rewrite plus_comm.
eapply nc_Sups_tp_plus_eq.
rewrite plus_comm; eauto.
eauto.
rewrite plus_comm; eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.


subst tv.
eapply EQ_CONV.
eapply EQ_CONG_FUN_E.
eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SYM.
eapply nc_Sups_plus_eq.
rewrite plus_comm; eauto.
rewrite plus_comm; eauto.
rewrite plus_comm; eauto.
rewrite plus_comm; eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
unfold Ssing.
eapply SEXT.
eauto.
eauto.
eapply CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
eauto.
eauto.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_SB; [ | eauto ].
unfold Ssing.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.

eapply SEXT.
eauto.
eauto.
eapply CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBID.
eauto.
eauto.
eauto.


eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eauto.

eapply EQ_SB_CONG_SEXT.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eapply SEXT.
eauto.
eauto.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.
eapply EQ_SB_REFL.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_SIDR.
eauto.
eapply Sups_tails.
eauto.
eauto.

eapply EQ_SYM.
eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eauto.

eauto.

Qed.
Hint Immediate RelReify1_Dfun_c.

