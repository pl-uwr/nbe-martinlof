(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.

Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Common.

(******************************************************************************
 *)
Lemma PerType_neutral_closed_under_per_tp: forall e e' T Gamma DT'_ DT''
  (i : e === e' #in# PerNe)
  (HDT' : DT'_ === DT'' #in# PerType),
  DT'_ = Dup Duniv e' ->
   (Gamma ||- T #in# PerType_ne i <-> Gamma ||- T #in# HDT')
.
Proof.
intros.
destruct HDT'; try solve [clear_inversion H ].
clear_inversion H.

split; intro.
+{
simpl in *.
decompose record H; repeat (split; auto).

intros.
edestruct H1 as [v]; eauto.
decompose record H3.

assert (RbNe (length Delta) e v) as HX1.
{
clear_inversion H4.
auto.
}

assert (RbNe (length Delta) e' v) as HX2.
{
clear_inversion i.
destruct H6 with (length Delta).
destruct H7.
assert (v = x) by (eapply RbNe_deter; eauto).
subst.
auto.
}

exists v.
split.
eapply reifyNf_ne.
auto.

auto.
}

+{ 
simpl in *.
decompose record H; repeat (split; auto).
intros.

edestruct H1 as [v]; eauto.
decompose record H3.

assert (RbNe (length Delta) e' v) as HX1.
{
clear_inversion H4.
auto.
}

assert (RbNe (length Delta) e v) as HX2.
{
clear_inversion i.
destruct H6 with (length Delta).
destruct H7.
assert (v = x) by (eapply RbNe_deter; eauto).
subst.
auto.
}

exists v.
split.
eapply reifyNf_ne.
auto.

auto.
}
Qed.
Hint Immediate PerType_neutral_closed_under_per_tp.


(******************************************************************************
 *)
Lemma PerType_neutral_closed_under_per_tm: forall e e' T Gamma DT'_ DT'' t dt dt' PT
  (i : e === e' #in# PerNe)
  (HDT' : DT'_ === DT'' #in# PerType),
  DT'_ = Dup Duniv e' ->
  dt === dt' #in# PT ->
  InterpType (Dup Duniv e) PT ->
  (Gamma ||- t : T #aeq# dt #in# PerType_ne i <->  Gamma ||- t : T #aeq# dt' #in# HDT')
.
Proof.
intros.
destruct HDT'; try solve [clear_inversion H ].
clear_inversion H.
clear_inversion H1.

split; intros.
+{
simpl in *; intros. 
decompose record H; repeat split; auto.
-{
eapply Per_domR; eauto.
}

-{
intros.
edestruct H3 as [v]; eauto.
decompose record H7.
exists v; split; auto.

eapply reifyNf_ne.
clear_inversion H8.
clear_inversion i.
edestruct H8 with (length Delta).
destruct H10.
erewrite RbNe_deter; eauto.
}

-{
intros.
edestruct H6 as [v]; eauto.
decompose record H7.
exists v; split; auto.

rewrite Ddown_Dup.
clear_inversion H0.
eapply reifyNf_ne.
clear_inversion H8.
clear_inversion H10.
edestruct H0 with (length Delta).
destruct H8.
erewrite RbNe_deter; eauto.
}

}

+{
simpl in *; intros. 
decompose record H; repeat split; auto.
-{
eapply Per_domR; eauto.
}

-{
intros.
edestruct H3 as [v]; eauto.
decompose record H7.
exists v; split; auto.

eapply reifyNf_ne.
clear_inversion H8.
clear_inversion i.
edestruct H8 with (length Delta).
destruct H10.
erewrite RbNe_deter; eauto.
}

-{
intros.
edestruct H6 as [v]; eauto.
decompose record H7.
exists v; split; auto.

rewrite Ddown_Dup.
clear_inversion H0.
eapply reifyNf_ne.
clear_inversion H8.
clear_inversion H10.
edestruct H0 with (length Delta).
destruct H8.
erewrite RbNe_deter; eauto.
}

}
Qed.
Hint Immediate PerType_neutral_closed_under_per_tm.
