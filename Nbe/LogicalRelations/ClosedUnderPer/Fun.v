(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.

Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.

(******************************************************************************
 *)
Lemma PerType_fun_is_closed_under_per_tp: forall DA DF DA' DF' PA T Gamma DT'_ DT''
  (i : DA === DA' #in# PerType)
  (i0 : InterpType DA PA)
  (e : forall a : D, a === a #in# PA -> exists DB : D, App DF a DB)
  (e0 : forall a : D, a === a #in# PA -> exists DB' : D, App DF' a DB')
  (i1 : forall (a0 a1 DB0 DB1 : D) (P : relation D),
       InterpType DA P ->
       a0 === a1 #in# P ->
       App DF a0 DB0 -> App DF' a1 DB1 -> DB0 === DB1 #in# PerType)
  (H : forall (a0 a1 DB0 DB1 : D) (P : relation D) (i : InterpType DA P)
        (i0 : a0 === a1 #in# P) (a : App DF a0 DB0) 
        (a2 : App DF' a1 DB1) (T : Tm) (Gamma : Cxt) 
        (DT'_ : D),
      DT'_ = DB1 ->
      forall (DT'' : D) (HDT' : DT'_ === DT'' #in# PerType),
      (Gamma ||- T #in# i1 a0 a1 DB0 DB1 P i i0 a a2 <->
       Gamma ||- T #in# HDT') /\
      (forall (t : Tm) (dt dt' : D) (PT : relation D),
       dt === dt' #in# PT ->
       InterpType DB0 PT ->
       (Gamma ||- t : T #aeq# dt #in# i1 a0 a1 DB0 DB1 P i i0 a a2 <->
        Gamma ||- t : T #aeq# dt' #in# HDT')))
  (IHHDT : forall (T : Tm) (Gamma : Cxt) (DT'_ : D),
          DT'_ = DA' ->
          forall (DT'' : D) (HDT' : DT'_ === DT'' #in# PerType),
          (Gamma ||- T #in# i <-> Gamma ||- T #in# HDT') /\
          (forall (t : Tm) (dt dt' : D) (PT : relation D),
           dt === dt' #in# PT ->
           InterpType DA PT ->
           (Gamma ||- t : T #aeq# dt #in# i <->
            Gamma ||- t : T #aeq# dt' #in# HDT')))
  (H0 : DT'_ = Dfun DA' DF')
  (HDT' : DT'_ === DT'' #in# PerType),
   Gamma ||- T #in# PerType_fun i i0 e e0 i1 <-> Gamma ||- T #in# HDT'
.
Proof.
intros.
renamer.
rename i into HDA.
rename i0 into HPDA.
rename i1 into H_PDB.
destruct HDT'; try solve [clear_inversion H0].
clear_inversion H0.
renamer.
rename i0 into HPDA'.
rename i1 into H_PDB'.
rename i into HDA'.
rename e1 into e'.
rename e2 into e0'.

(* ->  *)

split; intro HS.
simpl in HS.
destruct HS as [A [F [HS1 [HS2 HS3 ]]]].
simpl. exists A, F.
repeat (split; auto).

specialize (IHHDT A Gamma _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
(*specialize (IHHDT_Type _ HDA').*)
apply IHHDT_Type; auto.

(**)

intros.
renamer.
assert (InterpType DA' PDA) by (eapply InterpType_resp_PerType; eauto).
find_extdeters.
close_doms.

destruct e with da as [DB_].
eauto.

destruct e' with da as [DB_'].
eauto.


specialize (HS3 Delta i a da DB_ DB_' PDA).
specialize (HS3 HPDA).
assert (da === da #in# PDA) as HdadaPDA.
apply H3.
auto.
specialize (HS3 HdadaPDA H9 H10 H0).

assert ( Delta ||- a : TmSb A (Sups i) #aeq# da #in# HDA ) as HS1_P.
specialize (IHHDT (TmSb A (Sups i)) Delta _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
move IHHDT_Term at bottom.
specialize (IHHDT_Term a da da PDA HdadaPDA HPDA).
apply IHHDT_Term.
eauto.

specialize (HS3 HS1_P).

move H at bottom.

assert (DB_' = DB) by eauto 2; subst.


specialize (H _ _ _ _ PDA HPDA HdadaPDA H9 H10).
specialize (H ( TmSb F (Sext (Sups i) a))).
specialize (H Delta _ eq_refl _ (H_PDB' da da DB DB' PDA'0 HPA Hda Happ Happ' ) ).
destruct H.
eapply H; auto.

(* <- *)

simpl in HS.
destruct HS as [A [F [HS1 [HS2 HS3 ]]]].
simpl. exists A, F.
repeat (split; auto).

specialize (IHHDT A Gamma _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
(*specialize (IHHDT_Type _ HDA').*)
apply IHHDT_Type; eauto.

(**)

intros.
renamer.
assert (InterpType DA PDA').
eapply InterpType_resp_PerType.
eauto 1.
symmetry; auto.

find_extdeters.
close_doms.
 
destruct e' with da as [DB_].
eauto.

destruct e0' with da as [DB_'].
eauto.

specialize (HS3 Delta i a da DB_ DB_' PDA').
specialize (HS3 HPDA').
assert (da === da #in# PDA') as HdadaPDA'.
eauto.

assert (DB' = DB_) by eauto 2; subst.

specialize (HS3 HdadaPDA' H9 H10 H0).  

assert ( Delta ||- a : TmSb A (Sups i) #aeq# da #in# HDA' ) as HS1_P.
specialize (IHHDT (TmSb A (Sups i)) Delta _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
move IHHDT_Term at bottom.
specialize (IHHDT_Term a da da _ HdadaPDA' H2).
apply IHHDT_Term.
eauto.


specialize (HS3 HS1_P).

move H at bottom.

specialize (H _ _ _ _ PDA0 HPA Hda Happ Happ').
specialize (H ( TmSb F (Sext (Sups i) a))).
specialize (H Delta _ eq_refl).
specialize (H _ ( H_PDB' da da DB_ DB_' PDA' HPDA' HdadaPDA' H9 H10 )).
destruct H.
eapply H; auto.
Qed.
Hint Immediate PerType_fun_is_closed_under_per_tp.

(******************************************************************************
 *)
Lemma PerType_fun_is_closed_under_per_tm: forall DA DF DA' DF' PA T Gamma DT'_ DT'' dt dt' t PT
  (i : DA === DA' #in# PerType)
  (i0 : InterpType DA PA)
  (e : forall a : D, a === a #in# PA -> exists DB : D, App DF a DB)
  (e0 : forall a : D, a === a #in# PA -> exists DB' : D, App DF' a DB')
  (i1 : forall (a0 a1 DB0 DB1 : D) (P : relation D),
       InterpType DA P ->
       a0 === a1 #in# P ->
       App DF a0 DB0 -> App DF' a1 DB1 -> DB0 === DB1 #in# PerType)
  (H : forall (a0 a1 DB0 DB1 : D) (P : relation D) (i : InterpType DA P)
        (i0 : a0 === a1 #in# P) (a : App DF a0 DB0) 
        (a2 : App DF' a1 DB1) (T : Tm) (Gamma : Cxt) 
        (DT'_ : D),
      DT'_ = DB1 ->
      forall (DT'' : D) (HDT' : DT'_ === DT'' #in# PerType),
      (Gamma ||- T #in# i1 a0 a1 DB0 DB1 P i i0 a a2 <->
       Gamma ||- T #in# HDT') /\
      (forall (t : Tm) (dt dt' : D) (PT : relation D),
       dt === dt' #in# PT ->
       InterpType DB0 PT ->
       (Gamma ||- t : T #aeq# dt #in# i1 a0 a1 DB0 DB1 P i i0 a a2 <->
        Gamma ||- t : T #aeq# dt' #in# HDT')))
  (IHHDT : forall (T : Tm) (Gamma : Cxt) (DT'_ : D),
          DT'_ = DA' ->
          forall (DT'' : D) (HDT' : DT'_ === DT'' #in# PerType),
          (Gamma ||- T #in# i <-> Gamma ||- T #in# HDT') /\
          (forall (t : Tm) (dt dt' : D) (PT : relation D),
           dt === dt' #in# PT ->
           InterpType DA PT ->
           (Gamma ||- t : T #aeq# dt #in# i <->
            Gamma ||- t : T #aeq# dt' #in# HDT')))
  (H0 : DT'_ = Dfun DA' DF')
  (HDT' : DT'_ === DT'' #in# PerType)
  (H1 : dt === dt' #in# PT)
  (H2 : InterpType (Dfun DA DF) PT),
   Gamma ||- t : T #aeq# dt #in# PerType_fun i i0 e e0 i1 <->
   Gamma ||- t : T #aeq# dt' #in# HDT'
.
Proof.
intros.
(* RelTerm Dfun *)

renamer.
rename i into HDA.
rename i0 into HPDA.
rename i1 into H_PDB.
destruct HDT'; try solve [clear_inversion H0].
clear_inversion H0.
renamer.
rename i0 into HPDA'.
rename i1 into H_PDB'.
rename i into HDA'.
rename e1 into e'.
rename e2 into e0'.

(* ->  *)

split; intro HS.
simpl in HS.
destruct HS as [? [A [F [Pfun [HS1 [HS2 [HS3 [HS4 HS5]]]]]]]].
simpl. split; auto. exists A, F, Pfun.
repeat (split; auto).

apply InterpType_resp_PerType with (DA := Dfun DA DF); auto.
eapply PerType_fun; eauto 4.
find_extdeters.
close_extdeters.
eauto.

(**)

specialize (IHHDT A Gamma _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
(*specialize (IHHDT_Type _ HDA').*)
apply IHHDT_Type; auto.

(**)

intros.
renamer.
assert (InterpType DA' PDA) by (eapply InterpType_resp_PerType; eauto).
find_extdeters.
close_doms.

destruct e with da as [DB_].
eauto.

destruct e' with da as [DB_'].
eauto.


specialize (HS5 Delta i a da DB_ DB_' PDA).
specialize (HS5 HPDA).
assert (da === da #in# PDA) as HdadaPDA.
eauto.
specialize (HS5 HdadaPDA H15 H16 H3).

assert ( Delta ||- a : TmSb A (Sups i) #aeq# da #in# HDA ) as HS1_P.
specialize (IHHDT (TmSb A (Sups i)) Delta _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
move IHHDT_Term at bottom.
specialize (IHHDT_Term a da da PDA HdadaPDA HPDA).
apply IHHDT_Term.
eauto.

specialize (HS5 HS1_P).
clear_inversion H2.

clear_inversion H1.
find_extdeters.
close_extdeters.
specialize (H2 da da H17).
destruct H2 as [y0 [y1 [Y HY]]].
destruct HY as [HY [Hy0 [Hy1 HyyY]]].
destruct HS5 as [dy HS6].
program_simpl.
assert (dy = y0) by eauto 2; subst.
exists y1.
split; auto.

move H at bottom.

assert (DB_' = DB) by eauto 2; subst.


specialize (H _ _ _ _ PDA HPDA HdadaPDA H15 H16).
specialize (H ( TmSb F (Sext (Sups i) a))).
specialize (H Delta _ eq_refl _ (H_PDB' da da DB DB' PDA'0 HPA Hda Happ Happ' ) ).
destruct H.
eapply H25.
eauto.
eauto.
eauto.

(* <- *)

simpl in HS.
destruct HS as [? [A [F [Pfun [HS1 [HS2 [HS3 [HS4 HS5]]]]]]]].
simpl. split; auto. exists A, F, Pfun.

assert (InterpType (Dfun DA DF) Pfun).
apply InterpType_resp_PerType with (DA := Dfun DA' DF'); auto.
symmetry.
eapply PerType_fun; eauto 4.


repeat (split; auto).
find_extdeters.
close_extdeters.
eauto.

(**)

specialize (IHHDT A Gamma _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
(*specialize (IHHDT_Type _ HDA').*)
apply IHHDT_Type; eauto.

(**)

intros.
renamer.

assert (InterpType DA PDA').
eapply InterpType_resp_PerType.
eauto 1.
symmetry; auto.

find_extdeters.
close_doms.
 
destruct e' with da as [DB_].
eauto.

destruct e0' with da as [DB_'].
eauto.

assert (DB' = DB_) by eauto 2; subst.

specialize (HS5 Delta i a da DB_ DB_' PDA').
specialize (HS5 HPDA').
assert (da === da #in# PDA') as HdadaPDA'.
eauto.
specialize (HS5 HdadaPDA' H16 H17 H4).


assert ( Delta ||- a : TmSb A (Sups i) #aeq# da #in# HDA' ) as HS1_P.
specialize (IHHDT (TmSb A (Sups i)) Delta _ eq_refl).
edestruct IHHDT as [IHHDT_Type IHHDT_Term].
move IHHDT_Term at bottom.
specialize (IHHDT_Term a da da _ HdadaPDA' H6).
eauto.
apply IHHDT_Term.
eauto.


specialize (HS5 HS1_P).
clear_inversion H2.
clear_inversion H1.
find_extdeters.
close_extdeters.
specialize (H2 da da H23).
destruct H2 as [y0 [y1 [Y HY]]].
destruct HY as [HY [Hy0 [Hy1 HyyY]]].
destruct HS5 as [dy HS6].
program_simpl.
assert (dy = y1) by eauto 2; subst.
exists y0.
split; auto.

move H at bottom.

specialize (H _ _ _ _ _ HPA Hda Happ Happ').
specialize (H ( TmSb F (Sext (Sups i) a))).
specialize (H Delta _ eq_refl _ (H_PDB' da da _ _ PDA' HPDA' HdadaPDA' H16 H17 ) ).
destruct H.
eapply H28.
eauto.
eauto.
eauto.

Qed.
Hint Immediate PerType_fun_is_closed_under_per_tm.
