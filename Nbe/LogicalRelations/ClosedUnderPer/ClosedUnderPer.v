(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Logical relations are closed under equality modeled by PER relation
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.

Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.

Require Import Nbe.LogicalRelations.ClosedUnderPer.Common.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Nat.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Unit.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Empty.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Fun.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Univ.
Require Import Nbe.LogicalRelations.ClosedUnderPer.NeUniv.

(******************************************************************************
 *)

Theorem ClosedUnderPer: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  Gamma ||- T #in# HDT -> 
  forall DT'' (HDT' : DT' === DT'' #in# PerType),
  Gamma ||- T #in# HDT' /\
 (forall t dt dt' PT, Gamma ||- t : T #aeq# dt #in# HDT -> dt === dt' #in# PT -> InterpType DT PT ->
   Gamma ||- t : T #aeq# dt' #in# HDT').

Proof.
(* Stronger induction hypothesis is required *)
cut (forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  forall DT'' (HDT' : DT' === DT'' #in# PerType), (Gamma ||- T #in# HDT <-> Gamma ||- T #in# HDT')
/\
  (forall t dt dt' PT, dt === dt' #in# PT -> InterpType DT PT ->
    (Gamma ||- t : T #aeq# dt #in# HDT <-> Gamma ||- t : T #aeq# dt' #in# HDT'))
).
intros.
specialize (H Gamma T DT DT' HDT DT'' HDT').
program_simpl.
repeat (split; auto).
apply H.
auto.

intros.
eapply H1; eauto.
(**)

intros until HDT.
cut (DT' = DT'); auto.
generalize DT' at 1 3 5 7 as DT'_.
generalize dependent Gamma.
generalize dependent T.
(* Theorem for RelType for base types are solved by simple destruction and inversion! *)
induction HDT using PerType_dind; split; intros;
  try solve [
    destruct HDT'; try solve [ clear_inversion H ];
    split; auto
      ]; eauto.



Qed.


(***************************************************************************
 *)

Corollary RelType_ClosedUnderPer: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  forall DT'' (HDT' : DT' === DT'' #in# PerType),
  Gamma ||- T #in# HDT -> 
  Gamma ||- T #in# HDT'
.
Proof.
intros.
edestruct ClosedUnderPer; eauto.
Qed.

(***************************************************************************
 *)
Corollary RelTerm_ClosedUnderPer: forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  forall DT'' (HDT' : DT' === DT'' #in# PerType),
  forall t dt dt' PT,
   Gamma ||- t : T #aeq# dt #in# HDT -> dt === dt' #in# PT -> InterpType DT PT ->
   Gamma ||- t : T #aeq# dt' #in# HDT'
.
Proof.
intros.
destruct ClosedUnderPer with (Gamma := Gamma) (T := T) (DT := DT) (DT' := DT') (HDT := HDT) (DT'' := DT'') (HDT' := HDT').
eapply RelTerm_implies_RelType.
apply H.
eauto.
Qed.

(***************************************************************************
 *)
Corollary RelTerm_ClosedUnderPer': forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  forall t dt dt' PT,
   Gamma ||- t : T #aeq# dt #in# HDT -> dt === dt' #in# PT -> InterpType DT PT ->
   Gamma ||- t : T #aeq# dt' #in# HDT
.
Proof.
intros.
assert (DT' === DT #in# PerType ) as HDT' by (symmetry; auto).

destruct ClosedUnderPer with (Gamma := Gamma) (T := T) (DT := DT) (DT' := DT') (HDT := HDT) (DT'' := DT) (HDT' := HDT'); auto.
eapply RelTerm_implies_RelType.
apply H.

destruct ClosedUnderPer with (Gamma := Gamma) (T := T) (DT := DT') (DT' := DT) (HDT := HDT') (DT'' := DT') (HDT' := HDT); auto.
eauto.
Qed.

(***************************************************************************
 *)
Corollary RelTerm_ClosedUnderPer'': forall Gamma T DT DT' (HDT : DT === DT' #in# PerType),
  forall t dt DT'' (HDT' : DT' === DT'' #in# PerType),
   Gamma ||- t : T #aeq# dt #in# HDT ->
   Gamma ||- t : T #aeq# dt #in# HDT'
.
Proof.
intros.

destruct ClosedUnderPer with (Gamma := Gamma) (T := T) (DT := DT) (DT' := DT') (HDT := HDT) (DT'' := DT'') (HDT' := HDT'); auto.
eapply RelTerm_implies_RelType.
apply H.

assert (exists PT, InterpType DT PT) as HX1.
eapply RelTerm_has_InterpType.
eauto.

destruct HX1 as [PT HPT].

assert (dt === dt #in# PT) as HX2.
eapply RelTerm_resp_InterpType.
eauto.
auto.

eapply H1.
eauto.
eauto.
auto.
Qed.


(***************************************************************************
 *)
Lemma RelType_ClosedUnderPer_sym: forall Delta T DT DT'
  (HDT1 : DT === DT' #in# PerType) (HDT2: DT' === DT #in# PerType),
Delta ||- T #in# HDT1 ->
Delta ||- T #in# HDT2
.
Proof.
intros.

assert (DT' === DT' #in# PerType) as HX1.
eapply PER_Transitive with DT.
symmetry; auto.
auto.

assert (Delta ||- T #in# HX1) as HX2.
eapply RelType_ClosedUnderPer.
eauto.

eapply RelType_ClosedUnderPer.
eauto.
Qed.

(***************************************************************************
 *)
Lemma RelType_ClosedUnderPer_right: forall Delta T DT DT' DT'' 
  (HDT1 : DT === DT' #in# PerType) (HDT2: DT === DT'' #in# PerType),
Delta ||- T #in# HDT1 ->
Delta ||- T #in# HDT2
.
Proof.
intros.

assert (DT' === DT'' #in# PerType) as HX1.
apply PER_Transitive with DT.
symmetry; auto.
auto.

assert (Delta ||- T #in# HX1 ) as HX2.
eapply RelType_ClosedUnderPer.
eauto.

assert (DT'' === DT #in# PerType ) as HX3.
symmetry; auto.

assert (Delta ||- T #in# HX3 ) as HX4.
eapply RelType_ClosedUnderPer.
eauto.

eapply RelType_ClosedUnderPer_sym.
eauto.
Qed.

(***************************************************************************
 *)
Lemma RelTerm_ClosedUnderPer_sym: forall Delta t T DT DT' dt dt' PT
  (HDT1 : DT === DT' #in# PerType) (HDT2: DT' === DT #in# PerType),
  dt === dt' #in# PT -> InterpType DT PT ->
Delta ||- t : T #aeq# dt #in# HDT1 ->
Delta ||- t : T #aeq# dt' #in# HDT2
.
Proof.
intros.

assert (DT' === DT' #in# PerType) as HX1.
eapply PER_Transitive with DT.
symmetry; auto.
auto.

assert (Delta ||- t : T #aeq# dt' #in# HX1) as HX2.
eapply RelTerm_ClosedUnderPer.
eauto.
eapply H.
eauto.

assert (PER PT) as HX3 by eauto.

eapply RelTerm_ClosedUnderPer with (PT := PT).
eauto.
eauto.
eauto.
Qed.

(***************************************************************************
 *)
Lemma RelTerm_ClosedUnderPer_right: forall Delta t T DT DT' DT'' dt dt' PT
  (HDT1 : DT === DT' #in# PerType) (HDT2: DT === DT'' #in# PerType),
  dt === dt' #in# PT -> InterpType DT PT ->
  Delta ||- t : T #aeq# dt #in# HDT1 ->
  Delta ||- t : T #aeq# dt' #in# HDT2
.
Proof.
intros.

assert (DT' === DT'' #in# PerType) as HX1.
apply PER_Transitive with DT.
symmetry; auto.
auto.

assert (Delta ||- t : T #aeq# dt #in# HX1 ) as HX2.
eapply RelTerm_ClosedUnderPer with (PT := PT).
eauto.
eauto.
eauto.


assert (DT'' === DT #in# PerType ) as HX3.
symmetry; auto.

assert (Delta ||- t : T #aeq# dt #in#  HX3 ) as HX4.
eapply RelTerm_ClosedUnderPer with (PT := PT).
eauto.
eauto.
eauto.

eapply RelTerm_ClosedUnderPer_sym with (PT := PT).
eauto.
eauto.
eauto.
Qed.

