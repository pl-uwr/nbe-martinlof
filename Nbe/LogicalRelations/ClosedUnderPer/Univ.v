(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.

Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderPer.Common.

(******************************************************************************
 *)
Lemma PerType_univ_closed_under_per_tp: forall T Gamma DT'_ DT'' 
  (i : forall d1 d2 : D, d1 === d2 #in# PerUniv -> d1 === d2 #in# PerType)
  (HDT' : DT'_ === DT'' #in# PerType),
  (forall (d1 d2 : D) (i0 : d1 === d2 #in# PerUniv) 
        (T : Tm) (Gamma : Cxt) (DT'_ : D),
      DT'_ = d2 ->
      forall (DT'' : D) (HDT' : DT'_ === DT'' #in# PerType),
      (Gamma ||- T #in# i d1 d2 i0 <-> Gamma ||- T #in# HDT') /\
      (forall (t : Tm) (dt dt' : D) (PT : relation D),
       dt === dt' #in# PT ->
       InterpType d1 PT ->
       (Gamma ||- t : T #aeq# dt #in# i d1 d2 i0 <->
        Gamma ||- t : T #aeq# dt' #in# HDT'))) ->
  DT'_ = Duniv ->
  (Gamma ||- T #in# PerType_univ i <-> Gamma ||- T #in# HDT').
Proof.
intros.

destruct HDT'; try solve [clear_inversion H0 ].
clear_inversion H0.

split; intro; eauto.
Qed.
Hint Immediate PerType_univ_closed_under_per_tp.


(******************************************************************************
 *)

Lemma PerType_univ_closed_under_per_tm: forall T Gamma DT'_ DT'' t dt dt' PT
  (i : forall d1 d2 : D, d1 === d2 #in# PerUniv -> d1 === d2 #in# PerType)
  (HDT' : DT'_ === DT'' #in# PerType),
  (forall (d1 d2 : D) (i0 : d1 === d2 #in# PerUniv) 
        (T : Tm) (Gamma : Cxt) (DT'_ : D),
      DT'_ = d2 ->
      forall (DT'' : D) (HDT' : DT'_ === DT'' #in# PerType),
      (Gamma ||- T #in# i d1 d2 i0 <-> Gamma ||- T #in# HDT') /\
      (forall (t : Tm) (dt dt' : D) (PT : relation D),
       dt === dt' #in# PT ->
       InterpType d1 PT ->
       (Gamma ||- t : T #aeq# dt #in# i d1 d2 i0 <->
        Gamma ||- t : T #aeq# dt' #in# HDT'))) ->
  DT'_ = Duniv ->
  dt === dt' #in# PT ->
  InterpType Duniv PT ->
 (Gamma ||- t : T #aeq# dt #in# PerType_univ i <->  Gamma ||- t : T #aeq# dt' #in# HDT')
.
Proof.
intros.
destruct HDT'; try solve [clear_inversion H0 ].
clear_inversion H2.

split; intro; eauto.
+{
simpl in *.
decompose record H2.
rename x into HDU.

rename H1 into HDU'.

assert (forall d1 d2 d3 (PHDU: d1 === d2 #in# PerUniv) (PHDU': d2 === d3 #in# PerUniv),
             Gamma ||- t #in# i d1 d2 PHDU -> Gamma ||- t #in# i d2 d3 PHDU') as HF.
{
intros.
specialize (H _ _ PHDU).
specialize (H t Gamma d2 eq_refl).
specialize (H d3 (i d2 d3 PHDU')).
destruct H.
eapply H.
eauto.
}

assert (dt' === dt' #in# PerUniv) as HDU''.
{
eapply Per_domR; eauto.
}

assert (Gamma ||- t #in# i dt' dt' HDU'') as HX1.
{
eapply HF with (PHDU := HDU').
eapply HF.
eauto.
}


specialize (H _ _ HDU'').
specialize (H t Gamma dt' eq_refl).
specialize (H _ (i0 dt' dt' HDU'')).
destruct H.
destruct H.


repeat (apply conj; auto).
{
intros.
decompose record H2.
specialize (H10 _ _ H8).
destruct H10 as [tv [HTV1 HTV2]].
exists tv; split; auto.

assert (Ddown Duniv dt === Ddown Duniv dt' #in# PerNf) as HX2.
{
eapply Reify_Characterization with (PT := PerUniv).
eapply PerType_univ; intros; eauto.
eauto.
eauto.
}

clear_inversion HX2.
destruct H10 with (m := length Delta).
destruct H9.
assert (x0 = tv)  as Hx3.
{
eapply RbNf_deter; eauto.
}
subst.
auto.
}


exists HDU''.
eauto.
}


+{
simpl in *.
decompose record H2.
rename x into HDU.

rename H1 into HDU'.

assert (forall d1 d2 d3 (PHDU: d1 === d2 #in# PerUniv) (PHDU': d2 === d3 #in# PerUniv),
             Gamma ||- t #in# i d1 d2 PHDU -> Gamma ||- t #in# i d2 d3 PHDU') as HF.
{
intros.
specialize (H _ _ PHDU).
specialize (H t Gamma d2 eq_refl).
specialize (H d3 (i d2 d3 PHDU')).
destruct H.
eapply H.
eauto.
}

assert (dt === dt #in# PerUniv) as HDU''.
{
eapply Per_domR; eauto.
}

repeat (apply conj; auto).

{
intros.
decompose record H2.
specialize (H8 _ _ H1).
destruct H8 as [tv [HTV1 HTV2]].
exists tv; split; auto.

assert (Ddown Duniv dt === Ddown Duniv dt' #in# PerNf) as HX2.
{
eapply Reify_Characterization with (PT := PerUniv).
eapply PerType_univ; intros; eauto.
eauto.
eauto.
}

clear_inversion HX2.
destruct H8 with (m := length Delta).
destruct H7.
assert (x0 = tv)  as Hx3.
{
eapply RbNf_deter; eauto.
}
subst.
auto.
}


exists HDU''.




specialize (H _ _ HDU).
specialize (H t Gamma dt' eq_refl).
specialize (H _ (i0 dt' dt' HDU)).
destruct H.
destruct H.

assert (dt' === dt #in# PerUniv) as HDU'_sym.
{
symmetry.
auto.
}

eapply HF with (PHDU := HDU'_sym).
eapply HF.
eapply H7.
eauto.
}
Qed.
Hint Immediate PerType_univ_closed_under_per_tm.

