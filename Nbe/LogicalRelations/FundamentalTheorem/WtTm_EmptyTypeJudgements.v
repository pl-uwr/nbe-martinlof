(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.


Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ConvertibleToReification.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.Lift.

(******************************************************************************
 *)

Theorem Rel_Fundamental_Absurd: forall Gamma A Delta SB denv DT DT' (HDT : DT === DT' #in# PerType) dt,
  Gamma |-- A ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
      EvalTm A denv DT -> Delta ||- TmSb A SB #in# HDT) ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm (TmArr TmEmpty A) denv DT ->
  EvalTm (TmAbsurd A) denv dt ->
  Delta ||- TmSb (TmAbsurd A) SB : TmSb (TmArr TmEmpty A) SB #aeq# dt
   #in# HDT
.
Proof.

intros.

unfold TmArr in *.
clear_inversion H2.
clear_inversion H8.
clear_inversion H3.

dependent inversion HDT; subst.

rename i into HDempty.
rename i0 into HInterp.
rename e into HappDF.
rename e0 into HappDF'.
rename i1 into HREC.

inversion HInterp; subst.

(**)

assert (Delta |-- [SB] : Gamma) as HYY1.
eapply RelSb_wellformed.
eauto.

simpl.
split.

eapply System.SB.
eauto.
eapply EMPTY_E.
auto.

exists TmEmpty. 
exists (TmSb A (Sseq SB Sup)).

assert (exists PT, InterpType (Dfun Dempty (Dclo (TmSb A Sup) denv)) PT) as HYY2.
eauto.

destruct HYY2 as [PT HPT].
exists PT.
split; auto.

clear_inversion HPT.
clear_inversion H5.

split.
{
(**)

constructor 1.
intros. 
inversion H2; subst.
rename H2 into Hempty.
rename H3 into H2.

exists (Dup DA (DneAbsurd (DdownN DA) e)).
exists (Dup DA (DneAbsurd (DdownN DA) e')).

clear_inversion H6.
clear_inversion po_ro.
destruct ro_resp_ex with (Dup Dempty e) as [Y HY].
eauto.
 
exists Y.
split.
eauto.

split.
unfold Dup at 1. simpl. 
eauto.

split.
unfold Dup at 1. simpl. 
eauto.

assert (Gamma |= A).
eapply Valid_for_WfTp.
eauto.

clear_inversion H3.
assert ([denv] === [denv] #in# Gamma).
eapply RelSb_valenv.
eauto.

specialize (H6 _ _ H3).
destruct H6.
destruct H6.
destruct H6.
destruct H8.
renamer.
elim_deters.

eapply Reflect_Characterization.
eauto.

assert (Dup Dempty e === Dup Dempty e #in# PerEmpty) as He.
eauto.

eapply H7.

eapply He.
eauto.
eauto.

constructor 1.
intros.


clear_inversion H2.
assert (exists PA, InterpType DA_denv1 PA) as HPA.
eauto.
destruct HPA as [PA HPA].

assert (DdownN DA_denv1 === DdownN DA_denv1 #in# PerNf) as Htp.
eapply ReifyTp_Characterization.
eauto.
eauto.

clear_inversion Htp.

edestruct H10 as [ne [Hne Hne']].
edestruct H2 as [NFA [HNFA _]].

eexists.
split.
eauto.
eauto.
}
(**)


assert (Delta,, TmSb TmEmpty SB |--  [Sext (Sseq SB Sup) TmVar]: Gamma,, TmEmpty) as HXX1.
{
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.
eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply SB_F.
eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_REFL.
eapply SB_F.
eauto.
eauto.
}

(**)
split.
{
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_FUN.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_SB_REFL.
eauto.
eauto.
}

(****)
split.
{

dependent inversion HDempty.
subst; simpl.
eauto.
}
(*****)

intros.
inversion HPA; subst.
inversion Hda; subst.

eexists.
split.
unfold Dup; simpl; eauto.

inversion Happ; subst.
clear_inversion H13.
clear_inversion H11.

assert (DA = DB) as HDAeqDB by eauto.
rewrite HDAeqDB in *.
elim_deters.
renamer.
clear_dups.
(* Coq8.4 -> Coq8.6 *)
rename H4 into H15.

(**)

assert ( Delta0 |-- [Sseq SB (Sups i)] : Gamma ) as HXX2 by eauto.

assert ( Delta0 ||- [Sseq SB (Sups i)] : Gamma #aeq# envs ) as HXX3.
{
eapply RelSb_ClosedUnderLift.
eauto.
eauto.
}

assert (Delta0 |-- a :  TmSb TmEmpty (Sups i)) as HXX4.
{
eapply RelTerm_implies_WtTm.
eauto.
}

assert (Delta0 |-- a :  TmEmpty) as HXX5.
{
eapply CONV.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
}

assert (Delta0 |--  [Sext (Sups i) a]: Delta,,TmEmpty) as HXX7.
{
eapply SEXT.
eauto.
eauto.
eauto.
}

assert (Delta0 |--  TmSb (TmSb A (Sseq SB Sup)) (Sext (Sups i) a) === TmSb A (Sseq SB (Sups i))) as HXX6.
{
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.


eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eapply EQ_SB_REFL.
eauto.
eauto.
}
(**)

assert ( Delta0 ||- TmSb (TmSb A (Sseq SB Sup)) (Sext (Sups i) a)
   #in# HREC (Dup Dempty e) (Dup Dempty e) DA_envs0 DB' PerEmpty HPA Hda Happ
          Happ') as HZZ2.

eapply RelType_ClosedForConversion.
eauto.
eauto.

specialize (H0 _ _ _ HXX3).
specialize (H0 _ _ (HREC (Dup Dempty e) (Dup Dempty e) DA_envs0 DB' PerEmpty HPA Hda Happ
          Happ')).
specialize (H0 H15).

(****)
eapply RelTerm_Back.
eauto.

renamer.
assert (DdownN DA_envs === DdownN DA_envs #in# PerNf) as HXXX4.

assert (Gamma |= A) as HXXX2.
eapply Valid_for_WfTp.
eauto.


clear_inversion HXXX2.
assert ([envs] === [envs] #in# Gamma) as HXXX3.
eapply RelSb_valenv.
eauto.

specialize (H8 _ _ HXXX3).
destruct H8 as [DA_ [ _DA [_HDA [HDA_ HDA]]]].

elim_deters.
renamer.

assert (exists PT, InterpType DA_envs PT) as HPT.
eauto.

destruct HPT as [PT HPT].
renamer.
 
eapply ReifyTp_Characterization.
eauto.
eauto.
clear_inversion HXXX4.

 
(**)
constructor 1.

intros.

clear_inversion H5.

edestruct H8 as [ne [Hne1 Hne2]].
edestruct H4 as [NFA [HNFA1 HNFA2]].
eexists.
split.
eauto.
eauto.
 
(*****)

intros.
renamer.
assert (DdownN DA_envs === DdownN DA_envs #in# PerNf) as HXXX4.

assert (Gamma |= A) as HXXX2.
eapply Valid_for_WfTp.
eauto.


clear_inversion HXXX2.
assert ([envs] === [envs] #in# Gamma) as HXXX3.
eapply RelSb_valenv.
eauto.

specialize (H10 _ _ HXXX3).
destruct H10 as [DA_ [ _DA [_HDA [HDA_ HDA]]]].

elim_deters.
renamer.

assert (exists PT, InterpType DA_envs PT) as HPT.
eauto.

destruct HPT as [PT HPT].
renamer.
 
eapply ReifyTp_Characterization.
eauto.
eauto.
clear_inversion HXXX4.
inversion H5; subst.
 
edestruct H10 as [ne [Hne1 Hne2]].
edestruct H8 as [NFA [HNFA1 HNFA2]].
eexists.
split.
eauto.

(**)

rename Delta0 into Psi.
rename Delta1 into Theta.

move a at bottom.
remember (HREC (Dup Dempty e) (Dup Dempty e) DA_envs DB' PerEmpty HPA Hda
                Happ Happ') as HDA.

rename i0 into j.
move HDA at bottom.
assert (Theta ||- TmSb a (Sups j) : TmSb (TmSb TmEmpty (Sups i)) (Sups j) #aeq# Dup Dempty e #in# HDempty) as HX1.
eapply RelTermLifting.
eauto.
eauto.

assert (Theta ||- TmSb (TmSb TmEmpty (Sups i)) (Sups j) #in# HDempty) as HX2.
eapply RelTerm_implies_RelType.
eauto.

assert (Theta |-- TmSb a (Sups j) === ne : TmSb (TmSb TmEmpty (Sups i)) (Sups j)) as HX3.
edestruct RelTerm_Reify.
eauto.
eauto.
destruct H11.
clear_inversion H11.
renamer.
assert (ne = x) as HHH.
eapply RbNe_deter.
eapply Hne1.
eauto.
subst.
elim_deters.
clear_dups.
eauto.


assert (Theta ||- TmSb (TmSb A (Sseq SB (Sups i))) (Sups j) #in# HDA) as HX4.
eapply RelTypeLifting.
eauto.
eauto.


assert (Theta |-- TmSb (TmSb A (Sseq SB (Sups i))) (Sups j) === NFA) as HX5.
edestruct RelType_Reify.
eauto.
clear_inversion H11.
assert (x = NFA) as HHH.
eapply RbNf_deter.
eauto.
eauto.
subst.
eauto.

assert (Delta |-- TmSb (TmAbsurd A) SB : TmSb (TmArr TmEmpty A) SB) as HX6.
eapply System.SB.
eauto.
eauto.


assert (Gamma,, TmEmpty |-- TmSb A Sup) as HX7.
eapply SB_F.
eauto.
eauto.

assert (Gamma |-- TmFun TmEmpty (TmSb A Sup)) as HX10.
eapply FUN_F.
eauto.
eauto.

assert (Psi |-- a : TmSb TmEmpty (Sseq SB (Sups i))) as HX11.
eapply CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eauto.


assert (Theta |-- TmSb (TmSb A (Sseq SB (Sups i))) (Sups j) === TmSb A (Sseq (Sseq SB (Sups i)) (Sups j))) as HX8.
eapply EQ_TP_SBSEQ; eauto.

assert (Theta |-- TmSb A (Sseq (Sseq SB (Sups i)) (Sups j)) === NFA) as HX9.
eapply EQ_TP_TRANS.
eapply EQ_TP_SYM.
eauto.
eauto.

assert (Theta |--  [Sseq SB (Sseq (Sups i) (Sups j))]=== [Sseq (Sseq SB (Sups i)) (Sups j)]:Gamma) as H12.
eapply EQ_SB_SYM.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j))) |--  [Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup]:Gamma) as HX13.
eapply SSEQ.
eauto.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j))) |-- TmVar : TmSb TmEmpty (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup)) as HX14.
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_REFL.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eapply SUP.
eapply SB_F.
eauto.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j))) |--  [Sext (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) TmVar]:Gamma,, TmEmpty ) as HX15.
eapply SEXT.
eauto.
eapply EMPTY_F.
eauto.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j))) |-- 
   [Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup]===
   [Sseq SB (Sseq (Sseq (Sups i) (Sups j)) Sup)]:Gamma) as HX16.

eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j))) |--  [Sseq SB (Sseq (Sseq (Sups i) (Sups j)) Sup)] === [Sseq Sup (Sext (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) TmVar)] :Gamma) as HX17.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SUP.
eapply SSEQ.
eapply SUP.
eapply SB_F.
eauto.
eauto.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j))) |-- 
   [Sseq SB (Sseq (Sseq (Sups i) (Sups j)) Sup)]===
   [Sseq Sup (Sext (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) TmVar)]
   :Gamma) as HX18.
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
 
assert (Gamma |-- TmAbsurd A : TmArr TmEmpty A) as HX12.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j)))
   |-- TmSb A (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) ===
   TmSb A
     (Sseq Sup (Sext (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) TmVar))) as HX19.
eapply EQ_TP_CONG_SB; [ | eauto ].
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eauto.

assert ( Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j)))
   |-- TmSb A
         (Sseq Sup (Sext (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) TmVar)) ===
   TmSb (TmSb A (Sseq SB (Sseq (Sups i) (Sups j)))) Sup) as HX20.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.

assert (Theta,, TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j)))
   |-- TmSb (TmSb A Sup)
         (Sext (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) TmVar) ===
   TmSb (TmSb A (Sseq SB (Sseq (Sups i) (Sups j)))) Sup) as HX21.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.


assert (Theta
   |-- TmSb (TmFun TmEmpty (TmSb A Sup)) (Sseq SB (Sseq (Sups i) (Sups j))) ===
   TmFun TmEmpty (TmSb (TmSb A (Sseq SB (Sseq (Sups i) (Sups j)))) Sup)) as HX22.

eapply EQ_TP_TRANS.
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_FUN.
eapply EQ_TP_SB_EMPTY.
eauto.
eauto.
 
assert ( Theta |-- TmArr TmEmpty (TmSb A (Sseq SB (Sseq (Sups i) (Sups j)))) ===
   TmSb (TmSb (TmArr TmEmpty A) SB) (Sseq (Sups i) (Sups j))) as HX23.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.

assert ( Theta |-- TmSb TmEmpty (Sseq SB (Sseq (Sups i) (Sups j))) ===
   TmSb (TmSb TmEmpty (Sups i)) (Sups j)) as HX24.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
 

(* Wielka Wojna Ojczyzniana *)

eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SBAPP.
eauto.

eapply CONV.
eauto.

unfold TmArr.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.
eauto.

eapply EQ_CONV.
eapply EQ_CONG_FUN_E.

eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SBSEQ.
eauto.
eauto.

eauto.

eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SBABSURD.
eauto.
eauto.

eapply EQ_CONG_ABSURD.
eapply EQ_TP_TRANS.
eapply EQ_TP_SYM.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS ; [ | eauto ].
eapply EQ_TP_CONG_SB.

eauto.
eauto.

(* hack to unify existential variables with expected types *)
Focus 4.
eauto.

(*.*)


eauto.
unfold TmArr.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

unfold TmArr in *.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SB_FUN.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_FUN.
eauto.

eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

(**)

assert (Theta |-- TmSb a (Sups j) : TmSb (TmSb TmEmpty (Sups i)) (Sups j)) as HX25.
eauto.

assert (Theta |-- TmSb (TmSb TmEmpty (Sups i)) (Sups j)) as HX26.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SEXT.
eauto.
eauto.

eapply CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
eapply SB_F.
eauto.
eauto.

eapply SSEQ.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply EMPTY_F.
eauto.

eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eapply SSEQ.
eauto.
eapply SSEQ.
eauto.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eauto.
eapply EQ_TP_CONG_SB.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_TP_SB_EMPTY.
eauto.
eauto.
eauto.

(**)

 
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SEXT.
eauto.
eapply EMPTY_F.
eauto.

eapply CONV.
eapply System.SB.
eauto.
eauto.

eauto.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply EMPTY_F.
eauto.

eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.

eapply SSEQ.
eauto.
eauto.
eauto.

(* tyriada c.d. *)

assert (Psi,, TmEmpty |-- TmVar : TmSb TmEmpty (Sseq (Sseq SB (Sups i)) Sup)) as HX30.
eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eapply SSEQ.
eauto.
eauto.


eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SSEQ.
eapply SEXT.
eauto.
eapply EMPTY_F.
eauto.
eapply CONV.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eauto.

eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply EMPTY_F.
eauto.

eauto.
eauto.
eauto.

(* ... *)
 
eapply EQ_TP_CONG_SB.
unfold Ssing.


assert (Theta |-- 
   [Sseq Sup
      (Sseq (Sext (Sseq (Sseq SB (Sups i)) Sup) TmVar)
         (Sext (Sups j) (TmSb a (Sups j))))]===
   [Sseq
      (Sseq Sup (Sext (Sseq (Sseq SB (Sseq (Sups i) (Sups j))) Sup) TmVar))
      (Sext Sid (TmSb a (Sups j)))]:Gamma) as HXDELUXE.

eapply EQ_SB_TRANS.
eapply EQ_SB_SYM.
eapply EQ_SB_COMM.

eapply SEXT.
eauto.
eapply EMPTY_F.
eauto.
eauto.

eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eauto.

 
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL.
eapply SEXT.
eauto.
eapply EMPTY_F.
eauto.
eauto.
 
eapply EQ_SB_SUP.
eapply SSEQ.
eauto.
eauto.
eapply EMPTY_F.
eauto.
eauto.

(**)
 
eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL.
eapply SEXT.
eauto.
eauto.
eauto.

eapply EQ_SB_SUP.
eapply SSEQ.
eauto.
eauto.
eapply EMPTY_F.
eauto.
eauto. 

eapply CONV.
eapply HYP.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eapply SSEQ.
eauto.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eapply SSEQ.
eauto.
eauto.


eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.

eapply SEXT.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.

eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eapply EQ_SB_REFL; eauto.

eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.

eapply SEXT.
eauto.

eapply EMPTY_F.
eauto.
eauto.
eapply CONV.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eapply SSEQ.
eauto.
eauto.
 
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.

eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
 
eapply CONV.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eapply SSEQ.
eauto.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eauto.
eapply EQ_SB_REFL; eauto.
eauto.

eauto.

(****)
eapply EQ_TP_REFL.
eauto.
(****)


assert (Theta |--  [Sext (Sups j) (TmSb a (Sups j))]:Psi,, TmEmpty) as HX40.
eapply SEXT.
eauto.
eapply EMPTY_F.
eauto.
eauto.



assert ( Psi,, TmEmpty |-- TmSb TmEmpty Sup ===  TmSb TmEmpty (Sseq (Sseq SB (Sups i)) Sup)) as HX41.
eapply EQ_TP_TRANS.
eapply EQ_TP_SB_EMPTY.
eapply SUP.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SB_EMPTY.
eauto.

assert (Psi,, TmEmpty |-- TmVar : TmSb TmEmpty (Sseq (Sseq SB (Sups i)) Sup)) as HX42.
eapply CONV.
eapply HYP.
eauto.
eauto.

assert (Psi,, TmEmpty |--  [Sext (Sseq (Sseq SB (Sups i)) Sup) TmVar]:Gamma,, TmEmpty) as HX43.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eapply EMPTY_F.
eauto.
eauto.

assert (Delta,, TmEmpty |-- TmSb A (Sseq SB Sup)) as HX44.
eapply SB_F.
eapply SSEQ.
eauto.
eauto.
eauto.


eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.


eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.


eapply EQ_TP_CONG_SB.

eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SYM.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eapply EQ_SB_REFL.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL; eauto.
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eapply EQ_SB_REFL; eauto.

eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SYM.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_REFL;
eauto.
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eapply EMPTY_F.
eauto.
eauto.
eapply EQ_SB_REFL; eauto.

eauto.

(**)
eauto.
Qed.


