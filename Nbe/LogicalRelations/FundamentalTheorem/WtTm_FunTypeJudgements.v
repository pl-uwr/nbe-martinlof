(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Nbe.Syntax.
Require Import Nbe.Syntax.LogRelTmSyntaxStuff.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.ConvertibleToReification.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.ProofIrrelevance.
Require Import Nbe.LogicalRelations.Lift.


(******************************************************************************
 *)
Lemma Helper_Eq: forall Gamma A XA DA (HDA : DA === DA #in# PerType), 
  Gamma ||- A #in# HDA ->
  Gamma ||- XA #in# HDA ->
  Gamma |-- A === XA.
Proof.
intros.
edestruct RelType_Reify with (Gamma := Gamma) (T := A) (DT := DA) (HDT := HDA) as [Anf].
auto.
clear_inversion H1.

edestruct RelType_Reify with (Gamma := Gamma) (T := XA) (DT := DA) (HDT := HDA) as [XAnf].
eauto.
clear_inversion H1.

assert (Anf = XAnf).
eapply RbNf_deter.
eauto.
eauto.
subst.

eapply EQ_TP_TRANS.
eauto.
auto.
Qed.

(******************************************************************************
 *)
Lemma Helper_Eq' : forall Gamma A XA DA (HDA HDA' : DA === DA #in# PerType),
  Gamma ||- A #in# HDA ->
  Gamma ||- XA #in# HDA' ->
  Gamma |-- A === XA.
Proof.
intros.

assert (Gamma ||-  XA #in# HDA).
eapply RelType_ProofIrrelevance.
eauto.

eapply Helper_Eq.
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Lemma Helper_Eq'' : forall Gamma A XA DA DA' (HDA : DA === DA' #in# PerType),
  Gamma ||- A #in# HDA ->
  Gamma ||- XA #in# HDA ->
  Gamma |-- A === XA
.
Proof.
intros.

assert (DA === DA #in# PerType) as HDA_refl.
eauto.

assert (DA' === DA #in# PerType) as HDA_sym.
symmetry.
auto.

assert (Gamma ||- A #in# HDA_sym) as HXX1.
eapply RelType_ClosedUnderPer_sym.
eauto.

assert (Gamma ||- XA #in# HDA_sym) as HXX2.
eapply RelType_ClosedUnderPer_sym.
eauto.

assert (Gamma ||- A #in# HDA_refl) as HXX3.
eapply RelType_ClosedUnderPer.
eauto.

assert (Gamma ||- XA #in# HDA_refl) as HXX4.
eapply RelType_ClosedUnderPer.
eauto.

eapply Helper_Eq.
eauto.
eauto.
Qed.

(******************************************************************************
 *)
Lemma Ssing_eq: forall SB B N A Gamma Delta,
  Gamma |-- [SB] : Delta ->
  Delta |-- N : A ->
  Delta,,A |-- B ->
  Gamma |-- TmSb (TmSb B (Ssing N)) SB === TmSb B (Sext SB (TmSb N SB))
.
Proof.
intros.
assert (Delta |-- A) as HX1.
eauto.

assert (Delta |-- N : TmSb A Sid) as HX2.
eapply CONV.
eauto.
eauto.
eauto.

assert (Delta |--  [Sext Sid N] : Delta,,A) as HX3.
eapply SEXT.
eauto.
eauto.
eauto.


unfold Ssing.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eauto.

eapply EQ_SB_CONG_SEXT.
eauto.
eauto.
eapply EQ_REFL.
eapply CONV.
eauto.
eauto.
eauto.
Qed.


(******************************************************************************
 *)
Lemma Rel_Fundamental_App: forall Gamma Delta M N SB A B dt DT DT' (HDT : DT === DT' #in# PerType) denv,
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
      EvalTm (TmFun A B) denv DT ->
      EvalTm M denv dt ->
      Delta ||- TmSb M SB : TmSb (TmFun A B) SB #aeq# dt #in# HDT
  ) ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
       Delta ||-  [SB]:Gamma #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
       EvalTm A denv DT ->
       EvalTm N denv dt -> Delta ||- TmSb N SB : TmSb A SB #aeq# dt #in# HDT
  ) ->
  ( forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
       Delta ||-  [SB]:Gamma,, A #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
       EvalTm B denv DT -> Delta ||- TmSb B SB #in# HDT
  ) ->
  Gamma |-- M : TmFun A B ->
  Gamma |-- N : A ->
  EvalTm (TmSb B (Ssing N)) denv DT ->
  EvalTm (TmApp M N) denv dt ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
   Delta ||- TmSb (TmApp M N) SB : TmSb (TmSb B (Ssing N)) SB #aeq# dt
   #in# HDT
.
Proof.

do 14 intro.
intro Hnew.
intros.
 
clear_inversion H3.
clear_inversion H4.
clear_inversion H8.
clear_inversion H6.
compute_sth; renamer.

specialize (H _ _ _ H5).
specialize (H0 _ _ _ H5).

assert ([envs0] === [envs0] #in# Gamma) as HX1.
eapply RelSb_valenv.
eauto.

assert (Gamma |= M : TmFun A B) as HX2.
apply Valid_for_WtTm; auto.

clear_inversion HX2.

specialize (H4 _ _ HX1).
destruct H4 as [dtm0 [dtm1 [PF [H4' [H4'' [H4''' H4'''']]]]]].
destruct H4' as [df [HY1 HY2]].
clear_inversion HY1.
compute_sth; renamer.
(* checkpoit A *)

clear_inversion H3.
specialize (H15 _ _ HX1).
destruct H15 as [DF [DF' [HZ1 [HZ2 HZ3]]]].
(* checkpoint B *)

elim_deters.
clear HZ2.

specialize (H _ _ HZ3).
specialize (H DM_envs0).
specialize (H HZ1 H7).

assert (Gamma |= N : A) as HX3.
apply Valid_for_WtTm; auto.
clear_inversion HX3.
(* checkpoint C *)

specialize (H15 _ _ HX1).
destruct H15 as [dn0 [dn1 [PA [H6' [H6'' [H6''' H6'''']]]]]].

clear_inversion H3.
specialize (H16 _ _ HX1).
destruct H16 as [DA_ [DB_ [H15' [H15'' H15''']]]].
(* checkpoint D *)

elim_deters; clear_dups.

clear_inversion HZ1.

assert (DA = DA_envs0) as HQ1 by eauto.
subst. clear_dups.

(* checkpoint E *)

clear_inversion H6'.
clear_inversion H3.
elim_deters; clear_dups.

specialize (H0 _ _ H15''').
specialize (H0 _ H12 H9).

rename H0 into HREL_N.
rename H into HREL_M.
rename H15''' into HDA.
rename HZ3 into HDF.

assert (Delta |-- TmSb (TmFun A B) SB === TmFun (TmSb A SB) (TmSb B (Sext (Sseq SB Sup) TmVar))) as HX4.
eapply EQ_TP_SB_FUN.
eapply RelSb_wellformed.
eauto.
eauto.
assert (Gamma |-- A /\ Gamma ,, A |-- B) as HY1.
{
edestruct Inversion_FUN_F; eauto.
}
clear_inversion HY1.
eauto.

(* checkpoint F *)

assert (Delta ||- TmSb M SB : TmFun (TmSb A SB) (TmSb B (Sext (Sseq SB Sup) TmVar))  #aeq# DM_envs0 #in# HDF) as HX5.
eapply RelTerm_ClosedForConversion.
eauto.
eauto.
eauto.
eapply EQ_REFL.
eapply RelTerm_implies_WtTm.
eauto.

(* pushing sb down *)

assert (Delta |-- [SB] : Gamma) as HX7.
eapply RelSb_wellformed.
eauto.

assert (Delta |-- TmSb (TmApp M N) SB === TmApp (TmSb M SB) (TmSb N SB) : TmSb (TmSb B (Ssing N)) SB) as HX6.
eapply EQ_CONV.
eapply EQ_SBAPP.
eauto.
eauto.
eauto.
eapply EQ_TP_SYM.
eapply Ssing_eq; auto.
eauto.
eauto.
assert (Gamma |-- A /\ Gamma,,A |-- B) as HY1.
{
eapply Inversion_FUN_F.
eauto.
}
clear_inversion HY1.
eauto.

cut (Delta ||- TmApp (TmSb M SB) (TmSb N SB) : TmSb (TmSb B (Ssing N)) SB #aeq# dt #in# HDT).
intro.

eapply RelTerm_ClosedForConversion.
eauto.
eauto.
eauto.
auto.

(***)
auto.

generalize dependent HDF.
generalize (eq_refl (Dfun DA_envs0 (Dclo B envs0))).
generalize (eq_refl (Dfun DA_envs0 (Dclo B envs0))).

generalize (Dfun DA_envs0 (Dclo B envs0)) at 1 5 7 9 as DF.
generalize (Dfun DA_envs0 (Dclo B envs0)) at 2 4 5 6 as DF'.

intros.

(**) 
 
induction HDF using PerType_dind; intros;
try solve [clear_inversion H].
clear_inversion H.
clear_inversion H0.
clear IHHDF.
clear H3.
simpl in HX5.

destruct HX5 as [HC1 [XA [XF [PT [HC2 [HC3 [HC4 [HC5] ] ] ] ] ] ] ].

specialize (H Delta 0).
specialize (H (TmSb N SB)).
specialize (H DN_envs0).

destruct (e DN_envs0) as [DB HDB].
find_extdeters.
apply H0.
apply H6''''.

specialize (H DB DB).
specialize (H PA H16 H6'''').
specialize (H HDB HDB).

assert (CxtShift Delta 0 Delta) as HCxt by eauto.
specialize (H HCxt).

(**)

rename i into HDA'.
rename i1 into HDT'.

(**)

(*** CRAZY ***)

assert (Delta |-- TmSb A SB === XA) as HXX1.
eapply Helper_Eq'.
eauto.
eauto.

(**)

move HREL_N at bottom.

assert (Delta ||- TmSb N SB : TmSb A SB #aeq# DN_envs0 #in# HDA') as HXX2.
eapply RelTerm_ProofIrrelevance; eauto.

assert (Delta ||- TmSb N SB : TmSb XA (Sups 0) #aeq# DN_envs0 #in# HDA') as HXX3.
eapply RelTerm_ClosedForConversion with (T := TmSb A SB) (T' := TmSb XA (Sups 0)).
eauto.
eapply EQ_TP_TRANS.
apply HXX1.
unfold Sups.
apply EQ_TP_SYM.
eapply EQ_TP_SBID.
eauto.
eauto.
eapply EQ_REFL.
eauto.

(* ! *)

specialize (H HXX3).

destruct H as [dy].
destruct H as [HYY1].
unfold Sups in H.

(**)

generalize dependent DB.
do 2 intro.
inversion HDB.
subst.
assert (DB = DB_envs0_DN_envs0) as HXX4.
eauto.
subst.
clear_dups.
intros.

(**)

assert (dy = dt) as HXX4.
eapply App_deter.
eauto.
eauto.
subst.
clear_dups.

(**)

assert (Delta ||- TmApp (TmSb (TmSb M SB) Sid) (TmSb N SB) : TmSb XF (Sext Sid (TmSb N SB)) #aeq# dt #in# HDT) as HXX4.
eapply RelTerm_ClosedUnderPer''.
eauto.

(* new ---------------------------------*)

assert (Delta |-- [Sext SB (TmSb N SB)] : Gamma,,A ) as HXX5.
eapply SEXT.
eauto.
eauto.
eauto.

assert (Delta ||- [Sext SB (TmSb N SB)] : Gamma,,A #aeq# (Dext envs0 DN_envs0)) as HXX6.
econstructor 2.
eauto.
eauto.
eauto.
eapply EQ_SB_REFL.
eauto.
eauto.
eauto.
eauto.
eauto.


move Hnew at bottom.
specialize (Hnew _ _ _ HXX6).
specialize (Hnew _ _ HDT H11).

assert (Delta ||- TmSb XF (Sext Sid (TmSb N SB)) #in# HDT) as HXX7.
eapply RelTerm_implies_RelType.
eauto.

assert (Delta |-- TmSb B (Sext SB (TmSb N SB)) === TmSb XF (Sext Sid (TmSb N SB))) as HXX8.
eapply Helper_Eq''.
eauto.
eauto.


assert (Gamma |-- A /\ Gamma,,A |-- B) as HXX9.
{
eapply Inversion_FUN_F; eauto.
}

assert (Gamma |-- TmFun A B) as HYY1 by eauto.


assert (Gamma |-- A) as HXXX9.
eauto.

destruct HXX9.

eapply RelTerm_ClosedForConversion.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SYM.
eapply HXX8.

eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
unfold Ssing.
eapply SEXT.
eauto.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_SB.
unfold Ssing.

eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eapply SEXT.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SEXT.
eapply EQ_SB_SIDL.
eauto.
eauto.
eapply EQ_REFL.
eapply CONV.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
eapply EQ_SB_SIDR.
eauto.
eapply EQ_SB_SYM.
eapply EQ_SB_SIDL.
eauto.
eapply EQ_TP_REFL.
eauto.

eapply EQ_SB_REFL.
eauto.

eapply EQ_TP_REFL.
eauto.

eauto.

eapply EQ_CONG_FUN_E.
eapply EQ_SBID.
eauto.

eapply EQ_REFL.
eauto.
Qed.

(******************************************************************************
 *)

Lemma Rel_Fundamental_Abs: forall Gamma Delta A B M SB dt DT DT' (HDT: DT === DT' #in# PerType) denv,
  Gamma,, A |-- M : B ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
      EvalTm A denv DT -> Delta ||- TmSb A SB #in# HDT
  ) ->
  ( forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma,, A #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
      EvalTm B denv DT ->
      EvalTm M denv dt -> Delta ||- TmSb M SB : TmSb B SB #aeq# dt #in# HDT
  ) ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm (TmFun A B) denv DT ->
  EvalTm (TmAbs A M) denv dt ->
  Delta ||- TmSb (TmAbs A M) SB : TmSb (TmFun A B) SB #aeq# dt #in# HDT
.
Proof.

intros.
rename H0 into Hnew.
rename H1 into H0.
rename H2 into H1.
rename H3 into H2.
rename H4 into H3.

(* destroying HDT *)

move HDT at bottom.
remember DT as DTorg in H2.



(****)

clear_inversion H2.
clear_inversion H3.
renamer.

(**)

assert (Gamma,,A |--) as HX1.
eauto.

(**)

assert (Delta |-- [SB] : Gamma) as HX2.
eapply RelSb_wellformed.
eauto.

(**)

assert (Delta |-- TmSb A SB) as HX3.
eapply SB_F.
eauto.
clear_inversion HX1.
auto.

(**)

assert (Delta,, TmSb A SB  |-- [Sext (Sseq SB Sup) TmVar] : Gamma,,A ) as HX4.
eapply SEXT.
eapply SSEQ.
eapply SUP.
auto.
auto.
clear_inversion HX1.
auto.

eapply CONV.
eapply HYP.
apply EXT.
eauto.
auto.

(* eq *)
eapply EQ_TP_SBSEQ.
eapply SUP.
auto.
apply HX2.
clear_inversion HX1.
auto.

(**)

assert (Delta |-- TmSb (TmFun A B) SB === TmFun (TmSb A SB) (TmSb B (Sext (Sseq SB Sup) TmVar))) as HX10.
eapply EQ_TP_SB_FUN.
eauto.
clear_inversion HX1.
auto.
eauto.

assert (Delta |-- TmSb (TmAbs A M) SB === TmAbs (TmSb A SB) (TmSb M (Sext (Sseq SB Sup) TmVar)) : TmFun (TmSb A SB) (TmSb B (Sext (Sseq SB Sup) TmVar))) as HX11.
eapply EQ_SBABS.
eauto.
auto.

(* NEW *****)

dependent inversion HDT. subst.

renamer.
rename i into HDA.
rename i0 into HPA.
rename i1 into HAPPDB.
rename e into Hclo1.
rename e0 into Hclo2.


assert (exists PT, InterpType (Dfun DA_denv (Dclo B denv)) PT) as HX20.
eauto.


simpl.

(**)

split.
eapply System.SB.
eauto.
eapply FUN_I.

assert (Gamma ,, A |--) as HYY1.
eauto.
clear_inversion HYY1.
auto.
auto.

(**)

exists (TmSb A SB).
exists (TmSb B (Sext (Sseq SB Sup) TmVar)).
destruct HX20 as [PT HPT].
exists PT.
split.

(**)

auto.
split.

(**)

clear_inversion HPT.
constructor 1.
intros.

assert (Gamma,,A |= M : B) as HXX21.
apply Valid_for_WtTm; auto.

clear_inversion HXX21.

assert ( [Dext denv a0] === [Dext denv a1] #in# Gamma,,A) as HYY1.
econstructor 2.
eapply RelSb_valenv.
eauto.
red.
exists DA_denv.
split.
auto.
eauto.
auto.

specialize (H7 _ _ HYY1).
destruct H7 as [dtm0 [dtm1 [PB HYY2] ] ].
exists dtm0. exists dtm1.

destruct HYY2.
destruct H10.
destruct H11.


clear_inversion H5.
clear_inversion po_ro.

destruct H7 as [DBa0 HDBa0].
destruct HDBa0.
renamer.

assert (a0 === a0 #in# PDA_denv0) as HYY3.
eauto.

destruct ro_resp_ex with a0 as [Y].
auto.

exists Y.
 
split.
auto.

(**)


repeat (split; auto).

assert (InterpType DB_denv_a0 Y) as HYY4.
eapply H6.
eauto.
eauto.
auto.

assert (PDB_denv_a0 =~= Y) as HYY5.
eauto.
apply HYY5.
eauto.

(**)

split.
auto.
split.
auto.
(**)

eauto. 

(**)

intros.
renamer.

assert (Gamma,,A |= M : B) as HYY1.
apply Valid_for_WtTm; auto.

clear_inversion HYY1.

assert ( [Dext denv da] === [Dext denv da] #in# Gamma,,A) as HYY2.
econstructor 2.
eapply RelSb_valenv.
eauto.
red.
exists DA_denv.
split.
auto.
eauto.
auto.

specialize (H5 _ _ HYY2).
destruct H5 as [dtm0 [dtm1 [PB [HiDB [Hdtm0 [Hdtm1 HdtmPA ] ] ] ] ] ].
exists dtm0.

split.
eauto.


assert (Delta ||- TmSb A SB #in# HDA) as HYY3.
eauto.

assert (Delta0 ||- [Sseq SB (Sups i)] : Gamma #aeq# denv) as HYY4.
eapply RelSb_ClosedUnderLift.
eauto.
eauto.

assert (Gamma |-- A) as HYY5.
assert (Gamma,,A |-- ) by eauto.
clear_inversion H5.
auto.

assert (Delta0 |-- TmSb (TmSb A SB) (Sups i) === TmSb A (Sseq SB (Sups i))) as HYY6.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

assert (Delta0 ||- a : TmSb A (Sseq SB (Sups i)) #aeq# da #in# HDA) as HYY7.
eapply RelTerm_ClosedForConversion.
eauto.
eauto.
eauto.
eapply EQ_REFL.
eapply RelTerm_implies_WtTm.
eauto.

assert (Delta0 ||- [Sext (Sseq SB (Sups i)) a] : Gamma,,A #aeq# Dext denv da) as HYY8.
econstructor 2.
eauto.
eauto.
eauto.
eapply EQ_SB_REFL.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply RelSb_valenv.
eauto.
eauto.
eauto.
eauto.

move H0 at bottom.
specialize (H0 Delta0 (Sext (Sseq SB (Sups i)) a) (Dext denv da)).
specialize (H0 HYY8).
specialize (H0 _ _ (  HAPPDB da da DB DB' PDA_denv0 HPA0 Hda Happ Happ' )).
specialize (H0 dtm0).

assert (EvalTm B (Dext denv da) DB) as HYY9.
inversion Happ.
auto.

specialize (H0 HYY9 Hdtm0).


assert (Delta0 |-- TmSb (TmSb B (Sext (Sseq SB Sup) TmVar)) (Sext (Sups i) a) === TmSb B (Sext (Sseq SB (Sups i)) a)) as HYY10.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.
eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply RelType_implies_WfTp.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.

eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eapply SEXT.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.
eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply RelType_implies_WfTp.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

assert ( Delta0 |-- a : TmSb (TmSb A SB) (Sups i)) as HZZZ0.
eapply RelTerm_implies_WtTm.
eauto.

assert (Delta0 |--  [Sext (Sups i) a]: Delta,,TmSb A SB) as HZZZ1.
eapply SEXT.
eauto.
eauto.
auto.



assert (Delta0 |-- [Sseq (Sseq SB Sup) (Sext (Sups i) a)] === [Sseq SB (Sups i)] : Gamma ) as HZZ1.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eauto.

(**)
eapply EQ_SB_CONG_SEXT.
apply HZZ1.
eauto.
(**)

assert (Delta0 |-- TmSb TmVar (Sext (Sups i) a) === a
   : TmSb A (Sseq (Sseq SB Sup) (Sext (Sups i) a))) as HZZZ2.

eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
apply HZZ1.
eauto.
eauto.
eauto.

assert (Delta0 |-- TmApp (TmSb (TmSb (TmAbs A M) SB) (Sups i)) a ===  TmSb M (Sext (Sseq SB (Sups i)) a) : TmSb (TmSb B (Sext (Sseq SB Sup) TmVar)) (Sext (Sups i) a)) as HYY11.

assert ( Delta0 |-- a : TmSb (TmSb A SB) (Sups i)) as HZZZ0.
eapply RelTerm_implies_WtTm.
eauto.

assert (Delta0 |--  [Sext (Sups i) a]: Delta,,TmSb A SB) as HZZZ1.
eapply SEXT.
eauto.
eauto.
auto.


assert (Delta0 |-- a : TmSb (TmSb A (Sseq SB (Sups i))) Sid) as HZZ1.
eapply CONV.
eapply RelTerm_implies_WtTm.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBID.
eapply RelType_implies_WfTp with (T := TmSb A (Sseq SB (Sups i)) ).
eapply RelTerm_implies_RelType.
eauto. 



assert ( Delta0,, TmSb A (Sseq SB (Sups i)) |-- TmVar
   : TmSb A (Sseq (Sseq SB (Sups i)) Sup)) as HZZ2.

eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply RelType_implies_WfTp with (T := TmSb A (Sseq SB (Sups i)) ).
eapply RelTerm_implies_RelType.

eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

assert (Delta0 |-- a : TmSb A (Sseq SB (Sups i))) as HZZ3.
eapply RelTerm_implies_WtTm.
eauto.



assert (Delta0 |-- [Sseq (Sseq SB Sup) (Sext (Sups i) a)] === [Sseq SB (Sups i)] : Gamma ) as HZZ4.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eauto.
eauto.
eauto.

assert (Delta0 |--  [Sext Sid a]: Delta0,, TmSb A (Sseq SB (Sups i))) as HZZZ4.
eapply SEXT.
eauto.
eauto.
auto.

assert (Delta0 |--  [Sseq (Sseq (Sseq SB (Sups i)) Sup) (Sext Sid a)]===
   [Sseq SB (Sups i)]:Gamma) as HZZ5.
eapply EQ_SB_TRANS.
eapply EQ_SB_COMM.
eauto.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SSEQ.
eapply EQ_SB_SUP.
eauto.
eapply RelType_implies_WfTp with (T := TmSb A (Sseq SB (Sups i)) ).
eapply RelTerm_implies_RelType.
eauto.
auto.
eapply EQ_SB_REFL.
eauto.
eapply EQ_SB_TRANS.
eapply EQ_SB_SIDR.
eauto.
eapply EQ_SB_REFL.
eauto.

(*
assert (Delta0 |-- TmSb TmVar (Sext (Sups i) a) === a
   : TmSb A (Sseq (Sseq SB Sup) (Sext (Sups i) a))) as HZZZ2.

eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eapply RelTerm_implies_WtTm.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_CONG_SB.
eapply EQ_SB_SYM.
apply HZZ1.
eauto.
eauto.
*)



assert (
  Delta0
   |-- TmSb B (Sseq (Sext (Sseq (Sseq SB (Sups i)) Sup) TmVar) (Sext Sid a)) ===
   TmSb (TmSb B (Sext (Sseq (Sseq SB (Sups i)) Sup) TmVar)) (Sext Sid a)
) as HZZ6.
eapply EQ_TP_SYM.
eapply EQ_TP_SBSEQ.
eauto.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.
auto.
eauto.


assert (Delta,, TmSb A SB |-- TmVar : TmSb A (Sseq SB Sup)) as HZZ7.
eapply CONV.
eapply HYP.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.


assert(Delta0 |-- TmSb TmVar (Sext Sid a) === a :TmSb (TmSb A (Sseq SB (Sups i))) Sid) as HZZZ5.
eapply EQ_SBVAR.
eauto.
eapply RelType_implies_WfTp with (T := TmSb A (Sseq SB (Sups i)) ).
eapply RelTerm_implies_RelType.
eauto.
auto.

assert ( Delta0 |-- TmSb TmVar (Sext Sid a) === a
   : TmSb A (Sseq (Sseq (Sseq SB (Sups i)) Sup) (Sext Sid a))) as HZZZ6.

eapply EQ_CONV.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBID.
eapply RelType_implies_WfTp with (T := TmSb A (Sseq SB (Sups i)) ).
eapply RelTerm_implies_RelType.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_CONG_SB.
eauto.
eauto.

(**)

eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_CONG_FUN_E.
eapply EQ_CONV.
eapply EQ_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_TP_SB_FUN.
eapply SSEQ.
eauto.
eauto.
eauto.
eauto.
eapply EQ_REFL.
auto.

eapply EQ_TRANS.
eapply EQ_CONG_FUN_E.
eapply EQ_SBABS.
eauto.
eauto.
eapply EQ_REFL.
eauto.

eapply EQ_TRANS.
eapply EQ_FUN_Beta.
eapply System.SB.
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.

auto.

eauto.


eauto.

eapply EQ_TRANS.
eapply EQ_SBSEQ.
unfold Ssing.
eauto.

auto.


eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.
auto. (*HZZ2*)
auto.

eapply EQ_CONV.
eapply EQ_CONG_SB.

unfold Ssing.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.

eapply SEXT.
eapply SSEQ.
eauto.
eapply SSEQ.
eauto.
eauto.
eauto.
auto.
  
eapply EQ_SB_CONG_SEXT.
auto.
eauto.

eauto.
eauto.

unfold Ssing.
auto.
unfold Ssing.

eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eapply SEXT.
eauto.
eauto.
auto.
eauto.


eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.

eapply EQ_TP_CONG_SB.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eauto.

eapply EQ_SB_TRANS.
eapply EQ_SB_CONG_SEXT.
eauto.
eauto.


assert (Delta0 |-- TmSb TmVar (Sext (Sups i) a) === a
   : TmSb A (Sseq (Sseq SB Sup) (Sext (Sups i) a))) as HZZZ7.
eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eauto.
eauto.

eauto.

eapply EQ_SB_SYM.
eapply EQ_SB_TRANS.
eapply EQ_SB_SEXT.
eauto.
eapply SEXT.
eauto.
eauto.
auto.

eapply EQ_SB_CONG_SEXT.
eapply EQ_SB_TRANS.
eauto.
eauto.
eauto.

assert (Delta0 |-- TmSb TmVar (Sext (Sups i) a) === a
   : TmSb A (Sseq (Sseq SB Sup) (Sext (Sups i) a))) as HZZZ7.
eapply EQ_CONV.
eapply EQ_SBVAR.
eauto.
eauto.
eauto.
eauto.

eauto.
eauto.

(***)

eapply RelTerm_ClosedForConversion.
eauto.
eauto.
eauto.
eauto.

Qed.



