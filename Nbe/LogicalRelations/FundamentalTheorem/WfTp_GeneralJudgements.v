(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)
Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.


(******************************************************************************
 *)
Lemma Fundamental_SB_F: forall Gamma Delta A S SB denv DT DT' (HDT : DT === DT' #in# PerType) Delta0,
  Gamma |--  [S]:Delta ->
  (forall (Delta0 : Cxt) (SB : Sb) (denv : DEnv),
      Delta0 ||-  [SB]:Gamma #aeq# denv ->
      forall dsb : DEnv,
      EvalSb S denv dsb -> Delta0 ||-  [Sseq S SB]:Delta #aeq# dsb) ->
  Delta |-- A ->
  (forall (Delta0 : Cxt) (SB : Sb) (denv : DEnv),
       Delta0 ||-  [SB]:Delta #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType),
       EvalTm A denv DT -> Delta0 ||- TmSb A SB #in# HDT) ->
  Delta0 ||-  [SB]:Gamma #aeq# denv -> 
  EvalTm (TmSb A S) denv DT ->
Delta0 ||- TmSb (TmSb A S) SB #in# HDT
.
Proof.
intros.
assert (Delta0 |-- [SB] : Gamma) as HX1 by (eapply RelSb_wellformed; eauto).

clear_inversion H4.

eapply RelType_ClosedForConversion.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
Qed.
Hint Immediate Fundamental_SB_F.

