(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.Lift.

(******************************************************************************
 *)
Lemma Fundamental_Tm1: forall Gamma Delta SB denv DT DT' (HDT : DT === DT' #in# PerType) dt,
  Gamma |-- ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm TmUnit denv DT ->
  EvalTm Tm1 denv dt ->
  Delta ||- TmSb Tm1 SB : TmSb TmUnit SB #aeq# dt #in# HDT
.
Proof.
intros.

assert (Delta |-- [SB] : Gamma) by (eapply RelSb_wellformed; eauto).
destruct HDT; try solve [clear_inversion H1].
simpl.
repeat (split; auto).
eauto.

intros.
exists Tm1.
split; auto.
unfold Ddown; simpl.
auto.

eapply EQ_Unit_Eta.
eapply CONV.
eapply Syntax.System.SB.
eauto.
eapply Syntax.System.SB.
eauto.
eauto.

eapply EQ_TP_TRANS.
eapply EQ_TP_CONG_SB.
eauto.
eapply EQ_TP_SB_UNIT.
eauto.
eapply EQ_TP_SB_UNIT.
eauto.
Qed.
Hint Immediate Fundamental_Tm1.

