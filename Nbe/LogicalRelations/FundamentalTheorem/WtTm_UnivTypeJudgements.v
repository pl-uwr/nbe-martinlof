(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.
Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.
Require Import Nbe.LogicalRelations.ConvertibleToReification.
Require Import Nbe.LogicalRelations.ClosedUnderPer.
Require Import Nbe.LogicalRelations.Lift.




Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_GeneralJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_GeneralJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtSb_GeneralJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_NatTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_NatTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_FunTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_FunTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_EmptyTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_EmptyTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WtTm_UnitTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_UnitTypeJudgements.
Require Import Nbe.LogicalRelations.FundamentalTheorem.WfTp_UnivTypeJudgements.


(******************************************************************************
 *)
Lemma Fundamental_UNIV_NAT_F: forall Gamma Delta SB denv DT DT' dt (HDT: DT === DT' #in# PerType),
  Gamma |-- ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm TmUniv denv DT ->
  EvalTm TmNat denv dt ->
  Delta ||- TmSb TmNat SB : TmSb TmUniv SB #aeq# dt #in# HDT
.
Proof.
intros.

induction HDT using PerType_dind; try solve
  [ clear_inversion H1 ].

clear_inversion H2.

simpl.



assert (Delta |-- [SB]:Gamma) as HX1.
{
eapply RelSb_wellformed; eauto.
}

assert (Dnat === Dnat #in# PerUniv) as HDU by eauto.


repeat split.

eauto.
eauto.

{
intros.
exists TmNat.
rewrite Ddown_Duniv.
split.
eauto.
eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SBSEQ.
eauto.
eauto.
eauto.
eapply EQ_CONV.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eauto.
}
{
exists HDU; eauto.
}
Qed.
Hint Immediate Fundamental_UNIV_NAT_F.


(******************************************************************************
 *)
Lemma Fundamental_UNIV_EMPTY_F: forall Gamma Delta SB denv DT DT' dt (HDT: DT === DT' #in# PerType),
  Gamma |-- ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm TmUniv denv DT ->
  EvalTm TmEmpty denv dt ->
  Delta ||- TmSb TmEmpty SB : TmSb TmUniv SB #aeq# dt #in# HDT
.
Proof.
intros.

induction HDT using PerType_dind; try solve
  [ clear_inversion H1 ].

clear_inversion H2.

simpl.



assert (Delta |-- [SB]:Gamma) as HX1.
{
eapply RelSb_wellformed; eauto.
}

assert (Dempty === Dempty #in# PerUniv) as HDU by eauto.


repeat split.

eauto.
eauto.

{
intros.
exists TmEmpty.
rewrite Ddown_Duniv.
split; eauto.
eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SBSEQ; eauto.
eapply EQ_CONV.
eapply EQ_UNIV_SB_EMPTY.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eauto.
}


exists HDU; eauto.
Qed.
Hint Immediate Fundamental_UNIV_EMPTY_F.

(******************************************************************************
 *)
Lemma Fundamental_UNIV_UNIT_F: forall Gamma Delta SB denv DT DT' dt (HDT: DT === DT' #in# PerType),
  Gamma |-- ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm TmUniv denv DT ->
  EvalTm TmUnit denv dt ->
  Delta ||- TmSb TmUnit SB : TmSb TmUniv SB #aeq# dt #in# HDT
.
Proof.
intros.

induction HDT using PerType_dind; try solve
  [ clear_inversion H1 ].

clear_inversion H2.

simpl.



assert (Delta |-- [SB]:Gamma) as HX1.
{
eapply RelSb_wellformed; eauto.
}

assert (Dunit === Dunit #in# PerUniv) as HDU by eauto.


repeat split.

eauto.
eauto.

{
intros.
exists TmUnit.
split; auto.

eapply EQ_CONV.
eapply EQ_TRANS.
eapply EQ_SBSEQ; eauto.
eapply EQ_CONV.
eapply EQ_UNIV_SB_UNIT.
eauto.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eauto.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ; eauto.
eauto.
}

exists HDU; eauto.
Qed.
Hint Immediate Fundamental_UNIV_UNIT_F.

(******************************************************************************
 *)
Lemma Fundamental_UNIV_ELEM_F: forall T Gamma Delta SB denv DT DT'
  (HDT : DT === DT' #in# PerType),
  Gamma |-- T : TmUniv ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
      EvalTm TmUniv denv DT ->
      EvalTm T denv dt ->
      Delta ||- TmSb T SB : TmSb TmUniv SB #aeq# dt #in# HDT) ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm T denv DT ->
  Delta ||- TmSb T SB #in# HDT
.
Proof.
intros.

assert (Duniv === Duniv #in# PerType) as HDuniv by eauto.

specialize (H0 _ _ _ H1).
specialize (H0 _ _ HDuniv).
specialize (H0 DT (evalUniv denv) H2).

generalize dependent Gamma.
generalize dependent T.
generalize dependent Delta.
generalize dependent SB.
generalize dependent denv.
generalize dependent DT.
generalize dependent DT'.

generalize dependent HDuniv.
cut (Duniv = Duniv); auto.
cut (Duniv = Duniv); auto.

generalize Duniv at 1 5 7 as d1.
generalize Duniv at 2 4 5 as d2.
do 5 intro.
induction HDuniv using PerType_dind; try solve [clear_inversion H0]; intros.

simpl in H2.
decompose record H2.
eapply RelType_ClosedUnderPer.
eapply H9.
Qed.
Hint Immediate Fundamental_UNIV_ELEM_F.


(******************************************************************************
 *)
Lemma Fundamental_UNIV_FUN_F: forall Gamma A B Delta SB denv DT DT' dt
  (HDT : DT === DT' #in# PerType),
  Gamma |-- A : TmUniv ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
      Delta ||-  [SB]:Gamma #aeq# denv ->
      forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
      EvalTm TmUniv denv DT ->
      EvalTm A denv dt ->
      Delta ||- TmSb A SB : TmSb TmUniv SB #aeq# dt #in# HDT) ->
  (Gamma,, A |-- B : TmUniv) ->
  (forall (Delta : Cxt) (SB : Sb) (denv : DEnv),
       Delta ||-  [SB]:Gamma,, A #aeq# denv ->
       forall (DT DT' : D) (HDT : DT === DT' #in# PerType) (dt : D),
       EvalTm TmUniv denv DT ->
       EvalTm B denv dt ->
       Delta ||- TmSb B SB : TmSb TmUniv SB #aeq# dt #in# HDT) ->
  Delta ||-  [SB]:Gamma #aeq# denv ->
  EvalTm TmUniv denv DT ->
  EvalTm (TmFun A B) denv dt ->
  Delta ||- TmSb (TmFun A B) SB : TmSb TmUniv SB #aeq# dt #in# HDT
.
Proof.
 
intros.
 
induction HDT using PerType_dind; try solve
  [ clear_inversion H4 ].

simpl.

assert (Delta |-- [SB]:Gamma) as HX1.
{
eapply RelSb_wellformed; eauto.
}


assert (Duniv === Duniv #in# PerType) as HDuniv by eauto.



split; [ eauto | ].
split; [ eauto | ].
split.

+{
intros.
clear_inversion H5.

rename Delta0 into Psi.



set (SBshift := Sups i0).

assert (Psi |-- TmSb (TmSb TmUniv SB) SBshift ===  TmSb TmUniv (Sseq SB SBshift)) as HY1.
{
eauto.
}

assert (Psi |-- TmSb (TmSb A SB) SBshift === TmSb A (Sseq SB SBshift) : TmSb TmUniv (Sseq SB SBshift)) as HY2.
{
eauto.
}

assert (Psi |-- TmSb (TmSb A SB) SBshift === TmSb A (Sseq SB SBshift) : TmUniv) as HY3.
{
eauto.
}

assert (Psi |-- TmSb (TmSb A SB) SBshift === TmSb A (Sseq SB SBshift)) as HY4.
{
eauto.
}

assert (Psi |-- [Sseq SB (Sups i0)] : Gamma) as HZ1.
{
eapply SSEQ; eauto.
}

assert (Psi ||- [Sseq SB (Sups i0)] : Gamma #aeq# denv) as HZ2.
{
eapply RelSb_ClosedUnderLift; eauto.
}


assert (Psi ||- TmSb A (Sseq SB SBshift) : TmSb TmUniv (Sseq SB SBshift) #aeq# DA #in# HDuniv) as HXX0.
{
specialize (H0 _ _ _ HZ2).
specialize (H0 _ _ HDuniv).
specialize (H0 DA).
specialize (H0 H4 H12).
auto.
}

assert (DA === DA #in# PerType) as HDA.
{
eapply i.
eapply RelTerm_resp_InterpType.
eauto.
auto.
}

assert (Psi ||- TmSb A (Sseq SB SBshift) #in# HDA) as HXX2.
{
eapply Fundamental_UNIV_ELEM_F; eauto.
}

assert (Psi,, TmSb A (Sseq SB SBshift) |-- [Sext (Sseq (Sseq SB SBshift) Sup) TmVar] : Gamma,,A) as HXX3.
{
eapply SEXT.
eapply SSEQ.
eauto.
eauto.
eauto.
eapply CONV.
eapply HYP.
eapply EXT.
eauto.
eapply SB_F.
eauto.
eauto.

eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
}

assert (Psi,, TmSb A (Sseq SB SBshift) ||- TmSb A (Sseq (Sseq SB SBshift) Sup) #in# HDA) as HXX4.
{
eapply RelType_ClosedForConversion.
eapply RelTypeLift1.
eauto.
eauto.
eapply EQ_TP_SBSEQ; eauto.
}

assert (exists PA, InterpType DA PA) as HX5 by eauto.
destruct HX5 as [PA HX5].

assert (Dup DA (Dvar (length Psi)) === Dup DA (Dvar (length Psi)) #in# PA) as HXX6.
{
eapply Reflect_Characterization; eauto.
}

assert ([denv] === [denv] #in# Gamma) as HX7.
{
eapply RelSb_valenv.
eapply H3.
} 

assert ([Dext denv (Dup DA (Dvar (length Psi)))] === [Dext denv (Dup DA (Dvar (length Psi)))] #in# (Gamma,, A )) as HXX8.
{ 
econstructor 2.
eauto.
red.
exists DA.
eauto.
eauto.
}

assert (Psi,, TmSb A (Sseq SB SBshift) ||- TmVar : TmSb A (Sseq (Sseq SB SBshift) Sup) #aeq# Dup DA (Dvar (length Psi)) #in# HDA) as HXX9.
{
eapply RelTerm_Back.
eauto.
eauto.
intros.
eexists.
split.
eauto.
eapply EQ_CONV.
eapply CxtShift_TmVarSups.
eauto.
eauto.
eapply EQ_TP_TRANS.
simpl.
eapply Sups_weird'; eauto.

eapply EQ_TP_CONG_SB.
eauto.
eapply EQ_TP_SBSEQ.
eauto.
eauto.
eauto.
}

assert (Psi,, TmSb A (Sseq SB SBshift) ||- [Sext (Sseq (Sseq SB SBshift) Sup) TmVar] : Gamma,,A #aeq# Dext denv (Dup DA (Dvar (length Psi)))) as HXX10.
{
econstructor 2 with (M := TmVar); eauto.
eapply RelSb_lift1; eauto.
}


assert (Gamma,,A |= B : TmUniv) as HX11.
{
eapply Valid_for_WtTm.
eauto.
}

clear_inversion HX11.

specialize (H8 _ _ HXX8).
destruct H8 as [DB0' [DB1' [PB' [HB1' [HB2' [HB3' HB4']]]]]].
clear_inversion HB1'.
clear_inversion H8.
clear_inversion H9.
clear_inversion H10.
elim_deters. clear_dups.
rename DB0' into DB'.


assert (Psi,, TmSb A (Sseq SB SBshift) ||- TmSb B (Sext (Sseq (Sseq SB SBshift) Sup) TmVar) : TmSb TmUniv (Sext (Sseq (Sseq SB SBshift) Sup) TmVar) #aeq# DB' #in# HDuniv) as HXX12.
{
specialize (H2 _ _ _ HXX10).
specialize (H2 _ _ HDuniv).
specialize (H2 DB').
specialize (H2 (evalUniv _)).
specialize (H2 HB2').
auto.
}

edestruct RelTerm_Reify with (t := TmSb A (Sseq SB SBshift)) (T := TmSb TmUniv (Sseq SB SBshift)) as [NFA [HNFA1 HNFA2]]; eauto.

edestruct RelTerm_Reify with (t := TmSb B (Sext (Sseq (Sseq SB SBshift) Sup) TmVar)) as [NFB [HNFB1 HNFB2]].
eapply RelTerm_implies_RelType.
eapply HXX12.
eauto.

exists (TmFun NFA NFB).
split.
{
eapply reifyNf_fun.
rewrite Ddown_Duniv in *.
eauto.
eauto.
eauto.
}
{
eapply EQ_TRANS.
eapply EQ_CONV.
eapply EQ_SBSEQ.
eauto.
eauto.
eauto.
eauto.

eapply EQ_TRANS.
eapply EQ_UNIV_SB_FUN.
eauto.
eauto.
eauto.

eapply EQ_UNIV_CONG_FUN.
eauto.
eauto.
}
}

(****)
+{
assert (dt === dt #in# PerUniv) as HDU.
{
assert (Gamma |= TmFun A B : TmUniv) as HX2 by (eapply Valid_for_WtTm; eauto).
clear_inversion HX2.
assert ([denv] === [denv] #in# Gamma) as HX2.
{
eapply RelSb_valenv.
eapply H3.
}
destruct (H8 _ _ HX2) as [DA [DB [HX3 [HX4 [HX5 [HX6 HX7]]]]]].
clear_inversion HX4.
clear_inversion H9.
clear_inversion H10.
clear_inversion H11.
assert (DA = dt) by (eauto using EvalTm_deter).
assert (DB = dt) by (eauto using EvalTm_deter).
subst.
auto.
}
exists HDU.

eapply Rel_Fundamental_Fun.
eauto.
intros.
eapply Fundamental_UNIV_ELEM_F.
eauto.
eauto.
eauto.
eauto.
eauto.
eauto.
eauto.
eauto.
}

Qed.
Hint Immediate Fundamental_UNIV_FUN_F.
