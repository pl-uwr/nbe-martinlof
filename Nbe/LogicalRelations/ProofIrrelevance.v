(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 * Technical theorems for logical relations.
     *
 * It states that proof-term does not matter and can be exchanges (It is
 * obvious since Coq does not allow to mix logic and computations, but we
 * require lemmas to exchange those proof terms).
 *)
Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.


Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.Validity.
Require Import Nbe.SoundnessOfJudgements.DeprecatedTactics.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.ClosedUnderPer.


(******************************************************************************
 * Proof irrelevance for Logical Relations
 *
 * Shape of the proof 'DT === DT #in# PerType' is irrelevant for satisfability
 * of the relation.
 *)

Theorem Rel_ProofIrrelevance: forall Gamma T DT DT' (HDT HDT': DT === DT' #in# PerType),
  (Gamma ||- T #in# HDT ->  Gamma ||- T #in# HDT') /\
  (forall t dt, Gamma ||- t : T #aeq# dt #in# HDT -> Gamma ||- t : T #aeq# dt #in# HDT')
.
Proof.
intros.
repeat split; intros.
{
assert (DT' === DT #in# PerType) as HDT_sym by (symmetry; auto).

eapply RelType_ClosedUnderPer_sym with (HDT1 := HDT_sym).
eapply RelType_ClosedUnderPer_sym with (HDT1 := HDT).
assumption.
}
(**)
{
assert (exists PT, InterpType DT PT) as HX1 by eauto.
program_simpl.
rename HX1 into PT.

assert (dt === dt #in# PT) as HX2.
eauto.

assert (DT' === DT #in# PerType) as HDT_sym by (symmetry; auto).

eapply RelTerm_ClosedUnderPer_sym with (HDT1 := HDT_sym); eauto.
eapply RelTerm_ClosedUnderPer_sym with (HDT1 := HDT); eauto.
}
Qed.

(******************************************************************************
 * Proof irrelevance for Logical Relations (unpacked)
 *)

Remark RelType_ProofIrrelevance: forall Gamma T DT DT' (HDT HDT': DT === DT' #in# PerType),
  (Gamma ||- T #in# HDT ->  Gamma ||- T #in# HDT')
.
Proof.
intros.
edestruct Rel_ProofIrrelevance; eauto.
Qed.

Remark RelTerm_ProofIrrelevance: forall Gamma T DT DT' (HDT HDT': DT === DT' #in# PerType),
  (forall t dt, Gamma ||- t : T #aeq# dt #in# HDT -> Gamma ||- t : T #aeq# dt #in# HDT')
.
Proof.
intros.
edestruct Rel_ProofIrrelevance; eauto.
Qed.

