(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)

Set Implicit Arguments.

Require Import Arith.
Require Import Arith.Plus.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.SoundnessOfJudgements.
Require Import Nbe.CompletenessOfNbe.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.
Require Import Nbe.LogicalRelations.Lift.
Require Import Nbe.LogicalRelations.ClosedUnderConversion.
Require Import Nbe.LogicalRelations.ConvertibleToReification.
Require Import Nbe.LogicalRelations.FundamentalTheorem.

(******************************************************************************
 * Identity environments
 *)

Lemma RelSid_Var: forall DT DT' (HDT : DT === DT' #in# PerType) Gamma T denv,
  Gamma,,T |-- TmVar : TmSb T Sup ->
  Gamma,, T ||- TmSb T Sup #in# HDT ->
  EvalCxt Gamma denv ->
  EvalTm T denv DT ->
  Gamma,, T ||- TmVar : TmSb T Sup #aeq# Dup DT (Dvar (length Gamma))
    #in# HDT
.
Proof. 
intros.

eapply RelTerm_Back.
eauto.
eauto.

intros.
eexists.
split.
eauto.

assert (S (length Gamma) = length (Gamma,, T)) as HX1.
eauto.

assert (Gamma,,T |-- ) as HX2.
eauto.

assert (Gamma |-- T ) as HX3.
clear_inversion HX2.
eauto.


rewrite HX1.
eapply EQ_CONV.
eapply CxtShift_TmVarSups.
eauto.
eauto.

simpl.
eapply EQ_TP_SYM.
eapply EQ_TP_TRANS.
eapply EQ_TP_SBSEQ.
eauto.
eapply SUP.
eauto.
eauto.

eapply EQ_TP_CONG_SB; [ | eauto ].
eapply EQ_SB_SYM.
eapply Sups_tail'.
eauto.
Qed.


(******************************************************************************
 *)
Lemma RelSid: forall Gamma denv,
  Gamma |-- -> EvalCxt Gamma denv -> Gamma ||- [Sid] : Gamma #aeq# denv
.
Proof.
intros.
generalize dependent denv.
induction H; intros.

clear_inversion H0.
auto.
(**)
clear_inversion H1.
rename denv0 into denv.
specialize (IHWfCxt denv H4).

assert (Gamma,,A ||- [Sseq Sid Sup] : Gamma #aeq# denv)
  as HSidSup
  by (apply RelSb_lift1; auto) 
.

assert (Gamma,,A ||- [Sup] : Gamma #aeq# denv)
  as HSup
.
eapply RelSb_ClosedUnderEq.
eapply EQ_SB_SIDL.
auto.
apply RelSb_lift1; auto.
(**)

assert (Gamma |= A) as HvalA.
eapply Valid_for_WfTp.
eauto.

clear_inversion HvalA.
assert ([denv] === [denv] #in# Gamma) as Hval.
eauto.

specialize (H2 _ _ Hval).
destruct H2 as [DA' [DA'']].
program_simpl.
elim_deters.
clear_dups.
rename H5 into HDA.


assert (Gamma ,, A ||- TmSb A Sup #in# HDA) as HZ1.
eapply Rel_Fundamental_Tp with (Gamma := Gamma) (T := A) (Delta := (Gamma,,A)) (SB := Sup) (denv := denv); auto.

(**)

assert (exists PA, InterpType DA PA) as HPA.
eauto.
destruct HPA as [PA HPA].

apply RelSb_ClosedUnderEq with (sb1 := Sext Sup TmVar); auto.
econstructor 2.
eapply HSup. 
eauto.
eapply RelSid_Var.
eauto.
eauto.
eauto.
eauto.
eauto.
eauto.
eauto.
eauto.


eapply Reflect_Characterization.
eauto.
eauto.

eauto.

Qed.

