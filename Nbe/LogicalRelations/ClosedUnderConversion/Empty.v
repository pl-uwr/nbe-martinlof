(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

(******************************************************************************
 *)
 
Set Implicit Arguments.

Require Import Program.
Require Import Program.Tactics.
Require Import Program.Equality.
Require Import Setoid.
Require Import Relations.Relation_Definitions.
Require Import Classes.RelationClasses.

Require Import Nbe.Syntax.
Require Import Nbe.Domain.
Require Import Nbe.Utils.
Require Import Nbe.Model.
Require Import Nbe.LogicalRelations.Def.
Require Import Nbe.LogicalRelations.Inversion.

(******************************************************************************
 *)
Lemma PerType_empty_is_closed_under_conversion: forall T Gamma T',
  Gamma ||- T #in# PerType_empty ->
  Gamma |-- T === T' ->
   Gamma ||- T' #in# PerType_empty /\
   (forall (dt : D) (t t' : Tm),
    Gamma ||- t : T #aeq# dt #in# PerType_empty ->
    Gamma |-- t === t' : T -> Gamma ||- t' : T' #aeq# dt #in# PerType_empty)
.
Proof.
simpl in *.
repeat split; eauto; intros.
decompose record H1.
eauto.
decompose record H1.
edestruct H7; eauto.
exists x. program_simpl; repeat split.
assumption.
eapply EQ_TRANS; eauto using H8.
eapply EQ_CONV; eauto.
Qed.
Hint Immediate PerType_empty_is_closed_under_conversion.
