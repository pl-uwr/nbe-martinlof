(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Set Implicit Arguments.

Require Import Nbe.Syntax.

(******************************************************************************
 * Domain
 *)
Inductive D : Set :=
| Dclo      : forall (t:Tm) (denv:DEnv), D 
| DupX      : forall (dT:D) (dnf:DNe), D
| DupN      : forall (dne:DNe), D
| DS        : D -> D
| D0        : D
| Dnat      : D
| Dfun      : forall (DA:D) (DF:D), D
| Dunit     : D
| D1        : D
| Dempty    : D
| Dabsurd   : D -> D
| Duniv     : D
with DNe  : Set :=
| Dvar      : forall (n:nat), DNe
| Dapp      : forall (dne:DNe) (dnf:DNf), DNe
| DneAbsurd : forall (da:DNf) (dne:DNe), DNe
with DNf  : Set :=
| DdownX    : forall (dT:D) (da:D), DNf
| DdownN    : forall (da:D), DNf
with DEnv   : Set :=
| Did       : DEnv
| Dext      : forall (denv:DEnv) (dx:D), DEnv
.
Hint Constructors D.
Hint Constructors DNe.
Hint Constructors DNf.
Hint Constructors DEnv.

(******************************************************************************
 * Auxiliary functions for Ddown/Dup markers
 *)

Definition DisFun (d:D) :=
  match d with
  | Dfun _ _ => True
  | _        => False
  end
.

Definition DisFun_dec : forall d, {DisFun d} + {~ DisFun d}.
intro d.
case d; simpl; auto.
Defined.

Definition DisUnit (d:D) :=
  match d with
  | Dunit => True
  | _     => False
  end
.

Definition DisUnit_dec : forall d, {DisUnit d} + {~ DisUnit d}.
intro d.
case d; simpl; auto.
Defined.

(******************************************************************************
 * Markers
 *
 * Dup (Ddown) decides if information about type must be preserved and returns
 * adequate constrcutor DupN (DdownN) or DupX (DdownX)
 *)

Definition Dup (dT:D) (d:DNe) : D.
destruct DisFun_dec with dT.
exact (DupX dT d).
destruct DisUnit_dec with dT.
exact (DupX dT d).
exact (DupN d).
Defined.

Definition Ddown (dT:D) (d:D) : DNf.
destruct DisFun_dec with dT.
exact (DdownX dT d).
destruct DisUnit_dec with dT.
exact (DdownX dT d).
exact (DdownN d).
Defined.


(******************************************************************************
 * Dup/Down equalities
 *)

Fact Dup_Duniv: forall e,
  Dup Duniv e = DupN e.
Proof.
intros.
unfold Dup; simpl; eauto.
Qed.

Fact Dup_DupN: forall f e,
  Dup (DupN f) e = DupN e.
Proof.
intros.
unfold Dup; simpl; eauto.
Qed.

Fact Dup_DupX: forall d f e,
  Dup (DupX d f) e = DupN e.
Proof.
intros.
unfold Dup; simpl; eauto.
Qed.

Fact Dup_Dup: forall f d e,
  Dup (Dup d f) e = DupN e.
Proof.
intros.
unfold Dup at 2; simpl.
destruct DisFun_dec with d; try (destruct DisUnit_dec with d); simpl; try first
  [ rewrite Dup_DupN; auto
  | rewrite Dup_DupX with (d := d); auto
  ].
Qed.

Fact Dup_Dempty: forall e,
  Dup Dempty e = DupN e.
Proof.
intros; unfold Dup; simpl; eauto.
Qed.

Fact Dup_Dunit: forall e,
  Dup Dunit e = DupX Dunit e.
Proof.
intros; unfold Dup; simpl; eauto.
Qed.

Fact Dup_Dnat: forall e,
  Dup Dnat e = DupN e.
Proof.
intros; unfold Dup; simpl; eauto.
Qed.

Fact Dup_Dfun: forall e da df,
  Dup (Dfun da df) e = DupX (Dfun da df) e.
Proof.
intros; unfold Dup; simpl; eauto.
Qed.

Fact Ddown_Dup_Duniv: forall e d,
   Ddown (Dup Duniv e) d =  DdownN d.
Proof.
intros.
intros; unfold Ddown; simpl; eauto.
Qed.

Fact Ddown_Dup_Dnat: forall e d,
   Ddown (Dup Dnat e) d =  DdownN d.
Proof.
intros.
intros; unfold Ddown; simpl; eauto.
Qed.

Fact Ddown_Dup_Dempty: forall e d,
   Ddown (Dup Dempty e) d =  DdownN d.
Proof.
intros.
intros; unfold Ddown; simpl; eauto.
Qed.

Fact Ddown_DupN: forall e d,
   Ddown (DupN e) d = DdownN d.
Proof.
intros.
intros; unfold Ddown; simpl; eauto.
Qed.

Fact Ddown_DupX: forall f e d,
   Ddown (DupX f e) d = DdownN d.
Proof.
intros.
intros; unfold Ddown; simpl; eauto.
Qed.

Fact Ddown_Dup: forall f e d,
   Ddown (Dup f e) d = DdownN d.
Proof.
intros.
unfold Dup; simpl.
destruct DisFun_dec; try (destruct DisUnit_dec); simpl; try first
  [ rewrite Ddown_DupN; auto
  | rewrite Ddown_DupX; auto
  ].
Qed.

Fact Ddown_Dunit: forall d,
  Ddown Dunit d = DdownX Dunit d.
Proof.
intros; unfold Ddown; simpl; auto.
Qed.

Fact Ddown_Duniv: forall d,
  Ddown Duniv d = DdownN d.
Proof.
intros; unfold Ddown; simpl; auto.
Qed.

Fact Ddown_Dnat: forall d,
  Ddown Dnat d = DdownN d.
Proof.
intros; unfold Ddown; simpl; auto.
Qed.

Fact Ddown_Dempty: forall d,
  Ddown Dempty d = DdownN d.
Proof.
intros; unfold Ddown; simpl; auto.
Qed.

Fact Ddown_Dfun: forall d da df,
  Ddown (Dfun da df) d = DdownX (Dfun da df) d.
Proof.
intros; unfold Ddown; simpl; auto.
Qed.
