(******************************************************************************
 * Dariusz Biernacki <dabi@cs.uni.wroc.pl>
 * Pawel Wieczorek <pawel.wieczorek@cs.uni.wroc.pl>
 *
 * Institute of Computer Science
 * University of Wroclaw
 *)

Set Implicit Arguments.


Require Import Nbe.Domain.Def.
Require Import Nbe.Domain.Eval.
Require Import Nbe.Domain.Rb.
Require Import Nbe.Syntax.


(******************************************************************************
 * Normalization for terms
 *)
Inductive Nbe (A:Tm) (Gamma:Cxt) (t:Tm) : Tm -> Prop :=
| Nbe_intro: forall DA d denv v,
  EvalCxt Gamma denv ->
  EvalTm A denv DA ->
  EvalTm t denv d ->
  RbNf (length Gamma) (Ddown DA d) v ->
  Nbe A Gamma t v
.
Hint Constructors Nbe.

(******************************************************************************
 * Defined relation is deterministic
 *)

Fact Nbe_deter: forall A Gamma t v1,
  Nbe A Gamma t v1 -> forall v2,
  Nbe A Gamma t v2 -> v1 = v2
.
Proof.
intros.
induction H.
inversion H0; subst.
replace denv with denv0 in *.
replace DA with DA0 in *.
replace d with d0 in *.
eapply RbNf_deter; eauto.
eapply EvalTm_deter; eauto.
eapply EvalTm_deter; eauto.
eapply EvalCxt_deter; eauto.
Qed.

(******************************************************************************
 * Nbe gives normal forms
 *)

Fact Nbe_gives_nf: forall A Gamma t v,
  Nbe A Gamma t v -> Nf v
.
Proof.
intros.
destruct H.
eapply RbNf_gives_Nf; eauto.
Qed.